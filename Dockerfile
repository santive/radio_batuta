FROM hacklab/wordpress
LABEL mantainer "Hacklab <contato@hacklab.com.br>"

COPY ["./", "/var/www/html/"]

RUN a2enmod headers \
    && mkdir -p /var/www/html/wp-content/plugins/wp-super-cache/ \
    && chown -R www-data: /var/www/html/

