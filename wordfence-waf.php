<?php
// Before removing this file, please verify the PHP ini setting `auto_prepend_file` does not point to this.
//
if ( !defined('ABSPATH') ) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

if (file_exists(ABSPATH . 'wp-content/plugins/wordfence/waf/bootstrap.php')) {
	define("WFWAF_LOG_PATH", ABSPATH . 'wp-content/wflogs/');
	include_once ABSPATH . 'wp-content/plugins/wordfence/waf/bootstrap.php';
}
?>
