<p class="category">
	<?php
	$post = get_post( $idpost );
	// get post type by post
	$post_type = $post->post_type;
	// get post type taxonomies
	$taxonomies = get_object_taxonomies( $post_type );
	// var_dump($taxonomies);
	$taxonomy = $taxonomies[1]; //Pula a taxonomie de tags.
	//var_dump($taxonomy);
	$terms = get_the_terms( $post->ID, $taxonomy );
	//var_dump($terms);

	if ( $terms ) {
		foreach ( $terms as $term ) {
			$term_link = get_term_link( $term ); ?>
            <a href="<?php echo $term_link; ?>"><?php echo $term->name;
				if ( next( $terms ) ) {
					echo ', ';
				} ?></a>
		<?php }//end foreach terms
		?>
	<?php } else {
		$post_type = get_post_type_object( get_post_type( $post ) );

		$parent_post_type = get_episodio_pai_post_type( $post->ID );

		$post_type_parent = get_post_type_object( $parent_post_type );

		if(strtolower($post_type->labels->name) == 'posts'){
			$category_post = get_the_category();
		    ?>
            <a href="<?php echo get_category_link($category_post[0]->term_id); ?>"><?php echo $category_post[0]->name; ?></a>
        <?php } elseif ( $post_type_parent ) { ?>
            <a href="<?php echo get_post_type_archive_link( $post_type_parent->has_archive ); ?>"><?php echo $post_type_parent->labels->name; ?></a>
		<?php } else { ?>
            <a href="<?php echo get_post_type_archive_link( $post_type->name ); ?>"><?php echo $post_type->labels->name; ?></a>
		<?php }?>

	<?php } ?>

</p>