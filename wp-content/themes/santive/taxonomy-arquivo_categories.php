<!-- Header -->
<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') { ?>
    <script type="text/javascript">
        jQuery('body').remvoveClass('single');
    </script>

    <?php
    //is ajax
    //Dont load Header
} else {
    get_header();
}

checkCookie();?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right'); bloginfo(); ?>" />

<?php
//Taxonomie Name
$taxonomy = "programa_categories";
?>

<div class="row">

    <?php $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
    //var_dump($term);
    $term_name = $term->name;
    $term_description = $term->description;
    $term_id = $term->term_id;

    $term_aux;

    //Get post term
    $terms = get_terms($taxonomy);
    //var_dump($terms);
    foreach ($terms as $term) {
        $term_aux = $term->slug;

    } ?>


    <div class="home-content content-infinite-scroll">
        <div class="row">
            <div class="col-xs-12">
                <div class="title-section">
                    <h1><?php echo $term_name; ?></h1>

                    <p><?php echo $term_description; ?></p>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row grid-wrap">

           <?php
           while (have_posts()) : the_post();

                ?>


                <div class="col-xs-12 col-sm-12 col-md-3 item-infinite-scroll">
                    <div class="post-thumb">
                        <!-- <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('programa_thumbnail', array('class' => 'img-responsive')); ?></a> -->
                        <a href="<?php the_permalink(); ?>"><?php $post = get_the_ID();
                            the_post_thumbnail_responsive($post, 'programa_thumbnail'); ?></a>

                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                        <p><?php echo getExcerpt(get_the_excerpt(), 0, 300); ?></p>

                        <p class="date"><?php the_time('d.m.Y'); ?></p>
                    </div>
                </div>
                <div class="navigation"><p><?php posts_nav_link(); ?></p></div>

            <?php endwhile;
            wp_reset_query();
            wp_reset_postdata(); ?>


        </div>
        <!-- end row -->
    </div>
    <!-- end home-content -->

</div><!-- end row -->


<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
    //is ajax
}else{ ?>

    <?php  get_footer(); ?>

    </div>

    <?php wp_footer(); ?>

    <!-- Inserindo player -->
    <?php get_template_part('content', 'player' ); ?>

    <script type="text/javascript" src="<?php echo get_bloginfo('template_url') ?>/js/player.js"></script>


    </body>

    </html>



<?php }?>
<!-- Footer -->