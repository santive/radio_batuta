<!DOCTYPE html> <!-- Html 5 -->
<html <?php language_attributes() ?> >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport' />
        <title><?php
        wp_title('-', true, 'right');
        bloginfo();
        ?></title>
        <!-- JQuery -->
        
        <!-- Css e Js do Tema -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/style.css" />        
        <link href='http://fonts.googleapis.com/css?family=Roboto:500,900,300,700,400' rel='stylesheet' type='text/css'>
        
        <?php wp_head(); ?>

        
    </head>
    <body>
        
        <!-- Facebook -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
        return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <button id="menu-button" class="menu-button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <div class="container main-container">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <div class="logo">
                            <a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png"></a>                            
                        </div>
                    </div>
                </div>
            </div><!-- end row -->
            
            <nav id="menu" class="menu">
                <ul>
                    <?php
                    $count = 0;
                    $submenu = false;
                    ?>
                    <?php $items = wp_get_nav_menu_items( 'Topo');  ?>
                    <?php foreach($items as $item){
                    if ( !$item->menu_item_parent ) : $parent_id = $item->ID; ?>
                    
                    <li>
                        <a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                        <?php endif ?>
                        <?php if ( $parent_id == $item->menu_item_parent ): ?>
                        <?php if ( !$submenu ): $submenu = true; ?>
                        <ul class="sub-menu">
                            <?php endif; ?>
                            <li>
                                <a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                            </li>
                            <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                        </ul>
                        <?php $submenu = false; endif; ?>
                        <?php endif; ?>
                        <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                    </li>
                    <?php $submenu = false; endif; ?>
                    <?php $count++; ?>
                    <?php } //endforeach ?>
                    <li>
                        <?php get_search_form(); ?>
                    </li>
                </ul>
            </nav><!-- end menu -->
<!--            --><?php //if (is_home()): ?>
<!--            --><?php //else: ?>
<!--            --><?php //get_template_part('content', 'player-aovivo' ); ?>
<!--            --><?php //endif ?>

            <section id="content">

