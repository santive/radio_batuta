<?php
@session_start();
$_SESSION['header_loaded'] =  true;
?>
<!DOCTYPE html> <!-- Html 5 -->
<html <?php language_attributes() ?> >
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
	<title><?php
		wp_title( '-', true, 'right' );
		bloginfo();
		?></title>
	<!-- JQuery -->

	<!-- Css e Js do Tema -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ) ?>/css/style.css"/>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ) ?>/style.css"/>
	<link href='http://fonts.googleapis.com/css?family=Roboto:500,900,300,700,400' rel='stylesheet' type='text/css'>
    <script src="https://use.fontawesome.com/ab38257ecc.js"></script>

	<?php wp_head(); ?>

</head>
<body <?php body_class( $class ); ?>>

<!-- Facebook -->
<div id="fb-root"></div>
<script>(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

<button id="menu-button" class="menu-button hidden-lg hidden-md">
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</button>
<nav class="top-menu hidden-sm hidden-xs">
	<div class="container">
		<div class="row">
			<div class="img-logo">
                <!-- link sem classe ajax -->
				<a id="link-ims" href="http://ims.com.br/ims/"><img
						src="<?php bloginfo( 'template_directory' ); ?>/images/logo-ims.png"></a>
			</div>
            <ul class="menu-social menu-social-icon">
                <li><a href="https://www.facebook.com/institutomoreirasalles/" target="_blank" class="social-icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/imoreirasalles" target="_blank" class="social-icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/imoreirasalles/" target="_blank" class="social-icon"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
			<?php
			$defaults = array(
				'menu'        => 'Menu Top Bar',
				'container'   => false,
				'menu_class'  => 'menu-desktop menu-desktop-top pull-right',
				'echo'        => true,
				'fallback_cb' => 'wp_page_menu',
				'items_wrap'  => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			);
			wp_nav_menu( $defaults );

			?>
		</div>
	</div>
</nav>
<div class="container main-container">
	<div class="row row-header">
		<div class="col-md-3 col-xs-12">
			<div class="header">
				<div class="logo">
					<a href="<?php echo site_url(); ?>"><img
							src="<?php bloginfo( 'template_directory' ); ?>/images/logo.png"></a>
				</div>
			</div>
		</div>
        <div class="col-md-9 col-menu-desktop hidden-xs hidden-sm">
            <ul class="menu-social">
                <li><span id="btn-busca"><img
                                src="<?php bloginfo( 'template_directory' ); ?>/images/icon-search.png"></span></li>
            </ul>
	        <?php
	        $defaults = array(
		        'menu'        => 'Menu Desktop',
		        'container'   => false,
		        'menu_class'  => 'menu-desktop pull-right',
		        'echo'        => true,
		        'fallback_cb' => 'wp_page_menu',
		        'items_wrap'  => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	        );
	        wp_nav_menu( $defaults );

	        ?>
        </div>
        <div class="coluna-central-lg hidden-sm">
            <div class="search">
                <form action="<?php bloginfo( 'siteurl' ); ?>" id="searchform" method="get" role="search">
                    <input type="text" value="" name="s" id="s" placeholder="Pesquise aqui" class="ipt-search">

                    <button type="submit" id="searchsubmit">
                        <img style="margin-top:-3px;" src="<?php bloginfo( 'template_directory' ); ?>/images/icon-lupa-big.png">
                    </button>


                </form>
            </div>
        </div>
	</div><!-- end row -->

	<nav id="menu" class="menu menu-mobile">
		<ul>
			<?php
			$count   = 0;
			$submenu = false;
			?>
			<?php $items = wp_get_nav_menu_items( 'Menu mobile' ); ?>
			<?php foreach ( $items as $item ) {
				if ( ! $item->menu_item_parent ) : $parent_id = $item->ID; ?>

					<li>
					<a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
				<?php endif ?>
				<?php if ( $parent_id == $item->menu_item_parent ): ?>
					<?php if ( ! $submenu ): $submenu = true; ?>
						<ul class="sub-menu">
					<?php endif; ?>
					<li>
						<a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
					</li>
					<?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
						</ul>
						<?php $submenu = false; endif; ?>
				<?php endif; ?>
				<?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
					</li>
					<?php $submenu = false; endif; ?>
				<?php $count ++; ?>
			<?php } //endforeach ?>
			<li>
				<?php get_search_form(); ?>
			</li>
		</ul>
	</nav><!-- end menu -->
	<!--            --><?php //if (is_home()): ?>
	<!--            --><?php //else: ?>
	<!--            --><?php //get_template_part('content', 'player-aovivo' ); ?>
	<!--            --><?php //endif ?>
	<?php wp_reset_query();
	wp_reset_postdata(); ?>
	<?php get_template_part( 'content', 'player-aovivo' ); ?>
	<section id="content">