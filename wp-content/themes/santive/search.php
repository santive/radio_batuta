<!-- Header -->
<?php
if ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) AND strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) === 'xmlhttprequest' ) { ?>
	<script type="text/javascript">
		jQuery('body').removeClass('single');
	</script>

	<?php
	//is ajax
	//Dont load Header
} else {
	get_header();
}


checkCookie();?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title( '-', true, 'right' );
bloginfo(); ?>"/>

<div class="home-container">

	<div class="row">
		<div class="col-xs-12">
			<div class="search-result">
				<p>Busca por &quot;<?php the_search_query(); ?>&quot;</p>
			</div>
		</div>
	</div>

	<?php if ( have_posts() ) { ?>

		<div class="content-infinite-scroll">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="search-wrapper item-infinite-scroll">

					<div class="row">
						<div class="col-xs-12 col-sm-3">
							<a class="ajax" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'search_thumbnail' ); ?></a>
						</div>

						<div class="col-xs-12 col-sm-9">
							<?php get_template_part( 'content', 'category' ); ?>
							<p class="title"><a class="ajax" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
							<span class="content">
							<?php the_excerpt(); ?>
						</span>

							<span class="date"><?php the_time( 'd.m.Y' ); ?></span>


							<?php $posttags = get_the_tags();
							if ( $posttags ) { ?>
								<span class="tag">
					<?php
					$array_tag = array();
                    foreach ( $posttags as $tag ) {
						$taglink = get_term_by( 'name', $tag->name, 'post_tag' );
	                    $array_tag[] = '<a href="'.get_tag_link( $taglink ) . '">'.$tag->name.'</a>';
					} ?>

									<i class="fa fa-tag fa-rotate-90" aria-hidden="true"></i> <?php echo implode(', ', $array_tag); ?>


						</span>
							<?php } ?>

						</div>
					</div>

			</div>

		<?php endwhile; ?>
		<div class="navigation"><p><?php posts_nav_link(); ?></p></div>
		</div>


	<?php }else{ ?>
        <div class="search-wrapper" style="margin-bottom: 50px;">
            <p>Desculpe, nada foi encontrado.</p>
        </div>
    <?php } ?>


</div>


<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
	//is ajax
}else{ ?>

	<?php  get_footer(); ?>

	</div>

	<?php wp_footer(); ?>

	<!-- Inserindo player -->
	<?php get_template_part('content', 'player' ); ?>

	<script type="text/javascript" src="<?php echo get_bloginfo('template_url') ?>/js/player.js"></script>


	</body>

	</html>



<?php }?>