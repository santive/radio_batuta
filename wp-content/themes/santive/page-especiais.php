<?php /* Template Name: Página de Especiais2 */ ?>

<!-- Header -->

<?php get_header(); ?>      

<div class="home-content">



<?php $pagename = get_query_var('pagename');  $post = $wp_query->get_queried_object(); ?>



    <div class="row">

        <div class="col-md-6">

            <div class="title-section">

                <h1><?php echo $post->post_title; ?></h1>

                <p><?php echo $post->post_content; ?></p>

            </div>

        </div>

    </div><!-- end row -->

    

    <div class="row">



    <?php //WpQuery

            $loop = new WP_Query( array( 'post_type' => 'especiais', 'posts_per_page' => -1 ) );

            while ( $loop->have_posts() ) : $loop->the_post();?>



        <div class="col-md-3">

            <div class="post-thumb">

                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('documentario_thumbnail', array('class' => 'img-responsive')); ?></a>

                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                <?php the_excerpt(); ?>

                <p class="date"><?php the_time('d F Y');?></p>

            </div>            

        </div><!-- end col -->



    <?php endwhile; wp_reset_query(); wp_reset_postdata();?>



    </div><!-- end row -->

    

    

</div><!-- end home-content -->







<?php get_footer(); ?>

<!-- Footer -->     