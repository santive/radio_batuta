<?php /* Template Name: Página de Programas */ ?>
<!-- Header -->
<?php get_header(); ?> 

<?php 
//Taxonomie Name
$taxonomy  = "programa_categories";
?>

<div class="row">

<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
 //var_dump($term); 
 $term_name = $term->name;
 $term_description = $term->description; ?>


<?php if($term->parent > 0){ //Child Theme 


$term_aux;

//Get post term
$terms = get_terms($taxonomy);
//var_dump($terms);
foreach($terms as $term){
$term_aux = $term->slug;    

} ?>



<div class="home-content">

    <div class="row">
        <div class="col-md-6">
            <div class="title-section">
                <h1><?php echo $term_name; ?></h1>
                <p><?php echo $term_description; ?></p>
            </div>
        </div>
    </div><!-- end row -->
    <div class="row grid-wrap">

<?php
$args = array(
    'post_type' => 'programa',
    'tax_query' => array(
        array(
            'taxonomy' => $taxonomy,
            'field'    => 'slug',
            'terms'    => $term_aux,
            
        ),
    ),
);

$loop = new WP_Query( $args );
while ($loop->have_posts()) : $loop->the_post();
?>
        <div class="col-sm-2 col-md-3 grid-item">
            <div class="post-thumb">
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('programa_thumbnail', array('class' => 'img-responsive')); ?></a>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p><?php echo getExcerpt(get_the_excerpt(), 0, 250); ?></p>
                <p class="date"><?php the_time('d F Y');?></p>
            </div>            
        </div>
<?php  endwhile;  wp_reset_query(); wp_reset_postdata();  ?>

    </div><!-- end row -->
</div><!-- end home-content -->

<?php }//Parent Theme
else{ ?>

<div class="home-container">

    <div class="row">
        <div class="col-md-12">
            <div class="title-section">
                <h1>Programas</h1>
                <p><?php echo term_description(); ?></p>
            </div>
        </div>
    </div><!-- end row -->


<?php 

$term_aux;

//Get post term
$terms = get_terms($taxonomy);
//var_dump($terms);
foreach($terms as $term){
if($term->parent > 0){

//var_dump(get_term_link($term));
$term_aux = $term->slug; 
$term_link = get_term_link($term);    


?>

    <div class="row">
        <div class="col-md-12">           

            <div class="category-container">                
                <div class="category-description">
                    <h2><a href="<?php echo $term_link; ?>"><?php echo $term->name; ?></a></h2>
                    <p><?php echo $term->description ?></p>
                    <p class="destaque"><a href="<?php echo $term_link; ?>">VER TODOS</a></p>
                </div>


                <?php 
                $args = array(
                    'post_type' => 'programa',
                    'posts_per_page'=> 3,
                    'tax_query' => array(
                        array(
                            'taxonomy' => $taxonomy,
                            'field'    => 'slug',
                            'terms'    => $term_aux,
                            
                        ),
                    ),
                );

                $loop = new WP_Query( $args );
                while ($loop->have_posts()) : $loop->the_post();

                ?>


                <div class="category-post">
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('selecao_parent_thumbnail', array('class' => 'img-responsive')); ?></a>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <p><?php echo getExcerpt(get_the_excerpt(), 0, 250); ?></p>
                    <span class="destaque"><?php the_time('d F Y');?></span>
                </div>

                <?php  endwhile;  wp_reset_query(); wp_reset_postdata();  ?>
                
            </div><!-- end category-container -->

        </div><!-- end col -->
    </div><!-- end row -->


<?php
 } //end if parent 
} //end foreach terms ?>

</div><!-- end home-container -->


<?php
}// End if/else Parent Theme
?>

</div><!-- end row -->


<?php get_footer(); ?>
<!-- Footer -->     