var objID = 0;
var airtimeUrl = '';
var airtimeProg = '';
var airtimeUpdate = false;

setInterval(function(){
    if(airtimeUpdate){
        atualizarAirtime(airtimeUrl, airtimeProg);
    }
},5000);

// setInterval(function () {
    // jQuery('.jp-next, .jp-previous').unbind('click');
jQuery('.jp-next').click(function(){
    if(!jQuery(this).hasClass('disabled')) {
        var id = parseInt(objID) + 1;
        setMedia(id);
    }
});

jQuery('.jp-previous').click(function(){
    if(!jQuery(this).hasClass('disabled')) {
        var id = parseInt(objID) - 1;
        setMedia(id);
    }
});

jQuery('.post-url,  .post-category, .post-url').click(function(){
    jQuery('.player').fadeOut();
});

window.onresize = function() {
    debounce(marquee('titlez'), 500);
    debounce(marquee('autor'), 500);
}

jQuery('.toggle').click(function(){
    jQuery('.player').fadeToggle();
});


    // var player = jQuery('#batuta-player')[0];

jQuery('#batuta-player').on('ended', function(){
    objID++;
    setMedia(objID);
});

jQuery('#batuta-player').on('timeupdate', function () {
    var cur = jQuery('#batuta-player')[0].currentTime;
    var end = jQuery('#batuta-player')[0].duration;
    var perc = (cur * 100) / end;

    jQuery('.jp-play-bar').css('width', perc+'%');

    cur = end - cur;

    var sec= new Number();
    var min= new Number();
    sec = Math.floor(cur);
    min = Math.floor( sec / 60 );
    min = min >= 10 ? min : '0' + min;
    sec = Math.floor( sec % 60 );
    sec = sec >= 10 ? sec : '0' + sec;

    var dur_mins = Math.floor(end/60,10);
    var dur_secs = Math.floor(end - dur_mins * 60);

    // jQuery('.jp-current-time').html(min + ":"+ sec);
    jQuery('.jp-current-time').html("-" + min + ":"+ sec + " / " + end.toString().toTime());
});

String.prototype.toTime = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}

    if(hours == '00')
        return minutes+':'+seconds;

    return hours+':'+minutes+':'+seconds;
}

jQuery('.jp-seek-bar').click(function(e){
    var parentOffset = jQuery(this).offset();
    var relX = e.pageX - parentOffset.left;
    var dur = jQuery('#batuta-player')[0].duration;
    jQuery('.jp-play-bar').css('width', relX+'px');

    var perc =  (relX * 100)  / jQuery(this).width();
    jQuery('#batuta-player')[0].currentTime = (perc * dur / 100);
});

jQuery('.jp-volume-bar').click(function(e){
    var parentOffset = jQuery(this).offset();
    var relX = e.pageX - parentOffset.left;
    jQuery('.jp-volume-bar-value').css('width', (relX * 0.8 )+'px');
    var volume =  (relX)  / jQuery(this).width();
    jQuery('#batuta-player')[0].volume = volume;
});

jQuery('.jp-play').click(function() {
    jQuery('.jp-pause').show();
    jQuery(this).hide();
    jQuery('#batuta-player')[0].play();
    jQuery('#player-button').addClass('playing');
});

jQuery('.jp-pause').click(function() {
    jQuery(this).hide();
    jQuery('.jp-play').show();
    jQuery('#batuta-player')[0].pause();
    jQuery('#player-button').removeClass('playing');

});
// },1000);

function streaming(object){
    var obj = object.data('post-url');
    if(obj.indexOf('classic') == -1){
        airtimeUrl = 'https://radioims.airtime.pro/api/live-info-v2';
        airtimeProg = 'MPB';
    }
    else{
        airtimeUrl = 'https://classicoims.airtime.pro/api/live-info-v2'
        airtimeProg = 'Clássicos IMS';
    }
    atualizarAirtime(airtimeUrl, airtimeProg);
    airtimeUpdate = true;
    console.log('OBJETO: ', obj);

    jQuery('.jp-play').hide();
    jQuery('.jp-pause').show();
    jQuery('#player-button').fadeIn();
    jQuery('body').addClass('player-open');

    jQuery('#batuta-player')[0].src = obj;
    jQuery('#batuta-player')[0].load();
    jQuery('#batuta-player')[0].play();
    jQuery('#player-button').addClass('playing');

    jQuery('.jp-audio').addClass('active');
    jQuery('.jp-duration[role="timer"]').hide();
}

function atualizarAirtime(url, prog){
    jQuery.ajax({
        url : url,
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        type : 'GET',
        dataType: 'jsonp'
    }).success(function(data){
        var track = data.tracks.current.metadata;
        var show = data.shows.current;
        // jQuery('.stream-duration').text(showDiff(new Date(track.starts), new Date(track.ends)));
        jQuery('.jp-progress').hide();
        jQuery('.jp-interface').addClass('streaming');
        // jQuery('.jp-current-time').hide();
        // jQuery('.jp-duration').hide();
        jQuery('.jp-previous, .jp-next, .jp-time-holder').hide();
        jQuery('.compositor').text(track.composer);
        jQuery('.autor').text(track.artist_name);
        jQuery('.titlez').text(track.track_title);
        jQuery('.categoria').text(show.genre);
        jQuery('#post-tipo').html('BATUTA 24 HORAS');

        jQuery('#post-img').html('<img width="468" height="335" src="/wp-content/themes/santive/images/player-classico.jpg" class="wp-post-image">');

        if(airtimeProg == 'MPB')
            jQuery('#post-img').html('<img width="468" height="335" src="/wp-content/themes/santive/images/player-mpb.jpg" class="wp-post-image">');

        jQuery('#post-nome').html(prog);
    });
}

function hidePrevNext(){
    jQuery('.jp-next,.jp-previous').css('visibility','hidden');
}

function showPrevNext(){
    jQuery('.jp-next,.jp-previous').css('visibility','auto');
}

function setMedia(id){
    jQuery('.jp-previous, .jp-next, .jp-time-holder, .jp-time-holder').show();
    jQuery('.jp-interface').removeClass('streaming');

    jQuery('.jp-progress').show();
    jQuery('.jp-play').hide();
    jQuery('.jp-pause').show();
    jQuery('#player-button').fadeIn();
    jQuery('body').addClass('player-open');

    var player = document.getElementById('batuta-player');
    var obj = objMedia[id];

    debounce(marquee('titlez'), 500);
    debounce(marquee('autor'), 500);

    objID = id;
    jQuery('#batuta-player')[0].src = obj.mp3;
    jQuery('#batuta-player')[0].load();
    jQuery('#batuta-player')[0].play();
    jQuery('#player-button').addClass('playing');

    jQuery('.jp-audio').addClass('active');
    jQuery('.jp-duration[role="timer"]').show();
    // jQuery('.streaming-duration').hide();
    // jQuery(this).jPlayer("setMedia", objMedia[id]).jPlayer('play');

    jQuery('.compositor').text(obj.composer);
    jQuery('.titlez').text(obj.title);
    jQuery('.autor').text(obj.artist);
    jQuery('#post-img').html(objPost.img);
    jQuery('#post-tipo').html(objPost.tipo);
    jQuery('#post-nome').html(objPost.nome);
    jQuery('.post-url').attr('href',objPost.url);
    jQuery('.post-category').attr('href',objPost.category);
    airtimeUpdate = false;

    var sizes = [];
    jQuery('.res').each(function(){
        sizes.push(jQuery(this).width());
    });

    setTimeout(function(){
        var width = Math.max.apply( Math, sizes) + 460;
        console.log(width);
        if(width > 460)
            jQuery('.player-content').css('width',width+'px');
    },1000);
}

/*----------------------------------------------------------------------------*
**------------------- EFEITO DE ABERTURA DO PLAYER  E FUNÇÕES ----------------*
**----------------------------------------------------------------------------*/

function showDiff(date1, date2){
    var diff = (date2 - date1)/1000;
    var diff = Math.abs(Math.floor(diff));

    var days = Math.floor(diff/(24*60*60));
    var leftSec = diff - days * 24*60*60;

    var hrs = Math.floor(leftSec/(60*60));
    var leftSec = leftSec - hrs * 60*60;

    var min = Math.floor(leftSec/(60));
    var leftSec = leftSec - min * 60;

    if(hrs == 0)
        return min + ":" + leftSec;
    else
        return hrs + ":" + min + ":" + leftSec;

}


function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

function marquee(className){
    setTimeout(function(){
        var element = document.getElementsByClassName(className)[0];

        if (element.offsetHeight < element.scrollHeight || element.offsetWidth < element.scrollWidth)
            jQuery(element).html('<marquee>'+element.innerText+'</marquee>');
        else
            jQuery(element).html(element.innerText);
    },3000);
}


function initPlayer(){

    var $ = jQuery;

    var $html = jQuery(document.documentElement),
    $site = jQuery('.site'),
    $body = jQuery('body');

    var hasCSSClipPaths = !!window.chrome && !/constructor/i.test(window.HTMLElement);

    if (hasCSSClipPaths) {
        $html.addClass('cssclippaths');
    }

    jQuery(window).scrollTop(0);

    var $modal   = null,
    $overlay = jQuery('.overlay'),
    $wrapper = jQuery('.modal-wrapper'),
    $ripple  = jQuery('.overlay__ripple');

    function lockScroll(pointer)
    {
        var w2, w1 = $html[0].clientWidth;
        $body.addClass('js-lock-scroll' + (pointer ? ' js-lock-pointer' : ''));
        w2 = $html[0].clientWidth;
        if (w2 > w1) {
            $body.css('padding-right', w2 - w1);
        }
    }

    function unlockScroll()
    {
        $body.css('padding-right', '').removeClass('js-lock-scroll js-lock-pointer');
    }

    function dist(p1, p2)
    {
        var xs = p2[0] - p1[0];
        var ys = p2[1] - p1[1];
        return Math.sqrt((xs * xs) + (ys * ys));
    }

    function fillViewport(p1, width, height)
    {
        var nw = dist(p1, [0,          0]),
        ne = dist(p1, [width,      0]),
        se = dist(p1, [width, height]),
        sw = dist(p1, [0,     height]);
        return 2 * Math.max(nw, ne, se, sw);
    }

    var openOverlay = function(btn, id)
    {
        $modal = jQuery('.modal[data-modal="' + id + '"]');

        lockScroll(true);

        requestAnimationFrame(function() {
            $ripple.removeClass('closing');

            $overlay.addClass('js-open').attr('data-modal', id);
            var bounds = btn.getBoundingClientRect();

            var vw = jQuery(window).width(),
            vh = jQuery(window).height();
            var p1 = [bounds.left + bounds.width / 2, bounds.top + bounds.height / 2];
            var diameter = fillViewport(p1, vw, vh);
            var pps = 3000;

            var duration = 100 + (Math.round((diameter / pps) * 1000) * 0.5);
            $ripple.css({
                'transition-duration' : duration + 'ms',
                'width'  : diameter,
                'height' : diameter,
                'left'   : p1[0],
                'top'    : p1[1]
            });

            console.log({ 'width'  : diameter,
                'height' : diameter,
                'left'   : p1[0],
                'top'    : p1[1] });

            setTimeout(function(){
                jQuery('.player').fadeIn();
            },duration);

            if (hasCSSClipPaths && id === 'test') {
                $modal.css('transition-duration', '300ms, ' + duration + 'ms');
                $modal.css('-webkit-clip-path', 'circle(' + (diameter/2) + 'px at 50% 50%)');
            }

            requestAnimationFrame(function() {
                $overlay.addClass('js-animate-in');

                $ripple.one('transitionend', function() {
                    $overlay.addClass('js-animate-done');
                    $body.removeClass('js-lock-pointer');
                    $ripple.removeAttr('style');
                })
            });

        });
    };

    var closeOverlay = function() {
        $body.addClass('js-lock-pointer');

        $overlay.removeClass('js-animate-done').removeAttr('data-modal');

        $ripple.addClass('closing');

        // setTimeout(function(){
        //     $ripple.css({
        //         'transition' : 'all 600ms' ,
        //         'width'  : 0,
        //         'height' : 0

        //     });
        // },10);


        requestAnimationFrame(function() {
            $modal.removeAttr('style');
            $overlay.removeClass('js-animate-in');
        });

        setTimeout(function(){
            $overlay.removeClass('js-open');
        },600);

        $ripple.one('transitionend', function() {
            $wrapper.scrollTop(0);
            unlockScroll();
        });
    }

    jQuery('[data-modal][data-action="open"], .playlist-item-link').on('click', function(e) {
        var btn = document.getElementById('player-button');
        requestAnimationFrame(function() {
            openOverlay(btn, jQuery(btn).data('modal'));
        });

        var sizes = [];

        setTimeout(function(){
            jQuery('.res').each(function(){
                sizes.push(jQuery(this).width());
            });
        },1000);

        setTimeout(function(){
            var width = Math.max.apply( Math, sizes) + 460;
            jQuery('.player-content').css('width',width+'px');
        },1200);
    });

    jQuery('.player-close').on('click', function() {
        jQuery('.player').fadeOut('fast');
        requestAnimationFrame(function() {
            closeOverlay();
        });
    });

    (function() {
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
            || window[vendors[x]+'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function(callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                    timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };

            if (!window.cancelAnimationFrame)
                window.cancelAnimationFrame = function(id) {
                    clearTimeout(id);
                };
            }());
}

setTimeout(initPlayer(),1000);
setTimeout(initPlayer(),2000);
setInterval(initPlayer())