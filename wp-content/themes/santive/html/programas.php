<!-- Header -->
<?php get_header(); ?>
<div class="home-container">

    <div class="row">
        <div class="col-md-12">
            <div class="title-section">
                <h1>Programas</h1>
                <p>Grandes cantores e compositores têm suas trajetórias lembradas neste programa, atualizado mensalmente. É possível saber mais de suas vidas e curtir as músicas mais marcantes</p>
                
            </div>
        </div>
    </div><!-- end row -->

    <div class="row">
        <div class="col-md-12">           

            <div class="category-container">                
                <div class="category-description">
                    <h2>Música é história</h2>
                    <p>O programa se vale de canções brasileiras para retratar tipos, situações e histórias da vida nacional. Em 2014 iniciou-se uma longa série dedicada aos grandes compositores brasileros. Vai ao ar aos domingos, às 14h50, na CBN e, com as músicas na íntegra, entra às segundas-feiras na batuta.</p>
                    <p class="destaque">VER TODOS</p>
                </div>
                <div class="category-post">
                    <img src="<?php bloginfo('template_directory'); ?>/images/category-post.png">
                    <h2>Haroldo Barbosa</h2>
                    <p>No ano do centenário de Haroldo Barbosa, completado em 21 de março, a Batuta faz agora, depois de um documentário em três episódios, um passeio rápido pela obra do compositor para o Música é História. Na seleção, sucessos como Nossos momentos e Pra que discutir com madame?.</p>
                    <span class="destaque">30 de março de 2015</span>
                </div>
                <div class="category-post">
                    <img src="<?php bloginfo('template_directory'); ?>/images/category-post.png">
                    <h2>Haroldo Barbosa</h2>
                    <p>No ano do centenário de Haroldo Barbosa, completado em 21 de março, a Batuta faz agora, depois de um documentário em três episódios, um passeio rápido pela obra do compositor para o Música é História. Na seleção, sucessos como Nossos momentos e Pra que discutir com madame?.</p>
                    <span class="destaque">30 de março de 2015</span>
                </div>
                <div class="category-post">
                    <img src="<?php bloginfo('template_directory'); ?>/images/category-post.png">
                    <h2>Haroldo Barbosa</h2>
                    <p>No ano do centenário de Haroldo Barbosa, completado em 21 de março, a Batuta faz agora, depois de um documentário em três episódios, um passeio rápido pela obra do compositor para o Música é História. Na seleção, sucessos como Nossos momentos e Pra que discutir com madame?.</p>
                    <span class="destaque">30 de março de 2015</span>
                </div>
            </div><!-- end category-container -->



            <div class="category-container">                
                <div class="category-description">
                    <h2>Música é história</h2>
                    <p>O programa se vale de canções brasileiras para retratar tipos, situações e histórias da vida nacional. Em 2014 iniciou-se uma longa série dedicada aos grandes compositores brasileros. Vai ao ar aos domingos, às 14h50, na CBN e, com as músicas na íntegra, entra às segundas-feiras na batuta.</p>
                    <p class="destaque">VER TODOS</p>
                </div>
                <div class="category-post">
                    <img src="<?php bloginfo('template_directory'); ?>/images/category-post.png">
                    <h2>Haroldo Barbosa</h2>
                    <p>No ano do centenário de Haroldo Barbosa, completado em 21 de março, a Batuta faz agora, depois de um documentário em três episódios, um passeio rápido pela obra do compositor para o Música é História. Na seleção, sucessos como Nossos momentos e Pra que discutir com madame?.</p>
                    <span class="destaque">30 de março de 2015</span>
                </div>
                <div class="category-post">
                    <img src="<?php bloginfo('template_directory'); ?>/images/category-post.png">
                    <h2>Haroldo Barbosa</h2>
                    <p>No ano do centenário de Haroldo Barbosa, completado em 21 de março, a Batuta faz agora, depois de um documentário em três episódios, um passeio rápido pela obra do compositor para o Música é História. Na seleção, sucessos como Nossos momentos e Pra que discutir com madame?.</p>
                    <span class="destaque">30 de março de 2015</span>
                </div>
                <div class="category-post">
                    <img src="<?php bloginfo('template_directory'); ?>/images/category-post.png">
                    <h2>Haroldo Barbosa</h2>
                    <p>No ano do centenário de Haroldo Barbosa, completado em 21 de março, a Batuta faz agora, depois de um documentário em três episódios, um passeio rápido pela obra do compositor para o Música é História. Na seleção, sucessos como Nossos momentos e Pra que discutir com madame?.</p>
                    <span class="destaque">30 de março de 2015</span>
                </div>
            </div><!-- end category-container -->



        </div>
    </div><!-- end row -->



</div><!-- end home-container -->

<?php get_footer(); ?>
<!-- Footer -->     