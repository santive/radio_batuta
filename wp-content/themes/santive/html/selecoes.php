<!-- Header -->
<?php get_header(); ?>     
<!-- Layout da busca (search php) -->

<div class="home-container">
	

<div class="row">
	<div class="col-md-12">
		<div class="title-section">
            <h1>Busca por "Pixinguinha"</h1>
            <p class="search-cont-result">5 Resultados</p>
        </div>
	</div>
</div>


<div class="row">
<div class="search-result">

	<div class="col-md-3">
		<div class="search-image">
			<img src="<?php bloginfo('template_directory'); ?>/images/search.png">
		</div>
	</div>

	<div class="col-md-6">
		<div class="search-content">
		<p class="breadcrump">Programas > Música é História</p>
		<p class="title">Pixinguinha</p>
		<p class="content">Pixinguinha é um nome central da história da música brasileira. Como instrumentista (na flauta e no sax), como arranjador e como compositor. É deste, o autor de Carinhoso, Rosa, Naquele tempo e tantas obras ...</p>
		<p ><span class="date">92 de agosto de 2014</span><span class="tag">Pixinguinha, Velha Guarda, MPB</span></p>
			
		</div>
	</div>

	<div class="col-md-3">
		<div class="search-playlist">
			<ul>
				<li>Suíte Retratos, 1º movimento - Pixinguinha</li>
			</ul>
		</div>
	</div>

</div><!-- end search-result -->
</div><!-- end row -->

<div class="row">
<div class="search-result">

	<div class="col-md-3">
		<div class="search-image">
			<img src="<?php bloginfo('template_directory'); ?>/images/search.png">
		</div>
	</div>

	<div class="col-md-6">
		<div class="search-content">
		<p class="breadcrump">Programas > Música é História</p>
		<p class="title">Pixinguinha</p>
		<p class="content">Pixinguinha é um nome central da história da música brasileira. Como instrumentista (na flauta e no sax), como arranjador e como compositor. É deste, o autor de Carinhoso, Rosa, Naquele tempo e tantas obras ...</p>
		<p ><span class="date">92 de agosto de 2014</span><span class="tag">Pixinguinha, Velha Guarda, MPB</span></p>
			
		</div>
	</div>

	<div class="col-md-3">
		<div class="search-playlist">
			<ul>
				<li>Suíte Retratos, 1º movimento - Pixinguinha</li>
			</ul>
		</div>
	</div>

</div><!-- end search-result -->
</div><!-- end row -->



</div><!-- end home-container -->



<?php get_footer(); ?>
<!-- Footer -->   