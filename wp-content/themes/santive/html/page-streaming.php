<?php /* Template Name: Página de Streaming */ ?>
<!-- Header -->
<?php get_header(); ?>      
<div class="home-container">

<?php $pagename = get_query_var('pagename');  $post = $wp_query->get_queried_object(); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="title-section">
                <h1>Streaming</h1>
                <p>Grandes cantores e compositores têm suas trajetórias lembradas neste programa, atualizado mensalmente. É possível saber mais de suas vidas e curtir as músicas mais marcantes</p>
            </div>
        </div>
    </div><!-- end row -->

    <?php //WpQuery 
            $id = 0;
            $loop = new WP_Query( array( 'post_type' => 'streaming', 'posts_per_page' => -1 ) );
            while ( $loop->have_posts() ) : $loop->the_post();
            $postid = get_the_ID();?>

            
    <div class="row">
        <div class="col-md-12">
            <div class="category-streaming-container">    
                <div class="row">
                    <div class="col-sm-12">
                        <h2><?php the_title(); ?></h2>
                    </div>  
                </div>
                <div class="row category-streaming-content">
                    <div class="col-md-5">
                        <?php the_post_thumbnail('streaming_thumbnail', array('class' => 'img-responsive')); ?>    
                    </div>
                    <div class="col-md-7">
                        <div class="category-streaming-description">
                            <div class="row">
                                <div class="col-md-7">
                                    <?php the_excerpt(); ?>
                                </div>
                                <div class="col-md-5 play">
                                    <div class="play-container">
                                        <img src="<?php bloginfo('template_directory'); ?>/images/play.png">
                                        <p class=""><a href="#" id="<?php echo $id++; ?>" class="streaming-link" data-post-url="<?php echo rwmb_meta('batuta_streaming', $postid) ?>" data-post-id="<?php echo $postid; ?>">Ouça Ao Vivo</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end category-streaming-container -->
        </div><!-- end col -->
    </div><!-- end row -->


    <?php endwhile; wp_reset_query(); wp_reset_postdata();?>

    
    
</div><!-- end home-container -->



<?php get_footer(); ?>
<!-- Footer -->     