<?php

function playlist(){
	global $post;
	require_once(ABSPATH."/wp-content/themes/santive/api/getID3/getid3.php");

	$postid = get_the_ID();
	$postid = $post->ID;

	if(get_query_var('pagename')){
		$jsonurl =WWW."/wp-admin/admin-ajax.php?action=player_ajax&post_id=".$postid;
		$json = file_get_contents($jsonurl,0,null,null);
		$json_output = json_decode($json, true);
	}else{
		$audiofiles = get_posts(['post_type' => 'attachment','post_mime_type' => 'audio','numberposts' => -1]);
		foreach ($audiofiles as $file)
		{
			$json_output['url'][] = wp_get_attachment_url($file->ID);
			$json_output['title'][] = utf8_encode($file->post_title);
			$json_output['image'][] = '';
		}
	}
	$y = count($json_output['url']); 
	$x = 1; 
	foreach($json_output['url'] AS $key => $valor){ 
		$m = str_replace("\/","/",($json_output['url'][$key]));
		$mp3 = str_replace(WWW,ABSPATH,$m);
		$getID3 = new getID3;
		$OldThisFileInfo = $getID3->analyze($mp3);
		$tag = $OldThisFileInfo['tags']['id3v2'];
		?>
		{
		title:"<?php echo utf8_decode($json_output['title'][$key]); ?>",
		artist:"<?php echo $tag['artist'][0] ?>",
		genre:"<?php echo ($tag['genre'][0]); ?>",
		album:"<?php echo $tag['album'][0] ?>",
		autor:"<?php echo ($tag['year'][0]); ?>",
		mp3: "<?php echo ($json_output['url'][$key]); ?>",
		poster:"<?php echo ($json_output['image'][$key]); ?>"
	}
	<?php 
	if($x < $y) echo ","; 
	$x++; } 
}
?>
<link href="<?php echo WWW ?>/wp-content/themes/santive/player/dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo WWW ?>/wp-content/themes/santive/player/lib/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo WWW ?>/wp-content/themes/santive/player/dist/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?php echo WWW ?>/wp-content/themes/santive/player/player/dist/add-on/jplayer.playlist.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.streaming-link').click(function(){
			var obj = $(this).data('post-url');
			$("#jquery_jplayer_1").jPlayer("destroy");
			$('#jquery_jplayer_1').jPlayer({
				swfPath: "../dist/jplayer",
				solution: 'html',
				supplied: 'oga',
				shuffleOnLoop: true,
				errorAlerts: true,
				warningAlerts: true,
				ready: function () {
					$(this).jPlayer("setMedia",{oga:obj}).jPlayer('play');
				}
			});
		});

		$('#jquery_jplayer_1').jPlayer({
			swfPath: "../dist/jplayer",
			solution: 'html',
			supplied: 'mp3',
			shuffleOnLoop: true,
			errorAlerts: true,
			warningAlerts: true,
			ready: function () {
				$(this).jPlayer("setMedia",{oga: "http://radioims.out.airtime.pro:8000/radioims_a"}).Play;
			}
		});

		var myPlaylist = new jPlayerPlaylist({
			jPlayer: "#jquery_jplayer_1"
		}, [{oga: "http://radioims.out.airtime.pro:8000/radioims_a"}], {
			playlistOptions: {
				autoPlay: true,
				shuffleOnLoop: true,
				loopOnPrevious: true
			},
			supplied: "mp3",
			loop: true
		});

		$("#jquery_jplayer_1").bind($.jPlayer.event.play, function (event) {
			var current = myPlaylist.current,
			playlist = myPlaylist.playlist;

			$.each(playlist, function (index, obj) {
				if (index == current) {
					$('.compositor').text(obj.artist);
					$('.titlez').text(obj.title);
					$('.album').text(obj.album);
					$('.autor').text(obj.autor);
					$('.categoria').text(obj.genre);
					$('.imageposter').text("<img src='" + obj.poster + "' style='max-width:100px; max-height:100px;'>");
				}
			});

			$("#jquery_jplayer_1").bind($.jPlayer.event.ended, function(event) {
				var current = myPlaylist.current,
				playlist = myPlaylist.playlist;
				playlist.Next;
				playlist.Play;
			});
		});
	});
</script>

<div id="jquery_jplayer_1" class="jp-jplayer"></div>
<div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
	<div class="jp-type-single">
		<div class="jp-gui jp-interface">
			<div class="jp-progress">
				<div class="jp-seek-bar">
					<div class="jp-play-bar"></div>
				</div>
			</div>
			<div class="col-md-5" style="width: 35%">
				<div class="center-play"></div>
			</div>
			<div class="col-md-2" style="width: 30%">
				<div class="col-md-7">
					<div class="jp-controls">
						<button class="jp-previous" role="button" tabindex="0">previous</button>
						<button class="jp-play" role="button" tabindex="0">play</button>
						<button class="jp-pause" role="button" tabindex="0">pause</button>
						<button class="jp-next" role="button" tabindex="0">next</button>
					</div>
				</div>
				<div class="col-md-5">
					<div class="jp-time-holder">
						<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
					</div>
					<div class="jp-volume-controls">
						<div class="jp-volume-bar">
							<div class="jp-volume-bar-value"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- DADOS DO ÁUDIO -->
			<div class="col-md-5" style="width: 35%">
				<div class="center-play">
					<div id="imageposter"></div>
					<div class="col-md-6">
						<div class="dados-som">
							<div class="categoria"></div>
							<div class="album"></div>
							<div class="autor"></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="dados-som2">
							<div class="titlez"></div>
							<div class="compositor"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="jp-no-solution">
			<span>Update Required</span>
			To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
		</div>
	</div>
</div>
