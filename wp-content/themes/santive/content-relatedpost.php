
        <div class="related-posts-content">


                    <?php
                    $result = rwmb_meta('batuta_related_posts', 'type=select_advanced');

                    if(get_post_type($post) != 'documentario') {
                        $custom_posts_types = get_custom_post_types( false ); //Lista de todos os custom post types
                    }else{
                        $custom_posts_types = 'documentario';
                    }

                    $nposts = 3; //numero de posts
                    $nrposts = 0; //numero de posts gerados randomicamente
                    $args = array(); //args da wpQuery
                    $rposts = array(); //posts relacionados por tags
                    $result_posts = sizeof($result);

                    //Pega as tags do post atual
                    $tags = wp_get_post_tags($post->ID);


                    if($result_posts>0){

                        //Apenas dois posts são gerados
                        if($result_posts == 1) {
                            $nrposts =2; $nposts =1;
                            $args=array('post__in' => $result, 'post_type' => $custom_posts_types, 'posts_per_page' => $nposts);
                          }

                        //Apenas um post é gerado
                        if($result_posts == 2) {
                            $nrposts = 1; $nposts =2;
                            $args=array('post__in' => $result, 'post_type' => $custom_posts_types, 'posts_per_page' => $nposts);
                        }

                        //Nenhum post é gerado.
                        if($result_posts == 3) {$args=array('post__in' => $result, 'post_type' => $custom_posts_types, 'posts_per_page' => $nposts); }

                    }else{
                        //Gera todos os 3 posts automaticamente

                        if ($tags) {
                            $tag_ids = array(); $j=0;
                            foreach($tags as $individual_tag) $tag_ids[$j++] = $individual_tag->term_id;

                            $args=array(
                            'tag__in' => $tag_ids,
                            'post__not_in' => array($post->ID),
                            'posts_per_page'=> $nposts, // Number of related posts to display.
                            'post_type' => $custom_posts_types,
                            'orderby' => 'rand'
                            //'caller_get_posts'=>1
                            );

                            $loop = new WP_Query($args);

                            if(!$loop->have_posts()){
                                $args=array(
                                    'post__not_in' => array($post->ID),
                                    'posts_per_page'=> $nposts, // Number of related posts to display.
                                    'post_type' => $post->post_type,
                                    'orderby' => 'rand'
                                    //'caller_get_posts'=>1
                                );
                            }
                        }else{
                            if($post->post_type == 'episodios'){
                                $episodio_post_type_parent = get_episodio_pai_post_type($post->ID);
                                $parent_type = get_post_type_object( $episodio_post_type_parent );
	                            $args = array(
		                            'post__not_in'   => array( $post->ID ),
		                            'posts_per_page' => $nposts, // Number of related posts to display.
		                            'post_type'      => $parent_type->name,
		                            'orderby'        => 'rand'
		                            //'caller_get_posts'=>1
	                            );
                            }else {
	                            $args = array(
		                            'post__not_in'   => array( $post->ID ),
		                            'posts_per_page' => $nposts, // Number of related posts to display.
		                            'post_type'      => $post->post_type,
		                            'orderby'        => 'rand'
		                            //'caller_get_posts'=>1
	                            );
                            }
                        }

                        //$related_query = array('post_type' => $custom_posts_types, 'posts_per_page' => $nposts, );
                        //$loop = new WP_Query(array('post__in' => $result, 'post_type' => $custom_posts_types, 'posts_per_page' => $nposts));
                    }


                    //WPQuery
                    $do_not_duplicate = array();
                    $loop = new WP_Query($args);
                    while ($loop->have_posts()) : $loop->the_post();

                        if(!in_array($post->ID, $do_not_duplicate)){

                        ?>

                        <div class="related-post-thumb">

                            <div class="post-thumb-box"><p class="category">
		                            <?php
		                            $post = get_post( $idpost );
		                            // get post type by post
		                            $post_type = $post->post_type;
		                            // get post type taxonomies
		                            $taxonomies = get_object_taxonomies( $post_type );
		                            // var_dump($taxonomies);
		                            $taxonomy = $taxonomies[1]; //Pula a taxonomie de tags.
		                            //var_dump($taxonomy);
		                            $terms = get_the_terms( $post->ID, $taxonomy );
		                            //var_dump($terms);

		                            if ( $terms ) {
			                            foreach ( $terms as $term ) {
				                            $term_link = get_term_link( $term ); ?>
                                            <a href="<?php echo $term_link; ?>"><?php echo $term->name;
					                            if ( next( $terms ) ) {
						                            echo ', ';
					                            } ?></a>
			                            <?php }//end foreach terms
			                            ?>
		                            <?php } else {
			                            $post_type = get_post_type_object( get_post_type( $post ) );

			                            $parent_post_type = get_episodio_pai_post_type( $post->ID );

			                            $post_type_parent = get_post_type_object( $parent_post_type );

			                            if(strtolower($post_type->labels->name) == 'posts'){
				                            $category_post = get_the_category();
				                            ?>
                                            <a href="<?php echo get_category_link($category_post[0]->term_id); ?>"><?php echo $category_post[0]->name; ?></a>
			                            <?php } elseif ( $post_type_parent ) { ?>
                                            <a href="<?php echo get_post_type_archive_link( $post_type_parent->has_archive ); ?>"><?php echo $post_type_parent->labels->name; ?></a>
			                            <?php } else { ?>
                                            <a href="<?php echo get_post_type_archive_link( $post_type->name ); ?>"><?php echo $post_type->labels->name; ?></a>
			                            <?php }?>

		                            <?php } ?>
                            </p>
                            <p class="title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></p>
                            </div>

                    </div><!-- end col -->


                    <?php } $do_not_duplicate[] = $post->ID; endwhile; //End while loop ?>
            <?php wp_reset_query(); wp_reset_postdata(); ?>


                    <?php
                        if($result_posts>0 && $result_posts <3){

                        if($tags){
                                $tag_ids = array(); $j=0;
                                foreach($tags as $individual_tag) $tag_ids[$j++] = $individual_tag->term_id;
                                $args=array( 'tag__in' => $tag_ids,'post__not_in' => array($post->ID), 'posts_per_page'=> $nrposts, 'post_type' => $custom_posts_types,'orderby' => 'rand');

                            }//end if tags
                            //WpQuery
                    $loop = new WP_Query($args);
                    while ($loop->have_posts()) : $loop->the_post();

                        if(!in_array($post->ID, $do_not_duplicate)){

                        ?>


                        <div class="related-post-thumb">
                            <div class="post-thumb-box"><a href="<?php the_permalink(); ?>">
<!--                                    --><?php //the_post_thumbnail('related_post_thumbnail', array('class' => 'img-responsive')); ?><!--</a>-->
                            </div>
                            <div class="post-thumb-box"><p class="category">
		                            <?php
		                            $post = get_post( $idpost );
		                            // get post type by post
		                            $post_type = $post->post_type;
		                            // get post type taxonomies
		                            $taxonomies = get_object_taxonomies( $post_type );
		                            // var_dump($taxonomies);
		                            $taxonomy = $taxonomies[1]; //Pula a taxonomie de tags.
		                            //var_dump($taxonomy);
		                            $terms = get_the_terms( $post->ID, $taxonomy );
		                            //var_dump($terms);

		                            if ( $terms ) {
			                            foreach ( $terms as $term ) {
				                            $term_link = get_term_link( $term ); ?>
                                            <a href="<?php echo $term_link; ?>"><?php echo $term->name;
					                            if ( next( $terms ) ) {
						                            echo ', ';
					                            } ?></a>
			                            <?php }//end foreach terms
			                            ?>
		                            <?php } else {
			                            $post_type = get_post_type_object( get_post_type( $post ) );

			                            $parent_post_type = get_episodio_pai_post_type( $post->ID );

			                            $post_type_parent = get_post_type_object( $parent_post_type );

			                            if ( $post_type_parent ) { ?>
                                            <a href="<?php echo get_post_type_archive_link( $post_type_parent->has_archive ); ?>"><?php echo $post_type_parent->labels->name; ?></a>
			                            <?php } else { ?>
                                            <a href="<?php echo get_post_type_archive_link( $post_type->name ); ?>"><?php echo $post_type->labels->name; ?></a>
			                            <?php }?>

		                            <?php } ?>
                            </p>
                            <p class="title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></p>
                            </div>
                        </div>



                    <?php } $do_not_duplicate[] = $post->ID; endwhile; //End while loop ?>
                            <?php wp_reset_query(); wp_reset_postdata(); ?>




                <?php }//end if result_posts
                    ?>


    </div>