            </section>
            <div id="regular-footer" class="row home-section footer-section">
            	<div class="col-xs-12">
            		<div class="home-footer">
            			<div class="row">
            				<div class="col-xs-12">
            					<img src="<?php bloginfo('template_directory'); ?>/images/logo-footer.png">
                                <p class="p-footer text-center">A Rádio Batuta é a rádio de internet do Instituto Moreira Salles</p>
                                <p class="p-footer text-center">As obras veiculadas na programação da Rádio Batuta são protegidas pelos direitos de autor e/ou conexos.</p>
                                <p class="p-footer text-center"><a class="link-footer" href="https://www.facebook.com/radiobatuta/" target="_blank">Facebook</a> | <a class="link-footer" href="mailto:radiobatuta@ims.com.br">radiobatuta@ims.com.br</a> | <a class="link-footer" href="<?php bloginfo('rss2_url'); ?>" target="_blank">RSS</a></p>
            				</div>
            			</div><!-- end row -->
            		</div><!-- end home-footer -->
            	</div><!-- end col -->
<!--            	<div class="col-md-4">-->
<!--            		<div class="home-footer-social">-->
<!--            			<div class="fb-page" data-href="https://facebook.com/radiobatuta" data-width="340px" data-height="224" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://facebook.com/radiobatuta"><a href="https://facebook.com/radiobatuta">Rádio Batuta</a></blockquote></div></div>-->
<!--            		</div>-->
<!--            	</div>-->
            </div><!-- end row home-section-->
            <!-- footer-single -->
<!--            <div id="footer-single" class="coluna-central">-->
<!--	            <div class="home-footer">-->
<!--		            <img src="--><?php //bloginfo('template_directory'); ?><!--/images/home-footer-logo.png">-->
<!---->
<!--		            <ul>-->
<!--			            --><?php //$items = wp_get_nav_menu_items( 'Home Footer 1');  ?>
<!--			            --><?php //foreach($items as $item){?>
<!--				            <li>-->
<!--					            <a href="--><?php //echo $item->url; ?><!--">--><?php //echo $item->title; ?><!--</a>-->
<!--				            </li>-->
<!--			            --><?php //} //end foreach ?>
<!--		            </ul>-->
<!---->
<!--		            <ul>-->
<!--			            --><?php //$items = wp_get_nav_menu_items( 'Home Footer 2');  ?>
<!--			            --><?php //foreach($items as $item){?>
<!--				            <li>-->
<!--					            <a href="--><?php //echo $item->url; ?><!--">--><?php //echo $item->title; ?><!--</a>-->
<!--				            </li>-->
<!--			            --><?php //} //end foreach ?>
<!--		            </ul>-->
<!---->
<!--	            </div><!-- end home-footer -->
<!---->
<!--            </div>-->
            <!-- end footer-single -->
        </div><!-- end container main-container (header) -->
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e443f7e56ec0078"></script>
