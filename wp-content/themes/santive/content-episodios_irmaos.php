<div class="post-playlist-content">
    <ul class="playlist">
            <?php
            $array_episodios = array();
            $episodios_irmaos = get_episodios_irmaos($post->ID, $_SESSION['current_post_type']);
            foreach ($episodios_irmaos as $episodio):
                $loop_nomeEpisodio1 = new WP_Query("p=".$episodio ."&post_type=episodios");
                while($loop_nomeEpisodio1->have_posts()) : $loop_nomeEpisodio1->the_post();
                    $array_episodios[get_the_title()]=get_the_permalink();
                    endwhile;
                    wp_reset_postdata();
                    wp_reset_query();
            endforeach;
                    natsort($array_episodios);

                foreach($array_episodios as $key=>$episodio2):

                ?>

                <li class="playlist-item">
                    <img src="<?php bloginfo('template_url') ?>/images/plus-icon.png"><a class="ajax episodios-title" href="<?php echo $episodio2; ?>" class="playlist-item-link"><?php echo $key; ?></a>
                </li>

            <?php  endforeach; ?>
    </ul>
</div>