<!--Header -->
<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') { ?>
    <script type="text/javascript">
        jQuery('body').removeClass('single');
    </script>

    <?php
    //is ajax
    //Dont load Header
} else {
    get_header();
}


checkCookie();?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right');
bloginfo(); ?>"/>

<div class="row">
    <div class="home-container">
        <div class="home-section">

            <div class="slider-featured-post">

                <div id="flexslider1" class="flexslider">
                    <ul class="slides">
                        <?php
                        wp_reset_postdata();
                        $countHistory = 0;
                        //WpQuery
                        $loop = new WP_Query(array('post_type' => 'destaque', 'posts_per_page' => -1));
                        while ($loop->have_posts()) : $loop->the_post();
                            //Pega o id do post selecionado no destaque
                            $idpost = rwmb_meta('batuta_posts', array('type' => 'select_advanced'), get_the_ID());
                            $custom_posts_types = get_post_type($idpost);
                            $loop2 = new WP_Query(array('p' => $idpost, 'post_type' => $custom_posts_types, 'posts_per_page' => 1));
                            while ($loop2->have_posts()) {
                                $loop2->the_post();
                                ?>
                                <li>
                                    <div class="slider-featured-post-image">
                                        <a class="ajax" id="<?php echo 'slide-' . $countHistory++; ?>"
                                           href="<?php the_permalink(); ?>"><?php the_post_thumbnail('destaque_thumbnail', array('class' => 'img-responsive')); ?></a>
                                    </div>
                                    <div class="slider-featured-post-content">
                                        <p class="category">
                                            <?php
                                            $post = get_post($idpost);
                                            // get post type by post
                                            $post_type = $post->post_type;
                                            // get post type taxonomies
                                            $taxonomies = get_object_taxonomies($post_type);
                                            // var_dump($taxonomies);
                                            $taxonomy = $taxonomies[1]; //Pula a taxonomie de tags.
                                            //var_dump($taxonomy);
                                            $terms = get_the_terms($post->ID, $taxonomy);
                                            //var_dump($terms);
                                            if ($terms) {
                                                foreach ($terms as $term) {
                                                    $term_link = get_term_link($term); ?>
                                                    <a href="<?php echo $term_link; ?>"><?php echo $term->name;
                                                        if (next($terms)) echo ', '; ?></a>

                                                <?php }//end foreach terms
                                            } else {
                                                $post_type_object = get_post_type_object($post_type);
                                                if($post_type_object->label != 'Posts'){
                                                ?>
                                                <a href="<?php echo get_post_type_archive_link($post_type); ?>"><?php echo $post_type_object->label; ?></a>
                                            <?php } else{
                                                    $category_destaque = get_the_category();
                                                    ?>
                                                    <a href="<?php echo get_category_link($category_destaque[0]->term_id); ?>"><?php echo $category_destaque[0]->name; ?></a>
                                                <?php }
                                                }//endif
                                            ?>
                                        </p>
                                        <h1 class="title"><a class="ajax"
                                                             href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </h1>
                                        <p class="content"><?php echo getExcerpt(get_the_excerpt()); ?></p>
                                        <!--                                        <p class="date">-->
                                        <?php //the_time('d \d\e F \d\e Y');
                                        ?><!--</p>-->
                                    </div>
                                </li>
                            <?php } //End while loop2
                        wp_reset_postdata();

                        endwhile;//End while loop
                        wp_reset_postdata();
                        ?>
                    </ul>
                </div><!-- end flexslider -->
            </div><!-- end slider-featured-post -->

            <!-- Inserindo player-ao-vivo -->
<!--            --><?php //get_template_part('content', 'player-aovivo'); ?>


        </div><!-- end home-section -->

        <div class="home-section">
            <div class="grid-wrap">
                <?php
                $custom_posts_types = get_custom_post_types(false); //Lista de todos os custom post types
                $home_posts = 0; //Contador de posts dos destaques na home (precisa contabilizar 6)
                $home_posts_ids = array(); //Lista dos ids que já estão na home

                //Consulta todos os destaques
                $destaque_loop = new WP_Query(array('post_type' => 'destaque_home', 'posts_per_page' => 6));
                $i = 0;
                while ($destaque_loop->have_posts()) : $destaque_loop->the_post();
                    $home_posts++;
                    //Pega o id do post daquele destaque.
                    $idpost = rwmb_meta('batuta_posts');
                    $home_posts_ids[$i++] = $idpost;

                    //Consulta o post com o id recuperado.
                    $destaque_post_loop = new WP_Query(array('p' => $idpost, 'post_type' => $custom_posts_types));
                    while ($destaque_post_loop->have_posts()) : $destaque_post_loop->the_post();
                        ?>

                        <div class="col-xs-12 col-sm-6 col-md-4 grid-item">
                            <div class="home-featured-post">
                                <div class="home-featured-post-image">
                                    <!-- <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('home_post_thumbnail', array('class' => 'img-responsive', 'media' => '(min-width: 800px)')); ?></a>  -->
                                    <a href="<?php the_permalink(); ?>"><?php $post = get_the_ID();
                                        the_post_thumbnail_responsive($post, 'home_post_thumbnail'); ?></a>
                                </div>
                                <div class="home-featured-post-content">
                                    <p class="category">
                                        <?php
                                        $post = get_post($idpost);
                                        // get post type by post
                                        $post_type = $post->post_type;
                                        // get post type taxonomies
                                        $taxonomies = get_object_taxonomies($post_type);
                                        //var_dump($taxonomies);
                                        $taxonomy = $taxonomies[1]; //Pula a taxonomie de tags.
                                        //var_dump($taxonomy);
                                        $terms = get_the_terms($post->ID, $taxonomy);
                                        if ($terms) {
                                            foreach ($terms as $term) {
                                                $term_link = get_term_link($term); ?>
                                                <a href="<?php echo $term_link; ?>"><?php echo $term->name;
                                                    if (next($terms)) echo ', '; ?></a>
                                            <?php }//end foreach terms
                                        } else {
                                            $post_type_object = get_post_type_object($post_type)
                                            ?>
                                            <a href="<?php echo get_post_type_archive_link($post_type); ?>"><?php echo $post_type_object->label; ?></a>
                                        <?php }//endif ?>
                                    </p>
                                    <h1 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                                    <p class="content"><?php echo getExcerpt(get_the_excerpt()); ?></p>
                                    <!--                                    <p class="date">-->
                                    <?php //the_time('d \d\e F \d\e Y');?><!--</p>-->
                                </div><!-- end home-featured-post-content -->
                            </div><!-- end home-featured-post -->
                        </div> <!-- end col -->

                        <?php
                    endwhile; //End while destaque_post_loop
                    wp_reset_query();
                    wp_reset_postdata();
                endwhile;//End while destaque_loop
                wp_reset_query();
                wp_reset_postdata(); ?>


                <!-- Automatico -->
                <?php
                if ($home_posts < 6) {

                    $total_post = 6 - $home_posts;

                    //Consulta todos os posts
                    $custom_posts_types = get_custom_post_types(false);
                    $destaque_auto_loop = new WP_Query(array('post_type' => $custom_posts_types, 'posts_per_page' => $total_post, 'post__not_in' => $home_posts_ids, 'order' => 'DESC', 'orderby' => 'date'));
                    while ($destaque_auto_loop->have_posts()) : $destaque_auto_loop->the_post(); ?>
                        <div class="col-xs-12 col-sm-6 col-md-4 grid-item">
                            <div class="home-featured-post">
                                <div class="home-featured-post-image">
                                    <!-- <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('home_post_thumbnail', array('class' => 'img-responsive')); ?></a> -->
                                    <a href="<?php the_permalink(); ?>"><?php $post = get_the_ID();
                                        the_post_thumbnail_responsive($post, 'home_post_thumbnail'); ?></a>
                                </div>
                                <div class="home-featured-post-content">
                                    <p class="category">
                                        <?php
                                        $post = get_the_ID();
                                        // get post type by post
                                        $post_type = $post->post_type;
                                        // get post type taxonomies
                                        $taxonomies = get_object_taxonomies($post_type);
                                        //var_dump($taxonomies);
                                        $taxonomy = $taxonomies[1]; //Pula a taxonomie de tags.
                                        //var_dump($taxonomy);
                                        $terms = get_the_terms($post->ID, $taxonomy);
                                        if ($terms) {
                                            foreach ($terms as $term) {
                                                $term_link = get_term_link($term); ?>
                                                <a href="<?php echo $term_link; ?>"><?php echo $term->name;
                                                    if (next($terms)) echo ', '; ?></a>
                                            <?php }//end foreach terms
                                        } else {
                                            $post_type_object = get_post_type_object($post_type)
                                            ?>
                                            <a href="<?php echo get_post_type_archive_link($post_type); ?>"><?php echo $post_type_object->label; ?></a>
                                        <?php }//endif
                                        ?>
                                    </p>
                                    <h1 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                                    <p class="content"><?php echo getExcerpt(get_the_excerpt()); ?></p>
                                    <!--                                <p class="date">-->
                                    <?php //the_time('d \d\e F \d\e Y');?><!--</p>-->
                                </div><!-- end home-featured-post-content -->
                            </div><!-- end home-featured-post -->
                        </div> <!-- end col -->
                        <?php
                    endwhile; //End while destaque_auto_post_loop
                    wp_reset_query();
                    wp_reset_postdata();

                }//end if home_posts ?>
            </div><!-- end grid-wrap -->
        </div><!-- end home-section -->

    </div><!-- end home-container -->
</div><!-- end row -->
<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
    //is ajax
}else{ ?>

    <?php  get_footer(); ?>

    </div>

    <?php wp_footer(); ?>

    <!-- Inserindo player -->
    <?php get_template_part('content', 'player' ); ?>

    <script type="text/javascript" src="<?php echo get_bloginfo('template_url') ?>/js/player.js"></script>

    </body>

    </html>



<?php }?>