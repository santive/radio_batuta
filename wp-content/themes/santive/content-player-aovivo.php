<div id="menu-aovivo" class="menu-aovivo">
    <span style="text-decoration: none; color: #000;">
        <div class="menu-aovivo-tab">
            <div class="menu-aovivo-tab-icon"><img src="<?php bloginfo( 'template_directory' ); ?>/images/som.png"
                                                   alt=""></div>
            <div class="menu-aovivo-tab-label">BATUTA 24H</div>
        </div>
        <div class="menu-aovivo-content">
            <div class="menu-aovivo-content-wrap">
				<?php //WpQuery
				$loop_streaming = new WP_Query( array( 'post_type' => 'streaming', 'posts_per_page' => 2 ) );
				//r_dump($loop_streaming);
				while ( $loop_streaming->have_posts() ) : $loop_streaming->the_post();
					$postid = get_the_ID(); ?>
                    <a href="#" id="<?php echo $postid; ?>" class="streaming-link" onclick="streaming(jQuery(this));" data-post-url="<?php echo rwmb_meta( 'batuta_streaming', $postid ) ?>" data-post-id="<?php echo $postid; ?>"><?php echo get_the_title( $postid ); ?></a>
				<?php endwhile;
				wp_reset_query();
				wp_reset_postdata(); ?>
            </div>
        </div>
    </span>
</div>