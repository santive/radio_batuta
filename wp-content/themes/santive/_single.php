<!-- Header -->
<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
    //is ajax
    //Dont load Header
} else {
    get_header();
} ?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right'); bloginfo(); ?>" />

<?php $selecao = true; ?>

<?php get_template_part('breadcrump'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="post-featured">
                <div id="post-featured-image">
                    <div class="social-media">
                        <ul>
                            <!--                            <li><a href="" class="synved-social-provider-link"></a></li>-->
                            <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>"
                                   class="synved-social-provider-google_plus" target="_blank"></a></li>
                            <li>
                                <a href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&title=<?php the_title(); ?>"
                                   class="synved-social-provider-facebook" target="_blank"></a></li>
                            <li>
                                <a href="http://twitter.com/intent/tweet?status=<?php the_title(); ?>+<?php the_permalink(); ?>"
                                   class="synved-social-provider-twitter" target="_blank"></a></li>
                            <li>
                                <span class="facebook-counter">

                                    <?php echo fb_share(get_permalink()); ?>

                                </span>
                            </li>
                        </ul>
                    </div>
                    <?php the_post_thumbnail('post_thumbnail', array('class' => 'img-responsive')); ?>
                </div>
                <div id="post-featured-content">
                    <div class="post-featured-content-title">
                        <h1 class="title"><?php the_title(); ?></h1>
                        <?php
                        $posttags = get_the_tags();
                        if ($posttags) { ?>
                            <div class="tag">
                                <p>
                                    <?php
                                    //var_dump($posttags);
                                    foreach ($posttags as $tag) { ?>
                                        <?php $taglink = get_term_by('name', $tag->name, 'post_tag'); ?>
                                        <a href="<?php echo get_tag_link($taglink); ?>"><?php echo $tag->name;
                                            if (next($posttags)) echo ', '; ?></a>
                                    <?php } ?>
                                </p>
                            </div>
                        <?php }//end if tags ?>
                    </div>
                </div>
            </div>
            <!-- end post-featured -->
        </div>
        <!-- end col -->
    </div><!-- end row -->

    <div class="row">

        <?php
        $postid = get_the_ID();
        //var_dump($postid);
        $files = rwmb_meta('batuta_file', 'type=file', $postid);
        if ($files) {
            $selecao = true;
        } else {
            $selecao = false;
        }
        //var_dump($selecao);
        ?>

        <?php if ($selecao) {
            $col = 'col-md-8';
        } else {
            $col = 'col-md-12';
        } ?>
        <div class="<?php echo $col; ?>">
            <div class="post-content">
                <!-- Nav tabs -->
                <ul class="nav nav-pills" role="tablist">

                    <?php $fields = get_fields();
                    $fieldi = 0; ?>
                    <?php if ($fields != "") { ?>
                        <?php foreach ($fields as $field_name => $value) { ?>
                            <?php if (trim($value) !== "") { ?>
                                <?php $field = get_field_object($field_name, false, array('load_value' => false)); ?>
                                <li role="presentation" class="<?php if ($fieldi == 0) {
                                    echo 'active';
                                    $fieldi++;
                                } else {
                                    $fieldi++;
                                } ?>"><a href="<?php echo "#" . $field_name; ?>"
                                         aria-controls="<?php echo $field_name; ?>" role="tab"
                                         data-toggle="tab"><?php echo $field["label"]; ?></a></li>
                            <?php } //end if ?>
                        <?php }//end foreach ?>
                    <?php }//end if ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php //var_dump( $fields ); ?>
                    <?php
                    if ($fields != "") {
                        $fieldi = 0;
                        foreach ($fields as $field_name => $value) {
                            if(trim($value) != ""){
                                $field = get_field_object($field_name, false, array('load_value' => false)); ?>
                                <div role="tabpanel" class="tab-pane <?php if ($fieldi == 0) {
                                    echo 'active';
                                    $fieldi++;
                                } else {
                                    $fieldi++;
                                } ?>" id="<?php echo $field_name; ?>">
                                    <?php echo $value; ?>
                                </div>
                            <?php } //end if
                        } //end foreach
                    }//end if ?>
                </div>
            </div>
            <!-- end post-content -->
        </div>
        <!-- end col -->
        <?php //var_dump($selecao); ?>
        <?php if ($selecao) { ?>
            <div class="col-md-4 playlist-wrapper">
                <div class="post-playlist">
                    <div class="post-playlist-title">

                        <?php
                        //$pname = rwmb_meta('batuta_playlist_name') == '' ? 'Playlist' : rwmb_meta('batuta_playlist_name');
                        $pdescription = rwmb_meta('batuta_playlist_description');
                        ?>
                        <p class="title">Playlist</p>

                        <p class="sub-title"><?php echo $pdescription; ?></p>
                    </div>
                    <?php get_template_part('content', 'playlist'); ?>
                </div>
            </div>
        <?php }//end if selecao playlist ?>




<!--        <div class="col-md-8">-->
            <div class="related-posts">

                <div class="row">
                    <div class="col-md-12">
                        <div class="related-posts-header">
                            <p>CONTEÚDO RELACIONADO</p>
                        </div>
                    </div>
                </div>

                <?php get_template_part('content', 'relatedpost'); ?>


            </div>
            <!-- end related-posts -->
<!--        </div>-->
        <!-- end col -->


    </div><!-- end row -->


    <?php wp_reset_query();
    wp_reset_postdata(); ?>
<?php endwhile; ?>
<?php endif; ?>


<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
    //is ajax
    //Dont load footer
} else {
    get_footer();
} ?>
<!-- Footer -->