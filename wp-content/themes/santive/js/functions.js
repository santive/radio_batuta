jQuery.noConflict();
jQuery(document).ready(function($){

    $(".coletanea-container .post-playlist").mCustomScrollbar({
        axis:"y",
        theme: "dark-2"
    });

  //Start em todas as funcoes
  startFunctions();
  startFixedFunctions();

  $('.flex-control-nav li a').attr('href', '#');
  $('body').on('click', '.flex-control-nav li a', function(event){
    event.preventDefault();
    return false;
  });

/* ==========================================================================
    Funcoes que necessitam ser executadas com o loading de cada página. Ex: Flexslider.
    ========================================================================== */
    function startFunctions(){

        $('img').each(function(){
            $(this).attr('title', $(this).attr('alt'));
        });

        $(".coletanea-container .post-playlist").mCustomScrollbar({
            axis:"y",
            theme: "dark-2"
        });

      document.body.scrollTop = document.documentElement.scrollTop = 0;

      $('.content-infinite-scroll').infinitescroll({
        navSelector  : ".navigation",
          // selector for the paged navigation (it will be hidden)
          nextSelector : ".navigation a",
          // selector for the NEXT link (to page 2)
          itemSelector : ".item-infinite-scroll",
          // selector for all items you'll retrieve
          loading: {
            finished: undefined,
            finishedMsg: null,
            msgText: "",
            img: "http://radio.ims.com.br/wp-content/uploads/2014/05/ajax-loader.gif"
          },
        });

      //Dropdown de Episódios
  $('#post-episodios .post-playlist-title').click(function(){
    $(this).next('.post-playlist-content').toggle('fast');
    $(this).find('.veja-mais').toggle('fast');
    $(this).find('i').toggleClass('fa-plus fa-minus');
    $('.post-playlist .post-playlist-title p').toggleClass('show');
  });

      $('#post-audios .post-playlist-title').click(function(){
        $(this).next('.post-playlist-content').toggle('fast');
        $(this).find('.veja-mais').toggle('fast');
        $(this).find('i').toggleClass('fa-plus fa-minus');
        $('.post-playlist .post-playlist-title p').toggleClass('show');
      });

      $('#post-selecoes .post-playlist-title').click(function(){
        $(this).next('.post-playlist-content').toggle('fast');
        $(this).find('.veja-mais').toggle('fast');
        $(this).find('i').toggleClass('fa-plus fa-minus');
        $('.post-playlist .post-playlist-title p').toggleClass('show');
      });

      $('#post-episodios-audio .post-playlist-title').click(function(){
        $(this).next('.post-playlist-content').toggle('fast');
        $(this).find('.veja-mais').toggle('fast');
        $(this).find('i').toggleClass('fa-plus fa-minus');
        $('.post-playlist .post-playlist-title p').toggleClass('show');
      });

      // $('.playlist-item').hover(function(){
      //   $(this).find('img').show();
      // }, function() {
      //   $(this).find('img').hide();
      // });

      $('#menu').removeClass('is_open');
      $('#menu-button').removeClass('open');

      $(".menu-desktop ul.sub-menu").each(function(){
        $(this).removeClass('sub-menu');
        $(this).addClass('sub-menu-2');
      });

      /*Menu Dropdown*/
      $('.menu-item .sub-menu-2').hide().removeClass('.sub-menu-2');
      $('.menu-item').hover(
        function () {
          $('.sub-menu-2', this).stop().slideDown(100);
        },
        function () {
          $('.sub-menu-2', this).stop().slideUp(100);
        }
        );

      $('.container').click(function(e){

        var target = $(e.target);
        if(!target.is('#menu')) {
          //$('#menu-button').removeClass('open');
          //$('#menu').removeClass('is_open');
        }
      });

      //Limpa o campo de Busca
      $('.ipt-search').val('');


    //Change title
    document.title = $('#page_title').val();

    //flexslider
    $('#flexslider1').flexslider({
      animation: "slide",
      controlNav: true,
      controlsContainer: $(".custom-controls-container"),
      customDirectionNav: $(".custom-navigation a")
    });


    $('#flexslider2').flexslider({
        animation: "slide",
        animationLoop: true,
        itemMargin: 6,
        itemWidth: 140
    });


   //Boostrap Panel tabs
   $('#myTabs a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
  });

    //Adiciona padding extra no body
    var footer_height = $('#footer').innerHeight();
    $('body').css("padding-bottom", footer_height+'px');

      $('#searchform input').click(function(event){
        event.preventDefault();
      });

    //Playlist
    $('li.playlist-item .playlist-item-link').on('click', function(event) {
      event.preventDefault();
      objMedia = objMediaPost;
    }); //end clickPlaylist

    //Add Ajax Class aos itens dinâmicos (thumbs, titles, categories, etc)
    $('.category a').addClass('ajax');
    $('.home-container a').addClass('ajax');
    $('.title a').addClass('ajax');
    //$('img').parent('a').addClass('ajax');
    $('.tag a').addClass('ajax');
    $('.category-container a').addClass('ajax');
    $('.category-selecoes-left a').addClass('ajax');
    $('#breadcrump a').addClass('ajax');
    $('.post-thumb a').addClass('ajax');
    $('.home-featured-post-image a').addClass('ajax');
    $('.pagination a').addClass('ajax');
      $('.menu-desktop a').addClass('ajax');
      // Esse o footer antigo $('.home-footer').addClass('ajax');
      $('.home-section').addClass('ajax');
      $('.addthis_sharing_toolbox').addClass('ajax');
      $('.at-share-tbx-element').addClass('ajax');

      //Logo Batuta
      $('.logo a').addClass('ajax');

      //Logo Header
      $('.img-logo a').addClass('ajax');

      $('.prev-link a').addClass('ajax');
      $('.next-link a').addClass('ajax');

        $('.menu-desktop.menu-desktop-top a').removeClass('ajax');
        $('.post-playlist-content a').removeClass('ajax');
        $('a.link-footer').removeClass('ajax');
        $('.category-streaming-description .play-container a').removeClass('ajax');
        // Remove class ajax
        $('#link-ims').removeClass('ajax');




    //var $container = $('.grid-wrap');
    //$container.imagesLoaded(function () {
    //    $container.masonry({
    //        itemSelector: '.grid-item',
    //        columnWidth: '.grid-item',
    //        transitionDuration: 0
    //    });
    //});

  }//end startFunctions();


/* ==========================================================================
    Funcoes que necessitam ser executadas uma única vez.
    ========================================================================== */
    function startFixedFunctions(){

        // Toogle de menu ao vivo
        $('#menu-aovivo > span').on('click', function(event) {
            event.preventDefault();
            $('#menu-aovivo').toggleClass('is_open');
            return false;
        });

      //Busca
      $('#btn-busca').click(function(){
        $('.search').toggle('slow');
      });

      $('#searchsubmit').click(function(){
        $('.search').toggle();
      });

      $('#menu-button').click(function(){
        $('#menu').toggleClass('is_open');
      });

      //Efeito do Menu
      $('.menu-button').click(function(){
        $('.menu-button').toggleClass('open');
      });

      //Add Ajax Class aos itens estáticos (menu, footer)

      //Logotipo do Site
      //$('.header a').addClass('ajax');

      //Menu Desktop
      //$('.menu-desktop li a').addClass('ajax');

      $('#menu a').addClass('ajax');
      $('.fb-page').addClass('ajax');
      // $('.colunacentral.home-footer').addClass('ajax');
        $('a.link-footer').removeClass('ajax')
        $('.category-streaming-description .play-container a').removeClass('ajax');

  }//end startFixedFunctions();


/* ==========================================================================
                        HISTORY HTML API
                        ========================================================================== */

                        /* check for support before we move ahead */
                        if (typeof history.pushState !== "undefined") {
                          var historyCount = 0;

                          $(document).on('click', 'a.ajax', function(event){
                            event.preventDefault();

                            if($(this).attr('href') == "#"){

                            }else{                              
                              var href = $(this).attr('href');
                              goTo(href);
                              history.pushState(null, null, href);
                              return false;
                            }
                          });

                          $('#searchform').submit(function(e){
                            e.preventDefault();
                            var href = $(this).attr('action');
                            var input = $('#s').val();
                            href = href + '/?s=' + input;

                            goTo(href);
                            history.pushState(null, null, href);
                            return false;

                          });

                          window.addEventListener("popstate", function(e) {
                            goTo(location.pathname);
                          });
}//end history support

function goTo(href) {
  $('html').fadeTo('fast', 0.5);
  $.ajax({
    url: href,
    success: function(data) {
      $('html').fadeOut('fast', function(){
        $('#content').html(data);
        $(this).fadeTo('fast', 1);
                //Recarrega Funções na nova página carregada
                startFunctions();
              });
            // update the page title
            //var title = $('#content').first('h1').text();
            //document.title = title;
          }
        });
}//end goTo


}); //end document

/* Função para o player de Grandes Nomes */
function playGrandesNomes(playslistJSON, idMedia, filesSize, objPostParam, event) {
    if(filesSize == 1){
        var hideShow = 'hidePrevNext';
    }else{
        var hideShow = 'showPrevNext';
    }
    objMedia = playslistJSON;
    objPost = objPostParam;
    setMedia(idMedia, objMedia);
    event.preventDefault();
    setTimeout(hideShow,300);
}; //end clickPlaylist