<!-- Header -->
<?php
if ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) AND strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) === 'xmlhttprequest' ) { ?>
    <script type="text/javascript">
        jQuery('body').removeClass('single');
    </script>

	<?php
	//is ajax
	//Dont load Header
} else {
	get_header();
}

checkCookie(); ?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title( '-', true, 'right' );
bloginfo(); ?>"/>

<div class="home-container">

    <div class="row">
        <div class="col-md-12">
            <div class="title-section">
                <h1>Grandes Nomes</h1>
                <p> <?php if ( is_active_sidebar( 'coletaneas' ) ) { ?>
						<?php if ( is_active_sidebar( 'coletaneas' ) && ! dynamic_sidebar( 'coletaneas' ) ) : ?>
						<?php endif; ?>
					<?php } ?>
                </p>

                <?php
                    $args_coletaneas = array(
                      'post_type' => 'coletanea',
                        'posts_per_page' => -1,
                        'orderby' => 'date',
                        'order' => 'DESC',
                    );
                    /* Grandes nomes com carrossel */
                    $loop_coletaneas = new WP_Query($args_coletaneas);
                    if($loop_coletaneas->have_posts()): ?>
                        <div class="flexslider" id="flexslider2">
                            <ul class="slides">
	                            <?php while($loop_coletaneas->have_posts()):
		                            $loop_coletaneas->the_post() ?>
                                    <li style="margin-right: 13px;">
                                        <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('grandes_nomes_list') ?>
                                            <span><?php echo the_title(); ?></span></li></a>
	                            <?php endwhile; wp_reset_postdata(); wp_reset_query(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
            </div>
        </div>
    </div><!-- end row -->

	<?php //WpQuery
	$id   = 0;
	$id0 = 0;
	$loop = new WP_Query( array(
		'post_type'      => 'coletanea',
		'posts_per_page' => -1,
		'orderby'        => 'date',
		'order'          => 'DESC'
	) );
	while ( $loop->have_posts() ) :
		$loop->the_post(); ?>



                <div class="row">
                    <div class="col-md-12">
                        <div class="category-streaming-container coletanea-container">
                            <div class="row">
                                <div class="col-sm-12">

                                </div>
                            </div>
                            <div class="row coletanea-content">
                                <div class="col-md-5">
									<?php the_post_thumbnail( 'streaming_thumbnail', array( 'class' => 'img-responsive' ) ); ?>
                                </div>
                                <div class="col-md-7">
                                    <div class="category-streaming-description coletanea-description">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <h2><a href="<?php the_permalink() ?>"> <?php the_title(); ?></a></h2>
                                                <p><?php echo get_the_excerpt(); ?></p>
                                                <p class="post-date"><?php the_date('d.m.Y'); ?></p>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="post-single-documentario post-playlist" id="post-audios">
                                                    <div class="post-playlist-title">
			                                            <?php

			                                            $pname = rwmb_meta('batuta_playlist_name'); ?>

                                                    </div>
                                                    <div class="post-playlist-content">
                                                        <script>


                                                        </script>
                                                        <ul class="playlist">
			                                                <?php
			                                                $postid = get_the_ID();
			                                                ?>
                                                            <script type="text/javascript">
                                                                var objPost<?php echo $postid;?> = [];
                                                                var objMedia<?php echo $postid;?> = '';
                                                                var objMediaPost<?php echo $postid;?> = [<?php playlist_($postid); ?>];
                                                                setTimeout(function(){
                                                                    objPost<?php echo $postid;?> = {
                                                                        tipo : "<?php echo $objPType->labels->name; ?>",
                                                                        img : '<?php the_post_thumbnail('post_thumbnail', array('class' => 'img-responsive')); ?>',
                                                                        nome : '<?php the_title(); ?>' ,
                                                                        url : '<?php the_permalink(); ?>',
                                                                        category : '/?post_type=<?php echo $objPType->name ?>'
                                                                    };
                                                                },300);
                                                            </script>
                                                            <?php
			                                                $files = rwmb_meta('batuta_file', 'type=file', $postid);
			                                                if(empty($files)){
				                                                $files = rwmb_meta('batuta_file_ep', 'type=file', $postid);
			                                                }
			                                                $idmedia = 0;
			                                                foreach($files as $file){ ?>
                                                                <li class="playlist-item-coletanea">
                                                                    <a id="<?php echo $id0++; ?>"
                                                                       href="javascript:void(0);"
                                                                       class="playlist-item-link-<?php echo $id0; ?> playlist-item-link"
                                                                       onclick="playGrandesNomes(objMediaPost<?php echo $postid;?>, '<?php echo ($idmedia) ?>', <?php echo count($files); ?>, objPost<?php echo $postid;?>, event)"
                                                                       data-post-id="<?php echo $postid; ?>"
                                                                       data-playlist-path="<?php the_permalink(); ?>/playlist.json"
                                                                    >
						                                                <?php echo $file['title']; ?>
                                                                    </a>
                                                                </li>
			                                                <?php $idmedia++;
			                                                } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end category-streaming-container -->
                    </div><!-- end col -->
                </div><!-- end row -->
			<?php
		endwhile;
	wp_reset_query();
	wp_reset_postdata(); ?>


</div><!-- end home-container -->

<?php
if ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) AND strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) === 'xmlhttprequest' ) {
	//is ajax
	// do nothing
} else { ?>

	<?php get_footer(); ?>

    </div>

	<?php wp_footer(); ?>

    <!-- Inserindo player -->
	<?php get_template_part( 'content', 'player' ); ?>

    <script type="text/javascript" src="<?= get_bloginfo( 'template_url' ) ?>/js/player.js"></script>

    </body>

    </html>


<?php } ?>