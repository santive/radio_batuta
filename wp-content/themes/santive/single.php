<!-- Header -->
<?php
if ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) AND strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) === 'xmlhttprequest' ) {
    @session_start(); ?>
	<script type="text/javascript">
		jQuery('body').addClass('single');
	</script>

	<?php
} else {
	get_header();
}

checkCookie();
?>

<?php while(have_posts()): the_post(); ?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title( '-', true, 'right' );
bloginfo(); ?>"/>

<div class="coluna-central-lg">
    <div id="breadcrump" class="font-breadcrump">
        <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
        </div>
    </div>
    <h1><?php the_title();?></h1>
    <p class="post-date"><?php the_date('d.m.Y'); ?></p>
</div>

<div class="coluna-central">
    <?php $post_type_name = get_post_type();
    $_SESSION['current_post_type'] = $post_type_name;
    $in_same_term = true;
    switch ($post_type_name){
        case 'programa':
            $taxonomy_name = 'programa_categories';
            break;
        case 'selecao':
            $taxonomy_name = 'selecao_categories';
	        break;
        case 'documentario':
            $taxonomy_name = 'documentario_categories';
	        break;
        case 'streaming':
            $taxonomy_name = 'streaming_categories';
	        break;
        case 'especiais':
            $taxonomy_name = 'especiais_categories';
	        break;
        default:
	        $in_same_term = false;
            $taxonomy_name = 'category';
    }

    if(has_term('', $taxonomy_name) == false){
        $in_same_term = false;
    }
    ?>

	<?php next_post_link('<span class="prev-link">%link</span>','<p><img src="' . get_bloginfo("template_directory") . '/images/arrow-left.png" /></p><p>Próximo</p>', $in_same_term, '', $taxonomy_name); ?>

	<?php previous_post_link('<span class="next-link">%link</span>','<p><img src="' . get_bloginfo("template_directory") . '/images/arrow-right.png" /></p><p>Anterior</p>', $in_same_term, '' , $taxonomy_name); ?>

    <!--Thumbnail-->
    <?php the_post_thumbnail('post_thumbnail');?>
    <p class="thumb-caption">
        <?php $pdescription = rwmb_meta('batuta_playlist_description'); ?>
        <?php if($pdescription) { ?>
	        <?php echo $pdescription; ?>
        <?php } ?>
    </p>
    <!--/Thumbnail-->

    <?php if(rwmb_meta('batuta_file', 'type=file')) { ?>
    <!--Aúdio-->
    <div class="post-single-documentario post-playlist" id="post-audios">
        <div class="post-playlist-title">
            <?php

            $pname = rwmb_meta('batuta_playlist_name'); ?>
            <p class="title hide">Áudios</p>
            <p class="veja-mais">Ouça</p> <i class="fa fa-minus" aria-hidden="true"></i>
        </div>
        <?php get_template_part( 'content', 'playlist' ); ?>
    </div>
    <!--/Aúdio-->
    <?php } ?>

    <!--Episodios-->
    <?php if(rwmb_meta('batuta_nomeEpisodio', 'type=checkbox_list')) { ?>
        <div class="post-playlist" id="post-episodios">
            <div class="post-playlist-title">
                <p class="title">
                    Episódios
                </p>
                <i class="fa fa-minus" aria-hidden="true"></i>
            </div>
            <?php get_template_part('content', 'episodios'); ?>
        </div>
    <?php } ?>
    <!--/Episodios-->

    <!--Add This-->
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<div class="addthis_inline_share_toolbox_rp1f"></div>
    <!--/Add This-->

    <!--Content-->
    <div class="post-content">
        <?php the_content(); ?>
    </div>
    <!--/Content-->

	<div class="related-posts">
		<!--Tags-->
		<?php
		$posttags = get_the_tags();
		if ( $posttags ) { ?>
			<div class="tag">
				<i class="fa fa-tag fa-1 fa-rotate-90" aria-hidden="true"></i>
				<p>
					<?php
					//var_dump($posttags);
                    $array_tag = array();
					foreach ( $posttags as $tag ) { ?>
						<?php $taglink = get_term_by( 'name', $tag->name, 'post_tag' ); ?>
						<?php $array_tag[] = '<a href="'.get_tag_link( $taglink ) . '">'.$tag->name.'</a>';
					}
					echo implode(', ', $array_tag);
					?>

				</p>
			</div>
		<?php } ?>
		<!--/Tags-->
		<hr>
		<div class="related-posts-header">
			<p>CONTEÚDO RELACIONADO</p>
		</div>
		<?php get_template_part( 'content', 'relatedpost' ); ?>
	</div>
	<hr>
<!--	<div id="disqus_thread"></div>-->
</div>
<!--<script>-->
<!--	/**-->
<!--	 * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.-->
<!--	 * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables-->
<!--	 */-->
<!--	/*-->
<!--	 var disqus_config = function () {-->
<!--	 this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable-->
<!--	 this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable-->
<!--	 };-->
<!--	 */-->
<!--	(function () { // DON'T EDIT BELOW THIS LINE-->
<!--		var d = document, s = d.createElement('script');-->
<!---->
<!--		s.src = '//batuta.disqus.com/embed.js';-->
<!---->
<!--		s.setAttribute('data-timestamp', +new Date());-->
<!--		(d.head || d.body).appendChild(s);-->
<!--	})();-->
<!--	var disqus_developer = 1;-->
<!--</script>-->
<!--<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments-->
<!--		powered by Disqus.</a></noscript>-->

<?php endwhile; ?>
<?php
if ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) AND strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) === 'xmlhttprequest' ) {
	//is ajax
} else {
	get_footer(); ?>

	</div>

	<?php wp_footer(); ?>

	<!-- Inserindo player -->
	<?php get_template_part( 'content', 'player' ); ?>

	<script type="text/javascript" src="<?php echo get_bloginfo( 'template_url' ) ?>/js/player.js"></script>


	</body>

	</html>
<?php } ?>