<!-- Header -->
<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){  ?>
    <script type="text/javascript">
        jQuery('body').removeClass('single');
    </script>

    <?php
    //is ajax
    //Dont load Header
}else{
    get_header(); 
}
checkCookie();?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right'); bloginfo(); ?>" />

<div class="home-content">

    <div class="row">
        <div class="col-xs-12">
            <div class="title-section">
                <h1>Especiais</h1>
                <p><?php echo term_description(); ?></p>
            </div>
        </div>
    </div><!-- end row -->
    
    <div class="row grid-wrap">

    <?php //WpQuery
            $loop = new WP_Query( array( 'post_type' => 'especiais', 'posts_per_page' => -1 ) );
            while ( $loop->have_posts() ) : $loop->the_post();?>

        <div class="col-md-3 grid-item">
            <div class="post-thumb">
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('documentario_thumbnail', array('class' => 'img-responsive')); ?></a>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p><?php echo getExcerpt(get_the_excerpt(), 0, 300); ?></p>
                <p class="date"><?php the_time('d.m.Y'); ?></p>
            </div>            
        </div><!-- end col -->

    <?php endwhile; wp_reset_query(); wp_reset_postdata();?>

    </div><!-- end row -->
    
    
</div><!-- end home-content -->


<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
    //is ajax
}else{ ?>

    <?php  get_footer(); ?>

    </div>

    <?php wp_footer(); ?>

    <!-- Inserindo player -->
    <?php get_template_part('content', 'player' ); ?>

    <script type="text/javascript" src="<?php echo get_bloginfo('template_url') ?>/js/player.js"></script>



    </body>

    </html>



<?php }?>
<!-- Footer -->