<!-- Header -->
<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){ ?>
    <script type="text/javascript">
        jQuery('body').removeClass('single');
    </script>

    <?php
    //is ajax
    //Dont load Header
}else{
    get_header();
}

checkCookie();?>

<?php
//Taxonomie Name
$taxonomy  = "selecao_categories";
?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right'); bloginfo(); ?>" />

<div class="row">

    <?php get_template_part('breadcrump'); ?>

    <div class="grid-wrap">

        <?php

        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

        $term_aux = $term->slug;
        $term_id = $term->term_id;


        $args = array(
            'post_type' => 'selecao',
            'posts_per_page'=> -1,
            'tax_query' => array(
                array(
                    'taxonomy' => $taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $term_id,
                ),
            ),
        );

        $loop = new WP_Query( $args );
        while ($loop->have_posts()) : $loop->the_post();
        ?>

        <div class="col-xs-12 col-sm-12 col-md-6 grid-item">
            <div class="category-selecoes-container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="category-selecoes-left">
                            <!-- <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('selecao_thumbnail', array('class' => 'img-responsive')); ?></a>-->
                            <a href="<?php the_permalink(); ?>"><?php $post = get_the_ID(); the_post_thumbnail_responsive($post, 'selecao_thumbnail'); ?></a>
                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <p><?php echo getExcerpt(get_the_excerpt(), 0, 300); ?></p>
                            <?php
                                $posttags = get_the_tags();
                                if ($posttags) { ?>
                                    <div class="tag">
                                        <p>
                                           <?php
                                           //var_dump($posttags);
                                              foreach($posttags as $tag) { ?>
                                                <?php $taglink = get_term_by('name', $tag->name, 'post_tag'); ?>
                                                <a href="<?php echo get_tag_link($taglink); ?>"><?php echo $tag->name; if( next( $posttags ) ) echo ', ';?></a>
                                            <?php } ?>
                                        </p>
                                   </div>
                               <?php }//end if tags ?>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="category-selecoes-right">
                            <?php get_template_part( 'content', 'playlist' ); ?>
                        </div>
                    </div>
                </div>
            </div><!-- end category-selecoes-container -->
        </div><!-- end col -->


    <?php  endwhile;  wp_reset_query(); wp_reset_postdata();  ?>

    </div><!-- end grid-wrap -->

</div><!-- end row -->


<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
    //is ajax
}else{ ?>

    <?php  get_footer(); ?>

    </div>

    <?php wp_footer(); ?>

    <!-- Inserindo player -->
    <?php get_template_part('content', 'player' ); ?>

    <script type="text/javascript" src="<?php echo get_bloginfo('template_url') ?>/js/player.js"></script>



    </body>

    </html>



<?php }?>
<!-- Footer -->