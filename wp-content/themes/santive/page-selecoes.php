<?php /* Template Name: Página de Seleções */ ?>
<?php get_header(); ?>
<?php 
//Taxonomie Name
$taxonomy  = "selecao_categories";
?>




<div class="row">



<div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="title-section">
                <?php $pagename = get_query_var('pagename');  $post = $wp_query->get_queried_object(); ?>
                <h1><?php echo $post->post_title; ?></h1>
                <p><?php echo $post->post_content; ?></p>
            </div>
        </div>
 </div><!-- end row -->
<?php if($term->parent > 0){ //Child Theme 

    //get_template_part('breadcrump'); 
    $term_aux;
    //Get post term
    $terms = get_terms($taxonomy);
    foreach($terms as $term){
        $term_aux = $term->slug;    
    } ?>

<?php
    $args = array(
        'post_type' => 'selecao',
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => $term_aux,
            ),
        ),
    );

    $loop = new WP_Query( $args );
    while ($loop->have_posts()) : $loop->the_post();
?>


    <div class="col-md-6">
        <div class="category-selecoes-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="category-selecoes-left">
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('selecao_thumbnail', array('class' => 'img-responsive')); ?></a>
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>
                        <?php
                            $posttags = get_the_tags();
                            if ($posttags) { ?>
                        <div class="tag">
                            <p>
                               <?php 
                                  foreach($posttags as $tag) { ?>
                                    <?php $taglink = get_term_by('name', $tag->name, 'post_tag'); ?>
                                    <a href="<?php echo get_tag_link($taglink); ?>"><?php echo $tag->name; if( next( $posttags ) ) echo ', ';?></a>                                   
                                <?php } ?>
                            </p>
                       </div>
                       <?php }//end if tags ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="category-selecoes-right">
                        <ul>
                            <?php

                            $postid = get_the_ID();
                            $files = rwmb_meta('batuta_file', 'type=file', $postid); 
                            foreach($files as $file){ ?>
                                <li><?php echo $file['name']; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- end category-selecoes-container -->
    </div><!-- end col -->

<?php  endwhile;  wp_reset_query(); wp_reset_postdata();  

 }//Parent Theme
else{ ?>

<div class="home-container">


<?php 

$term_aux;

//Get post term
$terms = get_terms($taxonomy);
foreach($terms as $term){
if($term->parent > 0){

$term_aux = $term->slug; 
$term_link = get_term_link($term);    


?>



    <div class="row">
        <div class="col-md-12">           

            <div class="category-container">                
                <div class="category-description">
                    <h2><a href="<?php echo $term_link; ?>"><?php echo $term->name; ?></a></h2>
                    <p><?php echo $term->description ?></p>
                    <p class="destaque"><a href="<?php echo $term_link; ?>">VER TODOS</a></p>
                </div>

                <?php 
                $args = array(
                    'post_type' => 'selecao',
                    'posts_per_page'=> 3,
                    'tax_query' => array(
                        array(
                            'taxonomy' => $taxonomy,
                            'field'    => 'slug',
                            'terms'    => $term_aux,
                            
                        ),
                    ),
                );

                    $loop = new WP_Query( $args );
                    while ($loop->have_posts()) : $loop->the_post();

                    ?>

                     <div class="category-post">
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('selecao_parent_thumbnail', array('class' => 'img-responsive')); ?></a>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <p><?php echo getExcerpt(get_the_excerpt(), 0, 250); ?></p>
                    <span class="destaque"><?php the_time('d F Y');?></span>
                </div>

                <?php  endwhile;  wp_reset_query(); wp_reset_postdata();  ?>
                
            </div><!-- end category-container -->

        </div><!-- end col -->
    </div><!-- end row -->

<?php
 } //end if parent 
} //end foreach terms ?>

</div><!-- end home-container -->


<?php
}// End if/else Parent Theme
?>

</div><!-- end row -->


<?php get_footer(); ?>
<!-- Footer -->     