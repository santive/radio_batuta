<!-- Header -->
<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){ ?>
    <script type="text/javascript">
        jQuery('body').removeClass('single');
    </script>

    <?php
    //is ajax
    //Dont load Header
}else{
    get_header();
}

checkCookie();
?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right'); bloginfo(); ?>" />

<div class="home-content">

    <div class="row">
        <div class="col-xs-12">
            <div class="title-section">
                <h1>Documentários</h1>
                <p> <?php if ( is_active_sidebar( 'documentario' ) ) { ?>
                        <?php if ( is_active_sidebar( 'documentario' ) && !dynamic_sidebar('documentario') ) : ?>
                        <?php endif; ?>
                    <?php } ?>
                </p>
            </div>
        </div>
    </div><!-- end row -->

    <div class="row grid-wrap">

        <div class="documentario-container content-infinite-scroll">
    <?php



    //if ( have_posts() ) : while ( have_posts() ) : the_post();
    //$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    //WpQuery
            $loop = new WP_Query( array( 'post_type' => 'documentario', 'posts_per_page' => 8, 'paged' => $paged) );
            while ( $loop->have_posts() ) : $loop->the_post();
    ?>

        <div class="col-xs-12 col-sm-4 col-md-3 grid-item item-infinite-scroll">
            <div class="post-thumb">
                <!--<a href="<?php the_permalink(); ?>">
                <?php if ( has_post_thumbnail() ) { ?>
                <?php the_post_thumbnail('documentario_thumbnail', array('class' => 'img-responsive')); ?></a>-->
                <a href="<?php the_permalink(); ?>"><?php $post = get_the_ID(); the_post_thumbnail_responsive($post, 'documentario_thumbnail'); ?></a>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p><?php echo getExcerpt(get_the_excerpt(), 0, 300); ?></p>
                <p class="date"><?php the_time('d.m.Y');?></p>

                <div class="navigation"><p><?php posts_nav_link(); ?></p></div>

            </div>

        </div><!-- end col -->

            <!-- Se não tiver imagem -->

                <?php } else { ?>
            <picture class="post-thumbnail">
                <!--[if IE 9]><video style="display: none;">
                <![endif]-->
                <source srcset="<?php bloginfo('template_url'); ?>/images/imagem-padrao-rb.jpg" media="(max-width: 768px)">
                <!--[if IE 9]>
                </video>
                <![endif]-->
                <img class="img-responsive no-image-custom" srcset="<?php bloginfo('template_url'); ?>/images/imagem-padrao-rb.jpg" >
            </picture>


            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <p><?php echo getExcerpt(get_the_excerpt(), 0, 300); ?></p>
            <p class="date"><?php the_time('d.m.Y');?></p>

            <div class="navigation"><p><?php posts_nav_link(); ?></p></div>

        </div>

    </div><!-- end col -->

                <?php } ?>

    <?php endwhile; wp_reset_query(); wp_reset_postdata(); ?>
        </div>
    <?php //endif ?>
    <?php
            $temp_query = $wp_query;
            $wp_query   = NULL;
            $wp_query   = $loop;
     ?>
    </div><!-- end row -->

    <?php
    $wp_query = NULL;
    $wp_query = $temp_query;

    ?>


</div><!-- end home-content -->

<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
    //is ajax
    // do nothing
}else{ ?>

    <?php  get_footer(); ?>

    </div>

    <?php wp_footer(); ?>

    <!-- Inserindo player -->
    <?php get_template_part('content', 'player' ); ?>

    <script type="text/javascript" src="<?php echo get_bloginfo('template_url') ?>/js/player.js"></script>

    </body>

    </html>



<?php }?>