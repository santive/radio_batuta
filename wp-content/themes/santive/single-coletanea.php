<!-- Header -->
<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
	session_start(); ?>
    <script type="text/javascript">
        jQuery('body').addClass('single');
    </script>

	<?php
	//is ajax
	//Dont load Header
}else{
	get_header();
}


checkCookie();?>

<?php while (have_posts()): the_post(); ?>

    <!-- campo que passa o title para o history -->
    <input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right'); bloginfo(); ?>" />

    <div class="coluna-central-lg">
        <div id="breadcrump" class="font-breadcrump">
            <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<?php /* Get post parent */

				$args_doc = array(
					'post_type' => $_SESSION['current_post_type'],
					'meta_query' => array(
						array(
							'key' => 'batuta_nomeEpisodio',
							'value' => get_the_ID(),
						)
					)
				);

				$documentario_pai = new WP_Query($args_doc);
				$count_doc = 0;
				while ($documentario_pai->have_posts()){
					if($i == 0) {
						$documentario_pai->the_post();

						$parent_documentary = get_the_title();
					}else{
						continue;
					}
					$count_doc++;
				}
				wp_reset_postdata();
				wp_reset_query();

				$post_type_object = get_episodio_pai_post_type(get_the_ID());



				?>

                <a href="<?php echo get_post_type_archive_link('coletanea') ?>">Grandes Nomes</a><?php echo $parent_documentary; ?>
            </div>
        </div>
        <h1><?php the_title();?></h1>
    </div>

    <div class="coluna-central">
		<?php
		$link_anterior = null;
		$link_next = null;

		$episodios_irmaos = get_episodios_irmaos(get_the_ID(), $_SESSION['current_post_type'], true);
		$key_in_array = array_search(get_the_ID(), $episodios_irmaos);

		if($episodios_irmaos[$key_in_array - 1]){
			$link_anterior = get_permalink($episodios_irmaos[$key_in_array - 1]);
		}

		if($episodios_irmaos[$key_in_array + 1]){
			$link_next = get_permalink($episodios_irmaos[$key_in_array + 1]);
		}
		?>

		<?php if($link_anterior) { ?>
            <span class="prev-link"><a href="<?php echo $link_anterior ?>" rel="next" class="ajax"><p><img src="<?php echo get_bloginfo('template_directory') . '/images/arrow-left.png' ?>"></p><p>Anterior</p></a></span>
		<?php } ?>

		<?php if($link_next){ ?>
            <span class="next-link"><a href="<?php echo $link_next ?>" rel="next" class="ajax"><p><img src="<?php echo get_bloginfo('template_directory') . '/images/arrow-right.png' ?>"></p><p>Próximo</p></a></span>
		<?php } ?>

        <!--Thumbnail-->
		<?php the_post_thumbnail('post_thumbnail');?>
        <p class="thumb-caption">
			<?php $pdescription = rwmb_meta('batuta_playlist_description'); ?>
			<?php if($pdescription) { ?>
				<?php echo $pdescription; ?>
			<?php } ?>
        </p>
        <!--/Thumbnail-->

		<?php if(rwmb_meta('batuta_file', 'type=file')) { ?>
            <!--Aúdio-->
            <div class="post-playlist" id="post-episodios-audio">
                <div class="post-playlist-title">
					<?php
					$pdescription = rwmb_meta('batuta_playlist_description');
					$pname = rwmb_meta('batuta_playlist_name'); ?>
                    <p class="title">Ouça</p>
                    <i class="fa fa-minus" aria-hidden="true"></i>
                </div>
				<?php get_template_part( 'content', 'playlist' ); ?>
            </div>
            <!--/Aúdio-->
		<?php } ?>

        <!--Episodios-->
		<?php
		$episodios_irmaos = get_episodios_irmaos(get_the_ID(), $_SESSION['current_post_type']);
		if($episodios_irmaos) { ?>
            <div class="post-playlist">
                <div class="post-playlist-title">
                    <p class="title">Episódios</p>
                </div>
				<?php get_template_part('content', 'episodios_irmaos'); ?>
            </div>
		<?php } ?>
        <!--/Episodios-->

        <!--Add This-->
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <div class="addthis_inline_share_toolbox_rp1f"></div>
        <!--/Add This-->

        <!--Content-->
        <div class="post-content">
			<?php the_content(); ?>
        </div>
        <!--/Content-->

        <div class="related-posts">
            <!--Tags-->
			<?php
			$posttags = get_the_tags();
			if ($posttags) { ?>
                <div class="tag">
                    <i class="fa fa-tag fa-1 fa-rotate-90" aria-hidden="true"></i><p>
						<?php
						//var_dump($posttags);
						foreach($posttags as $tag) { ?>
							<?php $taglink = get_term_by('name', $tag->name, 'post_tag'); ?>
                            <a href="<?php echo get_tag_link($taglink); ?>"><?php echo $tag->name; if( next( $posttags ) ) echo ', ';?></a>
						<?php } ?>
                    </p>
                </div>
			<?php } ?>
            <!--/Tags-->
            <hr>
<!--            <div class="related-posts-header">-->
<!--                <p>CONTEÚDO RELACIONADO</p>-->
<!--            </div>-->
<!--			--><?php //get_template_part('content', 'relatedpost' ); ?>
        </div>
        <hr>
<!--        <div id="disqus_thread"></div>-->
    </div>

<!--    <script>-->
<!--        /**-->
<!--         * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.-->
<!--         * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables-->
<!--         */-->
<!--        /*-->
<!--         var disqus_config = function () {-->
<!--         this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable-->
<!--         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable-->
<!--         };-->
<!--         */-->
<!--        (function() { // DON'T EDIT BELOW THIS LINE-->
<!--            var d = document, s = d.createElement('script');-->
<!---->
<!--            s.src = '//batuta.disqus.com/embed.js';-->
<!---->
<!--            s.setAttribute('data-timestamp', +new Date());-->
<!--            (d.head || d.body).appendChild(s);-->
<!--        })();-->
<!--        var disqus_developer = 1;-->
<!--    </script>-->
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

<?php endwhile; ?>

<?php
if ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) AND strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) === 'xmlhttprequest' ) {
	//is ajax
} else {
	get_footer(); ?>

    </div>

	<?php wp_footer(); ?>

    <!-- Inserindo player -->
	<?php get_template_part( 'content', 'player' ); ?>

    <script type="text/javascript" src="<?php echo get_bloginfo( 'template_url' ) ?>/js/player.js"></script>


    </body>

    </html>
<?php } ?>