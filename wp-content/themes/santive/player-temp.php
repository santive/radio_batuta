<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<!-- Website Design By: www.happyworm.com -->
<title>Demo : The jPlayerPlaylist Object</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="wp-content/themes/santive/player/dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="wp-content/themes/santive/player/lib/jquery.min.js"></script>
<script type="text/javascript" src="wp-content/themes/santive/player/dist/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="wp-content/themes/santive/player/dist/jplayer/jplayer.playlist.min.js"></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function(){

	var myPlaylist = new jPlayerPlaylist({
		jPlayer: "#jquery_jplayer_N",
		cssSelectorAncestor: "#jp_container_N"
	}, [
		{
			title:"Teto De Vidro",
			artist:"Teto De Vidro",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/01-Teto-De-Vidro.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/01-Teto-De-Vidro.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		},		{
			title:"Semana Que Vem",
			artist:"Semana Que Vem",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/11-Semana-Que-Vem.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/11-Semana-Que-Vem.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		},		{
			title:"I Wanna Be",
			artist:"I Wanna Be",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/10-I-Wanna-Be.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/10-I-Wanna-Be.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		},		{
			title:"S� De Passagem",
			artist:"S� De Passagem",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/09-S�-De-Passagem.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/09-S�-De-Passagem.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		},		{
			title:"Temporal",
			artist:"Temporal",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/08-Temporal.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/08-Temporal.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		},		{
			title:"Do Mesmo Lado",
			artist:"Do Mesmo Lado",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/07-Do-Mesmo-Lado.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/07-Do-Mesmo-Lado.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		},		{
			title:"Emboscada",
			artist:"Emboscada",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/06-Emboscada.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/06-Emboscada.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		},		{
			title:"O Lobo",
			artist:"O Lobo",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/05-O-Lobo.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/05-O-Lobo.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		},		{
			title:"Equalize",
			artist:"Equalize",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/04-Equalize.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/04-Equalize.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		},		{
			title:"M�scara",
			artist:"M�scara",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/03-M�scara.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/03-M�scara.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		},		{
			title:"Admir�vel Chip Novo",
			artist:"Admir�vel Chip Novo",
			free:true,
			m4v: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/02-Admir�vel-Chip-Novo.mp3",
			ogv: "http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/02-Admir�vel-Chip-Novo.mp3",
			poster:"http://dev.santive.com/radio-batuta/wp-content/uploads/2015/07/Venda-quente-20-pcs-sementes-japonesas-sakura-flor-de-cerejeira-oriental-sementes-Bonsai-plantas-para-e1.jpg"
		}
	], {
		playlistOptions: {
			enableRemoveControls: true
		},
		swfPath: "../../dist/jplayer",
		supplied: "webmv, ogv, m4v, oga, mp3",
		useStateClassSkin: true,
		autoBlur: false,
		smoothPlayBar: true,
		keyEnabled: true,
		audioFullScreen: true
	});

	// Click handlers for jPlayerPlaylist method demo

	// Audio mix playlist



});
//]]>
</script>
</head>
<body>
<div id="jp_container_N" class="jp-video jp-video-270p" role="application" aria-label="media player">
	<div class="jp-type-playlist">
		<div id="jquery_jplayer_N" class="jp-jplayer"></div>
		<div class="jp-gui">
			<div class="jp-video-play">
				<button class="jp-video-play-icon" role="button" tabindex="0">play</button>
			</div>
			<div class="jp-interface">
				<div class="jp-progress">
					<div class="jp-seek-bar">
						<div class="jp-play-bar"></div>
					</div>
				</div>
				<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
				<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
				<div class="jp-details">
					<div class="jp-title" aria-label="title">&nbsp;</div>
				</div>
				<div class="jp-controls-holder">
					<div class="jp-volume-controls">
						<button class="jp-mute" role="button" tabindex="0">mute</button>
						<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
						<div class="jp-volume-bar">
							<div class="jp-volume-bar-value"></div>
						</div>
					</div>
					<div class="jp-controls">
						<button class="jp-previous" role="button" tabindex="0">previous</button>
						<button class="jp-play" role="button" tabindex="0">play</button>
						<button class="jp-stop" role="button" tabindex="0">stop</button>
						<button class="jp-next" role="button" tabindex="0">next</button>
					</div>
					<div class="jp-toggles">
						<button class="jp-repeat" role="button" tabindex="0">repeat</button>
						<button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
						<button class="jp-full-screen" role="button" tabindex="0">full screen</button>
					</div>
				</div>
			</div>
		</div>
		<div class="jp-playlist">
			<ul>
				<!-- The method Playlist.displayPlaylist() uses this unordered list -->
				<li></li>
			</ul>
		</div>
		<div class="jp-no-solution">
			<span>Update Required</span>
			To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
		</div>
	</div>
</div>
</body>

</html>
