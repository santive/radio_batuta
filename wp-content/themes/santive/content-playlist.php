<div class="post-playlist-content">
	<script>
		var objPost = [];
		var objMediaPost = [<?php playlist_(); ?>];
		var objMedia = '';
		setTimeout(function(){
			objPost = { 
				tipo : "<?php echo $objPType->labels->name; ?>",
				img : '<?php the_post_thumbnail('post_thumbnail', array('class' => 'img-responsive')); ?>',
				nome : '<?php the_title(); ?>' ,
				url : '<?php the_permalink(); ?>',
				category : '/?post_type=<?php echo $objPType->name ?>'
			};
		},300);
	</script>
	<ul class="playlist">
		<?php
		$postid = get_the_ID();
		$files = rwmb_meta('batuta_file', 'type=file', $postid);
		if(empty($files)){
			$files = rwmb_meta('batuta_file_ep', 'type=file', $postid);
        }
		$id = 0;       
		foreach($files as $file){ ?>
		<li class="playlist-item">
			<a id="<?php echo $id++; ?>" 
				href="javascript:void(0);" 
				class="playlist-item-link" 
				onclick="setMedia('<?php echo ($id - 1) ?>'); setTimeout(<?php if(count($files) == 1) echo 'hidePrevNext'; else echo 'showPrevNext' ?>,300);" 
				data-post-id="<?php echo $postid; ?>" 
				data-playlist-path="<?php the_permalink(); ?>/playlist.json"
				>
				<?php echo $file['title']; ?>
			</a>
		</li>
		<?php } ?>
	</ul>
</div>