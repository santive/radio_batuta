<!-- Header -->
<?php get_header(); ?>     
<!-- Layout da busca (search php) -->
<div class="home-container">

    <div class="row">
        <div class="col-md-12">
            <div class="title-section">
                <h1>Streaming</h1>
                <p>Grandes cantores e compositores têm suas trajetórias lembradas neste programa, atualizado mensalmente. É possível saber mais de suas vidas e curtir as músicas mais marcantes</p>
            </div>
        </div>
    </div><!-- end row -->

    <div class="row">
        <div class="col-md-12">
            <div class="category-streaming-container">    
                <div class="row">
                    <div class="col-sm-12">
                        <h2>Clássico</h2>
                    </div>  
                </div>
                <div class="row category-streaming-content">
                    <div class="col-md-5">
                        <img src="<?php bloginfo('template_directory'); ?>/images/streaming.png">    
                    </div>
                    <div class="col-md-7">
                        <div class="category-streaming-description">
                            <div class="row">
                                <div class="col-md-7">
                                    <p>Arthur Dapieve é jornalista desde 1986, trabalhando na área de cultura no "Jornal do Brasil" e em "O Globo", do qual é colunista, além do site "NoPonto". Tem dez livros entre ficção e näo ficção, alguns sobre música, como BRock - o rock brasileiro dos anos 80 (1995) e Renato Russo - O trovador solitário (2000). Escuta música clássica desde que era um adolescente fã de rock progressivo e hoje dedica a Bach, Tchaikovsky e Mahler a maioria do seu tempo.</p>    
                                </div>
                                <div class="col-md-5 play">
                                    <div class="play-container">
                                        <img src="<?php bloginfo('template_directory'); ?>/images/play.png">
                                        <p class="">Ouça Ao Vivo</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end category-streaming-container -->
        </div><!-- end col -->
    </div><!-- end row -->

    <div class="row">
        <div class="col-md-12">
            <div class="category-streaming-container">    
                <div class="row">
                    <div class="col-sm-12">
                        <h2>Clássico</h2>
                    </div>  
                </div>
                <div class="row category-streaming-content">
                    <div class="col-md-5">
                        <img src="<?php bloginfo('template_directory'); ?>/images/streaming.png">    
                    </div>
                    <div class="col-md-7">
                        <div class="category-streaming-description">
                            <div class="row">
                                <div class="col-md-7">
                                    <p>Arthur Dapieve é jornalista desde 1986, trabalhando na área de cultura no "Jornal do Brasil" e em "O Globo", do qual é colunista, além do site "NoPonto". Tem dez livros entre ficção e näo ficção, alguns sobre música, como BRock - o rock brasileiro dos anos 80 (1995) e Renato Russo - O trovador solitário (2000). Escuta música clássica desde que era um adolescente fã de rock progressivo e hoje dedica a Bach, Tchaikovsky e Mahler a maioria do seu tempo.</p>    
                                </div>
                                <div class="col-md-5 play">
                                    <div class="play-container">
                                        <img src="<?php bloginfo('template_directory'); ?>/images/play.png">
                                        <p class="">Ouça Ao Vivo</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end category-streaming-container -->
        </div><!-- end col -->
    </div><!-- end row -->

</div><!-- end home-container -->

<?php get_footer(); ?>
<!-- Footer -->     