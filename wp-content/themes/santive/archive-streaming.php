<!-- Header -->
<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){ ?>
    <script type="text/javascript">
        jQuery('body').removeClass('single');
    </script>

    <?php
    //is ajax
    //Dont load Header
}else{
    get_header();
}

checkCookie();?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right'); bloginfo(); ?>" />

<div class="home-container">

    <div class="row">
        <div class="col-md-12">
            <div class="title-section">
                <h1>Música 24 Horas</h1>
                <p> <?php if ( is_active_sidebar( 'streaming' ) ) { ?>
                        <?php if ( is_active_sidebar( 'streaming' ) && !dynamic_sidebar('streaming') ) : ?>
                        <?php endif; ?>
                    <?php } ?>
                </p>
            </div>
        </div>
    </div><!-- end row -->

    <?php //WpQuery
            $id = 0;
            $loop = new WP_Query( array( 'post_type' => 'streaming', 'posts_per_page' => -1 ) );
            while ( $loop->have_posts() ) : $loop->the_post();
            $postid = get_the_ID();?>


    <div class="row">
        <div class="col-md-12">
            <div class="category-streaming-container">
                <div class="row">
                    <div class="col-sm-12">

                    </div>
                </div>
                <div class="row category-streaming-content">
                    <div class="col-md-5">
                        <?php the_post_thumbnail('streaming_thumbnail', array('class' => 'img-responsive')); ?>
                    </div>
                    <div class="col-md-7">
                        <div class="category-streaming-description">
                            <div class="row">
                                <div class="col-md-7">
                                    <h2><?php the_title(); ?></h2>
                                    <p><?php echo getExcerpt(get_the_excerpt(), 0, 300); ?></p>
                                    <div class="play-container">
                                        <a href="javascript:void(0);" onclick="jQuery('#<?php echo $postid; ?>').trigger('click')" class="streaming-link" data-post-url="<?php echo rwmb_meta('batuta_streaming', $postid) ?>" data-post-id="<?php echo $postid; ?>">
                                            <img src="<?php bloginfo('template_directory'); ?>/images/play.png"></a>
                                        <p class=""><a href="javascript:void(0);" onclick="jQuery('#<?php echo $postid; ?>').trigger('click')" class="streaming-link" data-post-url="<?php echo rwmb_meta('batuta_streaming', $postid) ?>" data-post-id="<?php echo $postid; ?>">Ouça Ao Vivo</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end category-streaming-container -->
        </div><!-- end col -->
    </div><!-- end row -->


    <?php endwhile; wp_reset_query(); wp_reset_postdata();?>



</div><!-- end home-container -->



<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
    //is ajax
    // do nothing
}else{ ?>

    <?php  get_footer(); ?>

    </div>

    <?php wp_footer(); ?>

    <!-- Inserindo player -->
    <?php get_template_part('content', 'player' ); ?>

    <script type="text/javascript" src="<?php echo get_bloginfo('template_url') ?>/js/player.js"></script>

    </body>

    </html>



<?php }?>