<!-- Header -->
<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
    //is ajax
    //Dont load Header
}else{
    get_header();
}?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right'); bloginfo(); ?>" />

<?php $selecao = true; ?>

<?php get_template_part('breadcrump'); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="row">

    <!--Foto+Title+Tag--><div class="col-md-8">
            <div class="post-featured">
                <div id="post-featured-image">
<!--                    <div class="social-media">-->
<!--                        <ul>-->
<!--<!--                            <li><a href="" class="synved-social-provider-link"></a></li>-->-->
<!--                            <li><a href="https://plus.google.com/share?url=--><?php //the_permalink(); ?><!--" class="synved-social-provider-google_plus" target="_blank"></a></li>-->
<!--                            <li><a href="http://www.facebook.com/sharer/sharer.php?u=--><?php //the_permalink(); ?><!--&title=--><?php //the_title(); ?><!--" class="synved-social-provider-facebook" target="_blank"></a></li>-->
<!--                            <li><a href="http://twitter.com/intent/tweet?status=--><?php //the_title(); ?><!--+--><?php //the_permalink(); ?><!--" class="synved-social-provider-twitter" target="_blank"></a></li>-->
<!--                            <li>-->
<!--                                <span class="facebook-counter">-->
<!--                                --><?php //echo do_shortcode('[scp code="facebook"]'); ?>
<!--                            </span>-->
<!--                            </li>-->
<!---->
<!---->
<!--                        </ul>-->
<!--                    </div>-->
                    <?php the_post_thumbnail('post_thumbnail');?>
                </div>
                <div id="post-featured-content">
                    <div class="post-featured-content-title">
                        <h1 class="title"><?php the_title();?></h1>
                         <?php
                                $posttags = get_the_tags();
                                if ($posttags) { ?>
                        <div class="tag">
                            <i class="fa fa-tag fa-1 fa-rotate-90" aria-hidden="true"></i><p>
                               <?php
                               //var_dump($posttags);
                                  foreach($posttags as $tag) { ?>
                                    <?php $taglink = get_term_by('name', $tag->name, 'post_tag'); ?>
                                    <a href="<?php echo get_tag_link($taglink); ?>"><?php echo $tag->name; if( next( $posttags ) ) echo ', ';?></a>
                                <?php } ?>
                            </p>
                       </div>
                       <?php }//end if tags ?>

                        <div class="addthis_sharing_toolbox"></div>

                    </div>
                </div>
        </div>


        <div>
            <div class="post-content post-content-notabs">

                <?php the_content(); ?>


            </div>
        </div>
    </div><!--Foto+Title+Tag-->



        <!--Playlist+Conteúdo Relacionado--><?php if($selecao){ ?>
        <div class="col-md-4">
            <div class="post-playlist">
                <div class="post-playlist-title">

                    <?php
                    //$pname = rwmb_meta('batuta_playlist_name') == '' ? 'Playlist' : rwmb_meta('batuta_playlist_name');
                    $pdescription = rwmb_meta('batuta_playlist_description');
                    $pname = rwmb_meta('batuta_playlist_name');

                    ?>
                    <p class="title"><?php echo $pname; ?></p>
                    <p class="sub-title"><?php echo $pdescription; ?></p>
                </div>
                <?php get_template_part( 'content', 'playlist' ); ?>
            </div>

            <!--Conteúdo relacionado -->
            <?php if($selecao){ ?>
                <div class="related-posts">


                            <div class="related-posts-header">
                                <p>CONTEÚDO RELACIONADO</p>
                            </div>


                    <?php get_template_part('content', 'relatedpost' ); ?>


                </div><!-- end related-posts -->

            <?php } else { ?>

            <div class="related-posts">

                <div class="related-posts-header">
                    <p>CONTEÚDO RELACIONADO</p>


                    <?php get_template_part('content', 'relatedpost' ); ?>


                </div><!-- end related-posts -->
                <!--        </div><!-- end col -->

                <?php } ?>

            </div>

            <?php } ?><!--/Playlist+Conteudo Relacionado-->



    <?php
     $postid = get_the_ID();
     //var_dump($postid);
     $files = rwmb_meta('batuta_file', 'type=file', $postid);
     if($files){
        $selecao = true;
     }else{
        $selecao = false;
     }
     //var_dump($selecao);
    ?>

    <?php if($selecao){ $col='col-md-8'; }else{ $col='col-md-8';}?>
<!--    <div class="--><?php //echo $col; ?><!--">-->
<!--        <div class="post-content post-content-notabs">-->
<!---->
<!--        --><?php //the_content(); ?>
<!---->
<!---->
<!--        </div>-->

        <?php wp_reset_query(); wp_reset_postdata(); ?>
        <?php endwhile; ?>
    <?php endif; ?>

<!--        </div>-->


    </div><!-- end col -->

<div id="disqus_thread"></div>
<script>
    /**
     * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */
    /*
     var disqus_config = function () {
     this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
     this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
     };
     */
    (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');

        s.src = '//batuta.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
    var disqus_developer = 1;
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>







<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
    //is ajax
    //Dont load footer
}else{
    get_footer();
}?>
<!-- Footer -->