<?php

// Register Custom Post Type
function custom_post_type() {
    
//Posts do tipo Destaques Slideshow
$labels = array(
        'name' => _x('Destaques Slideshow', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Destaque', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Destaques Slideshow', 'text_domain'),
        'parent_item_colon' => __('Destaques Pai', 'text_domain'),
        'all_items' => __('Todos Destaques', 'text_domain'),
        'view_item' => __('Ver Destaque', 'text_domain'),
        'add_new_item' => __('Adicionar novo Destaque', 'text_domain'),
        'add_new' => __('Adicionar novo', 'text_domain'),
        'edit_item' => __('Editar', 'text_domain'),
        'update_item' => __('Atualizar Destaque', 'text_domain'),
        'search_items' => __('Procurar Destaque', 'text_domain'),
        'not_found' => __('Não Encontrado', 'text_domain'),
        'not_found_in_trash' => __('Não encontrado na lixeira', 'text_domain'),
    );
    $rewrite = array(
        'slug' => 'destaque',
        'with_front' => true,
        'pages' => true,
        'feeds' => true,
    );
    $args = array(
        'label' => __('destaque', 'text_domain'),
        'description' => __('Post Type De Destaques', 'text_domain'),
        'labels' => $labels,
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 4,
        'menu_icon' => get_template_directory_uri() . "/images/word.png",
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
//        'rewrite' => $rewrite,
        'capability_type' => 'post',
        'supports' => array(
	  'thumbnail',
	  )
    );

    register_post_type('destaque', $args);  


//Posts do tipo Destaques Home
$labels = array(
        'name' => _x('Destaques Home', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Destaque', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Destaques da Home', 'text_domain'),
        'parent_item_colon' => __('Destaques Pai', 'text_domain'),
        'all_items' => __('Todos Destaques', 'text_domain'),
        'view_item' => __('Ver Destaque', 'text_domain'),
        'add_new_item' => __('Adicionar novo Destaque', 'text_domain'),
        'add_new' => __('Adicionar novo', 'text_domain'),
        'edit_item' => __('Editar', 'text_domain'),
        'update_item' => __('Atualizar Destaque', 'text_domain'),
        'search_items' => __('Procurar Destaque', 'text_domain'),
        'not_found' => __('Não Encontrado', 'text_domain'),
        'not_found_in_trash' => __('Não encontrado na lixeira', 'text_domain'),
    );
    $rewrite = array(
        'slug' => 'destaque_home',
        'with_front' => true,
        'pages' => true,
        'feeds' => true,
    );
    $args = array(
        'label' => __('destaque_home', 'text_domain'),
        'description' => __('Post Type De Destaques da Home', 'text_domain'),
        'labels' => $labels,
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 4,
        'menu_icon' => get_template_directory_uri() . "/images/word.png",
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
//

        'capability_type' => 'post',
        'supports' => array(
      'content',

      )
    );

    register_post_type('destaque_home', $args);


//Posts do tipo Selecao
$labels = array(
        'name' => _x('Seleções', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Seleção', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Seleções', 'text_domain'),
        'parent_item_colon' => __('Seleção Pai', 'text_domain'),
        'all_items' => __('Todas Seleções', 'text_domain'),
        'view_item' => __('Ver Seleção', 'text_domain'),
        'add_new_item' => __('Adicionar nova Seleção', 'text_domain'),
        'add_new' => __('Adicionar nova', 'text_domain'),
        'edit_item' => __('Editar', 'text_domain'),
        'update_item' => __('Atualizar Seleção', 'text_domain'),
        'search_items' => __('Procurar Seleção', 'text_domain'),
        'not_found' => __('Não Encontrado', 'text_domain'),
        'not_found_in_trash' => __('Não encontrado na lixeira', 'text_domain'),
    );
    $rewrite = array(
        'slug' => 'selecao',
        'with_front' => true,
        'pages' => true,
        'feeds' => true,
    );
    $args = array(
        'label' => __('selecao', 'text_domain'),
        'description' => __('Post Type De Seleções', 'text_domain'),
        'labels' => $labels,
        'taxonomies' => array('selecao_categories', 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 4,
        'menu_icon' => get_template_directory_uri() . "/images/word.png",
        'can_export' => true,
        'has_archive' => 'selecao',
        'exclude_from_search' => false,
        'publicly_queryable' => true,
//        'rewrite' => array('slug' => 'selecao','with_front' => FALSE),
        'capability_type' => 'post',
        'supports' => array(
      'title',   
      'thumbnail',
      'excerpt',
      'post-formats',
      'editor'
      )
    );

    register_post_type('selecao', $args); 



//Posts do tipo Programa
$labels = array(
        'name' => _x('Programas', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Programa', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Programas', 'text_domain'),
        'parent_item_colon' => __('Programa Pai', 'text_domain'),
        'all_items' => __('Todos', 'text_domain'),
        'view_item' => __('Ver programas', 'text_domain'),
        'add_new_item' => __('Adicionar novo', 'text_domain'),
        'add_new' => __('Adicionar novo', 'text_domain'),
        'edit_item' => __('Editar', 'text_domain'),
        'update_item' => __('Atualizar', 'text_domain'),
        'search_items' => __('Procurar', 'text_domain'),
        'not_found' => __('Não Encontrado', 'text_domain'),
        'not_found_in_trash' => __('Não encontrado na lixeira', 'text_domain'),
    );
    $rewrite = array(
        'slug' => 'programa',
        'with_front' => true,
        'pages' => true,
        'feeds' => true,
    );
    $args = array(
        'label' => __('programa', 'text_domain'),
        'description' => __('Post Type "Programa"', 'text_domain'),
        'labels' => $labels,
        'taxonomies' => array('programa_categories', 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => get_template_directory_uri() . "/images/word.png",
        'can_export' => true,
        'has_archive' => 'programas',
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'rewrite' => $rewrite,
        'capability_type' => 'post',
        'supports' => array(
      'title',   
      'thumbnail', 
      'excerpt',           
      'post-formats',
      'editor'
      )
    );

    register_post_type('programa', $args);


	//Posts do tipo Grandes nomes
	$labels = array(
		'name' => _x('Grandes nomes', 'Post Type General Name', 'text_domain'),
		'singular_name' => _x('Grandes nomes', 'Post Type Singular Name', 'text_domain'),
		'menu_name' => __('Grandes nomes', 'text_domain'),
		'parent_item_colon' => __('Grandes nomes Mãe', 'text_domain'),
		'all_items' => __('Todas', 'text_domain'),
		'view_item' => __('Ver coletâneas', 'text_domain'),
		'add_new_item' => __('Adicionar nova', 'text_domain'),
		'add_new' => __('Adicionar nova', 'text_domain'),
		'edit_item' => __('Editar', 'text_domain'),
		'update_item' => __('Atualizar', 'text_domain'),
		'search_items' => __('Procurar', 'text_domain'),
		'not_found' => __('Não Encontrado', 'text_domain'),
		'not_found_in_trash' => __('Não encontrado na lixeira', 'text_domain'),
	);
	$rewrite = array(
		'slug' => 'grandes-nomes',
		'with_front' => true,
		'pages' => true,
		'feeds' => true,
	);
	$args = array(
		'label' => __('coletanea', 'text_domain'),
		'description' => __('Post Type "Grandes nomes"', 'text_domain'),
		'labels' => $labels,
		//'taxonomies' => array('programa_categories', 'post_tag'),
		'hierarchical' => false,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 5,
		'menu_icon' => get_template_directory_uri() . "/images/word.png",
		'can_export' => true,
		'has_archive' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'rewrite' => $rewrite,
		'capability_type' => 'post',
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			'excerpt',
		)
	);

	register_post_type('coletanea', $args);
    

//Posts do tipo Documentario
$labels = array(
        'name' => _x('Documentários', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Documentario', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Documentários', 'text_domain'),
        'parent_item_colon' => __('Documentario Pai', 'text_domain'),
        'all_items' => __('Todos Documentarios', 'text_domain'),
        'view_item' => __('Ver Documentario', 'text_domain'),
        'add_new_item' => __('Adicionar novo Documentario', 'text_domain'),
        'add_new' => __('Adicionar novo', 'text_domain'),
        'edit_item' => __('Editar', 'text_domain'),
        'update_item' => __('Atualizar Documentario', 'text_domain'),
        'search_items' => __('Procurar Documentario', 'text_domain'),
        'not_found' => __('Não Encontrado', 'text_domain'),
        'not_found_in_trash' => __('Não encontrado na lixeira', 'text_domain'),
    );
    $rewrite = array(
        'slug' => 'documentario',
        'with_front' => true,
        'pages' => true,
        'feeds' => true,
    );
    $args = array(
        'label' => __('documentario', 'text_domain'),
        'description' => __('Post Type De Documentario', 'text_domain'),
        'labels' => $labels,
        'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 6,
        'menu_icon' => get_template_directory_uri() . "/images/word.png",
        'can_export' => true,        
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        //'rewrite' => array('slug' => 'documentario/%documentario_categories%','with_front' => FALSE),
        'has_archive' => 'documentario',
        'capability_type' => 'post',
        'supports' => array(
      'title',   
      'thumbnail', 
      'excerpt',           
      'post-formats',
      'editor'
      )
    );

    register_post_type('documentario', $args); 


//Posts do tipo Streaming
$labels = array(
        'name' => _x('Streamings', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Streaming', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Streaming', 'text_domain'),
        'parent_item_colon' => __('Streaming Pai', 'text_domain'),
        'all_items' => __('Todos Streamings', 'text_domain'),
        'view_item' => __('Ver Streaming', 'text_domain'),
        'add_new_item' => __('Adicionar novo Streaming', 'text_domain'),
        'add_new' => __('Adicionar novo', 'text_domain'),
        'edit_item' => __('Editar', 'text_domain'),
        'update_item' => __('Atualizar Streaming', 'text_domain'),
        'search_items' => __('Procurar Streaming', 'text_domain'),
        'not_found' => __('Não Encontrado', 'text_domain'),
        'not_found_in_trash' => __('Não encontrado na lixeira', 'text_domain'),
    );
    $rewrite = array(
        'slug' => 'musica24horas',
        'with_front' => false,
        'pages' => true,
        'feeds' => true,
    );
    $args = array(
        'label' => __('streaming', 'text_domain'),
        'description' => __('Post Type De Streaming', 'text_domain'),
        'labels' => $labels,
        'taxonomies' => array('streaming_categories', 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 6,
        'menu_icon' => get_template_directory_uri() . "/images/word.png",
        'can_export' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'rewrite' => $rewrite,
        'has_archive' => 'musica24horas',
        'capability_type' => 'post',
        'supports' => array(
      'title',  
      'thumbnail',
      'editor', 
      
      )
    );

    register_post_type('streaming', $args); 





//Posts do tipo Especiais
$labels = array(
        'name' => _x('Especiais', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Especial', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Especiais', 'text_domain'),
        'parent_item_colon' => __('Especiais Pai', 'text_domain'),
        'all_items' => __('Todos Especiais', 'text_domain'),
        'view_item' => __('Ver Especial', 'text_domain'),
        'add_new_item' => __('Adicionar novo Especial', 'text_domain'),
        'add_new' => __('Adicionar novo', 'text_domain'),
        'edit_item' => __('Editar', 'text_domain'),
        'update_item' => __('Atualizar Especial', 'text_domain'),
        'search_items' => __('Procurar Especial', 'text_domain'),
        'not_found' => __('Não Encontrado', 'text_domain'),
        'not_found_in_trash' => __('Não encontrado na lixeira', 'text_domain'),


    );
    $rewrite = array(
        'slug' => 'especiais',
        'with_front' => false,
        'pages' => true,
        'feeds' => true,
    );
    $args = array(
        'label' => __('especiais', 'text_domain'),
        'description' => __('Post Type De Streaming', 'text_domain'),
        'labels' => $labels,
        'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 6,
        'menu_icon' => get_template_directory_uri() . "/images/word.png",
        'can_export' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        //'rewrite' => array('slug' => 'especiais/%especiais_categories%','with_front' => FALSE),
        'has_archive' => 'especiais',
        'capability_type' => 'post',
        'supports' => array(
      'title',   
      'thumbnail', 
      'excerpt',           
      'post-formats',
      'editor'
      )
    );



    register_post_type('especiais', $args);


    //Posts do tipo Documentario
    $labels = array(
        'name' => _x('Episódios', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Episódios', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Episódios', 'text_domain'),
        'parent_item_colon' => __('Episódio Pai', 'text_domain'),
        'all_items' => __('Todos Episódios', 'text_domain'),
        'view_item' => __('Ver Episódios', 'text_domain'),
        'add_new_item' => __('Adicionar novo Episódio', 'text_domain'),
        'add_new' => __('Adicionar novo', 'text_domain'),
        'edit_item' => __('Editar', 'text_domain'),
        'update_item' => __('Atualizar Episódio', 'text_domain'),
        'search_items' => __('Procurar Episódio', 'text_domain'),
        'not_found' => __('Não Encontrado', 'text_domain'),
        'not_found_in_trash' => __('Não encontrado na lixeira', 'text_domain'),
    );
    $rewrite = array(
        'slug' => 'episodios',
        'with_front' => true,
        'pages' => true,
        'feeds' => true,
    );
    $args = array(
        'label' => __('episodios', 'text_domain'),
        'description' => __('Post Type De Episódio', 'text_domain'),
        'labels' => $labels,
        'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 6,
        'menu_icon' => get_template_directory_uri() . "/images/word.png",
        'can_export' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        //'rewrite' => array('slug' => 'documentario/%documentario_categories%','with_front' => FALSE),
        'has_archive' => 'episodios',
        'capability_type' => 'post',
        'supports' => array(
            'title',
            'thumbnail',
            'excerpt',
            'post-formats',
            'editor'
        )
    );

    register_post_type('episodios', $args);


}


// Hook into the 'init' action
add_action('init', 'custom_post_type', 0);
add_filter('rwmb_meta_boxes', 'register_meta_boxes');

//MetaBoxes
function register_meta_boxes($meta_boxes) {
    /**
     * Prefix of meta keys (optional)
     * Use underscore (_) at the beginning to make keys hidden
     * Alt.: You also can make prefix empty to disable it
     */
    // Better has an underscore as last sign
    $prefix = 'batuta_';
    

    //Consulta todos os posts 
    $todos_posts = array();
    $custom_posts_types = get_custom_post_types(false);    
    $popularpost = new WP_Query( array( 'posts_per_page' =>-1, 'order' => 'ASC', 'orderby' => 'title' , 'post_type'=>$custom_posts_types ) );
    while ( $popularpost->have_posts() ){ 

        $popularpost->the_post();
    	$todos_posts[get_the_ID()] =  get_the_title() ; 
    	wp_reset_query();wp_reset_postdata();

    }

    //var_dump($todos_posts);

    $array_documentario = array( 'post_type' => 'documentario', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC');
    $loop2 = new WP_Query( $array_documentario );
    while ( $loop2->have_posts() ) : $loop2->the_post();
        $documentarioNome[get_the_ID()]=get_the_title();
        wp_reset_query();
        wp_reset_postdata();
    endwhile;

    $array_episodios = array( 'post_type' => 'episodios', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC');
    $loop3 = new WP_Query( $array_episodios );
    while ( $loop3->have_posts() ) : $loop3->the_post();
        $episodioNome[get_the_ID()]=get_the_title();
        wp_reset_query();
        wp_reset_postdata();
    endwhile;

    //Array com todas as seleções
	$array_selecoes = array( 'post_type' => array('selecao', 'episodios'), 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC');
	$loop4 = new WP_Query( $array_selecoes);
	while ( $loop4->have_posts() ) : $loop4->the_post();
		$selecaoNome[get_the_ID()]=get_the_title();
		wp_reset_query();
		wp_reset_postdata();
	endwhile;

	//Grandes nomes
	/*$meta_boxes[] = array(

		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'batuta-selecoes-coletanea',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Seleções da coletânea', 'batuta_' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'coletanea' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',
		// Auto save: true, false (default). Optional.
		'autosave'   => true,
		// List of meta fields
		'fields'     => array(
			// SELECT BOX
			array(
				'name'        => __( 'Seleções', 'batuta_' ),
				'id'          => "{$prefix}selecaoColetanea",
				'type'        => 'select_advanced',
				// Array of 'value' => 'Label' pairs for select box
				'options'     => $selecaoNome,
				// Select multiple values, optional. Default is false.
				'multiple'    => false,
				'std'         => 'value2',
				'placeholder' => __( 'Selecione as selecoes', 'galeria_se' ),
			),
		)
	);*/

    //Documentarios / Episodios
    $meta_boxes[] = array(

        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'         => 'batuta-documentarios-ep',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'      => __( 'Episódios', 'batuta_' ),
        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => array( 'documentario', 'selecao', 'programa', 'streaming', 'especiais' ),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'    => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority'   => 'high',
        // Auto save: true, false (default). Optional.
        'autosave'   => true,
        // List of meta fields
        'fields'     => array(
            // SELECT BOX
            array(
                'name'        => __( 'Episódios', 'batuta_' ),
                'id'          => "{$prefix}nomeEpisodio",
                'type'        => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options'     => $episodioNome,
                // Select multiple values, optional. Default is false.
                'multiple'    => true,
                'std'         => 'value2',
                'placeholder' => __( 'Selecione os episodios', 'galeria_se' ),
            ),
        )
    );

    //------------------------Campo Select Post-------------------------- 
                
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id' => 'batuta_select',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title' => __('Posts', 'rwmb'),
        // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
        'pages' => array('destaque', 'destaque_home'),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context' => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority' => 'high',
        // Auto save: true, false (default). Optional.
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            
            array(
                'name' => __('Post:<br />(Selecione o post a ser exibido no slider)', 'rwmb'),
                'id' => "{$prefix}posts",
                'type' => 'select_advanced',
                'options' => $todos_posts,
                'placeholder'=>'Selecione o Post'
            ),
        )
                        
    );        
    

    //------------------------Campo Related Post-------------------------- 
                
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id' => 'batuta_related_post',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title' => __('Posts Relacionados', 'rwmb'),
        // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
        'pages' => get_custom_post_types(false),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context' => 'side',
        // Order of meta box: high (default), low. Optional.
        'priority' => 'low',
        // Auto save: true, false (default). Optional.
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            
            array(
                'name' => __('Post:<br />(Busque o post a ser relacionado)', 'rwmb'),
                'id' => "{$prefix}related_posts",
                'type' => 'select_advanced',
                'multiple' => true,
                'options' => $todos_posts
            ),
        )
                        
    ); 


//------------------------Campo Playlist-------------------------- 
                
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id' => 'batuta_playlist',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title' => __('Dados da Playlist', 'rwmb'),
        // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
        'pages' => array('selecao', 'programa', 'posts', 'especiais'),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context' => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority' => 'low',
        // Auto save: true, false (default). Optional.
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            
            array(
                'name' => __('Nome da Playlist:', 'rwmb'),
                'id' => "{$prefix}playlist_name",
                'type' => 'text'
            ),
            array(
                'name' => __('Descrição da Playlist:', 'rwmb'),
                'id' => "{$prefix}playlist_description",
                'type' => 'text'
            ),
        )
                        
    ); 





    //------------------------Campo Arquivos (Áudio)-------------------------- 
                
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id' => 'batuta_file',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title' => __('Arquivos de Áudio', 'rwmb'),
        // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
        'pages' => array('selecao', 'programa' , 'posts', 'especiais', 'episodios', 'documentario', 'coletanea'),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context' => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority' => 'low',
        // Auto save: true, false (default). Optional.
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            
            array(
                'name' => __('Arquivos:', 'rwmb'),
                'id' => "{$prefix}file",
                'type' => 'file_advanced'
                
                
            ),
        )
                        
    );

    //------------------------Campo Episodios (Áudio)--------------------------

    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id' => 'batuta_file_ep',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title' => __('Episódios', 'rwmb'),
        // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
        'pages' => array('episodios'),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context' => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority' => 'low',
        // Auto save: true, false (default). Optional.
        'autosave' => true,
        // List of meta fields
        'fields' => array(

            array(
                'name' => __('Episódio:', 'rwmb'),
                'id' => "{$prefix}file_ep",
                'type' => 'file_advanced'


            ),
        )

    );


    //------------------------Campo Link Streaming-------------------------- 
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id' => 'batuta_streaming',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title' => __('Link do Streaming', 'rwmb'),
        // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
        'pages' => array('streaming'),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context' => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority' => 'low',
        // Auto save: true, false (default). Optional.
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            
            array(
                'name' => __('Link do Streaming: ', 'rwmb'),
                'id' => "{$prefix}streaming",
                'type' => 'text',
                'placeholder' => 'http://',
            ),
        )
                        
    );
    
    //Fim dos meta boxes
    return $meta_boxes;
    
}

//--Taxonomies--//
function selecao_taxonomy() {
    register_taxonomy(
        'selecao_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'selecao',        //post type name
        array(
            'hierarchical' => true,
            'label' => 'Categorias Seleção',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'selecoes', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before
            )
        )
    );
}
add_action( 'init', 'selecao_taxonomy');


function programa_taxonomy() {
    register_taxonomy(
        'programa_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'programa',        //post type name
        array(
            'hierarchical' => true,
            'label' => 'Categorias programas',  //Display name
            'query_var' => true,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_quick_edit' => true,
            'show_admin_column' => true,
            'rewrite' => array(
                'slug' => 'categoria-programa', // This controls the base slug that will display before each term
                'with_front' => true // Don't display the category base before
            )
        )
    );
}
add_action('init', 'programa_taxonomy');

function streaming_taxonomy() {
    register_taxonomy(
        'streaming_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'streaming',        //post type name
        array(
            'hierarchical' => true,
            'label' => 'Categorias Streaming',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'categoria-musica24horas', // This controls the base slug that will display before each term
                'with_front' => true // Don't display the category base before
            )
        )
    );
}
add_action('init', 'streaming_taxonomy');

function especiais_taxonomy() {
    register_taxonomy(
        'especiais_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'especiais',        //post type name
        array(
            'hierarchical' => true,
            'label' => 'Categorias Especiais',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'especiais', // This controls the base slug that will display before each term
                'with_front' => true // Don't display the category base before
            )
        )
    );
}
add_action('init', 'especiais_taxonomy');

function documentario_taxonomy() {
    register_taxonomy(
        'documentario_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'documentario',        //post type name
        array(
            'hierarchical' => true,
            'label' => 'Categorias Documentarios',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'documentario', // This controls the base slug that will display before each term
                'with_front' => true // Don't display the category base before
            )
        )
    );
}
add_action('init', 'documentario_taxonomy');

function arquivo_taxonomy() {
    register_taxonomy(
        'arquivo_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        array('programa','especiais','streaming','selecao','documentario'),        //post type name
        array(
            'hierarchical' => true,
            'label'=> 'Categorias arquivo',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'categoria-arquivo', // This controls the base slug that will display before each term
                'with_front' => true // Don't display the category base before
            )
        )
    );
}
add_action('init', 'arquivo_taxonomy');