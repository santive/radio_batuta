<!-- Header -->
<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){ ?>
    <script type="text/javascript">
        jQuery('body').removeClass('single');
    </script>

    <?php
    //is ajax
    //Dont load Header
}else{
    get_header(); 
}

checkCookie();
?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right'); bloginfo(); ?>" />

<?php 
//Taxonomie Name
$taxonomy  = "selecao_categories";
?>

<div class="row">


<div class="home-container">

    <div class="row">
        <div class="col-md-12">
            <div class="title-section">
                <h1>Seleções</h1>
                <p> <?php if ( is_active_sidebar( 'selecao' ) ) { ?>
                        <?php if ( is_active_sidebar( 'selecao' ) && !dynamic_sidebar('selecao') ) : ?>
                        <?php endif; ?>
                    <?php } ?>
                </p>
            </div>
        </div>
    </div><!-- end row -->


    <div class="row">
        <div class="col-md-12">


            <div class="category-container content-infinite-scroll">

                    <?php dynamic_sidebar( 'image_widget' ); ?>


                <?php
                while (have_posts()) : the_post();
                ?>


                <div class="category-post category-selecao-post item-infinite-scroll">
                    <!-- <a href="<?php the_permalink(); ?>">
                     <?php if ( has_post_thumbnail() ) { ?>
                    <?php the_post_thumbnail('selecao_parent_thumbnail', array('class' => 'img-responsive')); ?></a> -->
                    <a href="<?php the_permalink(); ?>"><?php $post = get_the_ID(); the_post_thumbnail_responsive($post, 'selecao_parent_thumbnail'); ?></a>                                        
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <p><?php echo getExcerpt(get_the_excerpt(), 0, 300); ?></p>
                    <span class="destaque"><?php the_time('d.m.Y');?></span>
                </div>
                    <div class="navigation"><p><?php posts_nav_link(); ?></p></div>

                <!-- Se não tiver imagem -->

                <?php } else { ?>
                        <picture class="post-thumbnail">
                            <!--[if IE 9]><video style="display: none;">
                            <![endif]-->
                            <source srcset="<?php bloginfo('template_url'); ?>/images/imagem-padrao-rb.jpg" media="(max-width: 768px)">
                            <!--[if IE 9]>
                            </video>
                            <![endif]-->
                            <img class="img-responsive no-image-custom-2" srcset="<?php bloginfo('template_url'); ?>/images/imagem-padrao-rb.jpg" >
                        </picture>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p><?php echo getExcerpt(get_the_excerpt(), 0, 500); ?></p>
                <span class="destaque"><?php the_time('d.m.Y'); ?></span>
                        </div>
                <?php } ?>

                <?php  endwhile; ?>
                
            </div><!-- end category-container -->

        </div><!-- end col -->
    </div><!-- end row -->

</div><!-- end home-container -->

</div><!-- end row -->


<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
    //is ajax
}else{ ?>

    <?php  get_footer(); ?>

    </div>

    <?php wp_footer(); ?>

    <!-- Inserindo player -->
    <?php get_template_part('content', 'player' ); ?>

    <script type="text/javascript" src="<?php echo get_bloginfo('template_url') ?>/js/player.js"></script>

    </body>

    </html>



<?php }?>