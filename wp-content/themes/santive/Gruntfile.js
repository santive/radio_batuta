module.exports = function(grunt) {
  grunt.initConfig({    
      less: {
        padrao: {
          options: {
            paths: ["css/less/"]
          },
          files: {
            "css/style.css": "css/less/app.less"
          }
        },
        
      },
      watch: {
        less: {
          files: ['**/*.less'],
          tasks: ['less'],
        },
      },
  });
 
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
 
  grunt.registerTask('default', ['watch', 'less']);
 
};