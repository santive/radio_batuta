<!-- Header -->
<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){ ?>
    <script type="text/javascript">
        jQuery('body').removeClass('single');
    </script>

	<?php
	//is ajax
	//Dont load Header
}else{
	get_header();
}


checkCookie();?>

<!-- campo que passa o title para o history -->
<input id="page_title" type="hidden" value="<?php wp_title('-', true, 'right'); bloginfo(); ?>" />

<?php
//Taxonomie Name
$taxonomy  = "programa_categories";
$term_id = 500;

$terms_children = get_term_children($term_id, $taxonomy);

$arg_get_terms = array(
	'taxonomy' => $taxonomy,
	'parent' => 500
);
?>

<div class="row">
    <div class="col-md-12">
		<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		//var_dump($term);
		$term_name = $term->name;
		$term_description = $term->description; ?>



        <div class="home-container">

            <div class="row">
                <div class="col-md-12">
                    <div class="title-section">
                        <h1>Arquivo</h1>
                        <p> <?php if ( is_active_sidebar( 'arquivo' ) ) { ?>
								<?php if ( is_active_sidebar( 'arquivo' ) && !dynamic_sidebar('arquivo') ) : ?>
								<?php endif; ?>
							<?php } ?>
                        </p>
                    </div>
                </div>
            </div><!-- end row -->


			<?php

			$term_aux;

			//Get post term
			$terms = get_terms($arg_get_terms);

			usort($terms, "cmp_terms");

			foreach($terms as $term){

			$term_aux = $term->slug;
			$term_link = get_term_link($term);


			?>

            <div class="row">
                <div class="col-md-12">

                    <div class="category-container">
                        <div class="category-description">
                            <h2><a href="<?php echo $term_link; ?>"><?php echo $term->name; ?></a></h2>
                            <p><?php echo $term->description ?></p>
                            <p class="destaque"><a href="<?php echo $term_link; ?>">VER TODOS</a></p>
                        </div>


						<?php
						$args = array(
							'post_type' => 'programa',
							'posts_per_page'=> 3,
							'tax_query' => array(
								array(
									'taxonomy' => $taxonomy,
									'field'    => 'slug',
									'terms'    => $term_aux,

								),
							),
						);

						$loop = new WP_Query( $args );
						while ($loop->have_posts()) : $loop->the_post();

						?>


                        <div class="cat-mais category-post">
                            <!--<a href="<?php the_permalink(); ?>">
                             <?php if ( has_post_thumbnail() ) { ?>

                            <?php the_post_thumbnail('selecao_parent_thumbnail', array('class' => 'img-responsive')); ?></a>-->
                            <a href="<?php the_permalink(); ?>"><?php $post = get_the_ID(); the_post_thumbnail_responsive($post, 'programa_thumbnail'); ?></a>
                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <p><?php echo getExcerpt(get_the_excerpt(), 0, 300); ?></p>
                            <span class="destaque"><?php the_time('d.m.Y');?></span>
                        </div>


                        <!-- Se n�o tiver imagem -->

						<?php } else { ?>
                        <picture class="post-thumbnail">
                            <!--[if IE 9]><video style="display: none;">
                            <![endif]-->
                            <source srcset="<?php bloginfo('template_url'); ?>/images/imagem-padrao-rb.jpg" media="(max-width: 768px)">
                            <!--[if IE 9]>
                            </video>
                            <![endif]-->
                            <img class="img-responsive no-image-custom-3" srcset="<?php bloginfo('template_url'); ?>/images/imagem-padrao-rb.jpg" >
                        </picture>
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <p><?php echo getExcerpt(get_the_excerpt(), 0, 300); ?></p>
                        <span class="destaque"><?php the_time('d.m.Y');?></span>
                    </div>
					<?php } ?>

					<?php  endwhile;  wp_reset_query(); wp_reset_postdata();  ?>

                </div><!-- end category-container -->

            </div><!-- end col -->
        </div><!-- end row -->

	<?php }//end foreach ?>
    </div><!-- end home-container -->
</div><!-- end col -->
</div><!-- end row -->


<?php
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' ){
	//is ajax
	// do nothing
}else{ ?>

	<?php  get_footer(); ?>

    </div>

	<?php wp_footer(); ?>

    <!-- Inserindo player -->
	<?php get_template_part('content', 'player' ); ?>

    <script type="text/javascript" src="<?php echo get_bloginfo('template_url') ?>/js/player.js"></script>

    </body>

    </html>



<?php }?>