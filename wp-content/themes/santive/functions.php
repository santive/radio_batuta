<?php

function checkCookie() {
	if ( ! isset( $_SESSION['header_loaded'] ) || $_SESSION['header_loaded'] != true ) {
		header( "Refresh:0" );
	}else{
		return;
	}
}

global $gplaylist;

//--Meta Box--//
require_once( 'metabox.php' );

add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );

function get_episodios_irmaos($post_id, $post_type, $return_own = false){
	$args = array(
		'post_type' => $post_type,
		'posts_per_page' => 1,
		'meta_query' => array(
			array(
			'key' => 'batuta_nomeEpisodio',
			'value' => $post_id
			)
		)
	);

	$documentario = new WP_Query($args);

	if($documentario->have_posts()){
		while ($documentario->have_posts()){
			$documentario->the_post();

			$episodios = rwmb_meta('batuta_nomeEpisodio', 'type=checkbox_list');

			if($return_own) {
				$episodios_irmaos = $episodios;
			}else{
				$episodios_irmaos = array_diff( $episodios, array( $post_id ) );
			}
		}

		wp_reset_query();
		wp_reset_postdata();

		return $episodios_irmaos;
	}else{
		return false;
	}
}

function get_episodio_pai_post_type($post_id){
	$args = array(
		'post_type' => get_custom_post_types(true),
		'posts_per_page' => 1,
		'meta_query' => array(
			array(
				'key' => 'batuta_nomeEpisodio',
				'value' => $post_id
			)
		)
	);

	$documentario = new WP_Query($args);

	if($documentario->have_posts()){
		while ($documentario->have_posts()){
			$documentario->the_post();


			$parent_post_type = get_post_type(get_post(get_the_ID()));
		}

		wp_reset_query();
		wp_reset_postdata();

		return $parent_post_type;
	}else{
		return false;
	}
}

// -enqueue functions gerais
// masonry
function main_enqueue() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'https://code.jquery.com/jquery-2.1.0.min.js' );
	wp_enqueue_script( 'jquery', false, array(), false, true );

//    wp_deregister_script('jquery-migrate');
//    wp_register_script('jquery-migrate', 'http://code.jquery.com/jquery-migrate-3.0.0.min.js');
//    wp_enqueue_script('jquery-migrate', false, array(), 'jquery', true);

	wp_enqueue_script( 'bootstrap', get_bloginfo( 'template_url' ) . '/js/bootstrap.min.js', 'jquery', '', true );
	wp_enqueue_script( 'flexslider', get_bloginfo( 'template_url' ) . '/js/jquery.flexslider-min.js', 'jquery', '', true );
	wp_enqueue_script( 'masonry-grids', get_bloginfo( 'template_url' ) . '/js/masonry-grids.js', 'jquery, masonry-grids', '', true );
	wp_enqueue_script( 'infinitescroll', get_bloginfo( 'template_url' ) . '/js/jquery.infinitescroll.js', 'jquery, infinitescroll', '', true );
	wp_enqueue_script( 'images-loaded', get_bloginfo( 'template_url' ) . '/js/imagesloaded.min.js', '', '', true );
	wp_enqueue_script( 'function-js', get_bloginfo( 'template_url' ) . '/js/functions.js', 'jquery', '', true );
	wp_enqueue_script( 'picturefill', get_bloginfo( 'template_url' ) . '/js/picturefill.js', '', '', true );
	wp_enqueue_script( 'history-js', get_bloginfo( 'template_url' ) . '/js/history.js', 'jquery', '', true );
	wp_register_script('player', get_bloginfo('template_url') . '/js/player.js', 'jquery','', true) ;
	//wp_register_script( 'player', get_bloginfo( 'template_url' ) . '/js/player.min.js', 'jquery', '', true );
	//wp_enqueue_script('player');
	wp_enqueue_script( 'player' );

//    wp_localize_script( 'player', 'postplayer', array(
//    'ajax_url' => admin_url( 'admin-ajax.php' )
//    ));


}

add_action( 'wp_enqueue_scripts', 'main_enqueue' );


/*******************************************************/
/******************Player ajax**************************/
/*******************************************************/
add_action( 'wp_ajax_nopriv_player_ajax', 'player_ajax' );
add_action( 'wp_ajax_player_ajax', 'player_ajax' );
function player_ajax() {

	$postid = $_GET['post_id'];
	$files  = rwmb_meta( 'batuta_file', 'type=file', $postid );


	/* Loop para obtenção da imagem destacada */

	/* get all registeres post types */
	$post_types_array = array();

	$post_types = get_post_types( '', 'names' );

	foreach ( $post_types as $post_type ) {

		$post_types_array[] = $post_type;
	}

	$params = array(
		'p'         => $postid,
		'post_type' => $post_types_array,
	);

	$post_data = new WP_Query( $params );


	$imagem = '';

	if ( $post_data->have_posts() ) {
		while ( $post_data->have_posts() ) {
			$post_data->the_post();

			$imagem = wp_get_attachment_url( get_post_thumbnail_id() );


		}
	}


	$playlist = array();
	$cont     = 0;
	/* ** dump **
   'ID'
   'name'
   'path' *File on server* - C:/www/batuta/uploads/...
   'url' *File link* - fbatuta/upload/music.mp3
   'title'
   */
	foreach ( $files as $file ) {
		$playlist["name"][ $cont ]     = $file['name'];
		$playlist["path"][ $cont ]     = $file['path'];
		$playlist["url"][ $cont ]      = $file['url'];
		$playlist['image'][ $cont ]    = $imagem;
		$playlist["title"][ $cont ++ ] = $file['title'];
	}

	//var_dump($playlist);

	header( 'Content-type: application/json; charset=UTF-8' );

	$playlist = json_encode( $playlist, JSON_UNESCAPED_UNICODE );

	//echo 'post-id: ' . $_POST['post_id'];
	//echo var_dump($playlist);
	echo $playlist;


	die();

}

function playlist_debug( $playlist ) {
	$gplaylist = $playlist;

	return $gplaylist;
}

add_action( 'wp_ajax_nopriv_streaming_ajax', 'streaming_ajax' );
add_action( 'wp_ajax_streaming_ajax', 'streaming_ajax' );
function streaming_ajax() {

	$postid = $_POST['post_id'];


	echo rwmb_meta( 'batuta_streaming', $postid ) . 'XX';


	//echo 'post-id: ' . $_POST['post_id'];
	//echo var_dump($playlist);
	//echo $playlist;
	//echo $link;

	die();

}

/*******************************************************/
/*******************************************************/
/*******************************************************/


function filter_post_type_link( $link, $post ) {
	if ( $post->post_type != 'selecao' && $post->post_type != 'programa' ) {
		return $link;
	}

	if ( $post->post_type == 'selecao' ) {
		if ( $cats = get_the_terms( $post->ID, 'selecao_categories' ) ) {
			$link = str_replace( '%selecao_categories%', array_pop( $cats )->slug, $link );
		}

		return $link;
	}

	if ( $post->post_type == 'programa' ) {
		if ( $cats = get_the_terms( $post->ID, 'programa_categories' ) ) {
			$link = str_replace( '%programa_categories%', array_pop( $cats )->slug, $link );
		}

		return $link;
	}
}

add_filter( 'post_type_link', 'filter_post_type_link', 10, 2 );

//Get Custom Posts Types
//true = inclui cpt streaming
function get_custom_post_types( $have_single = true ) {

	if ( $have_single ) {
		$cpt = array( 'selecao', 'documentario', 'programa', 'streaming', 'especiais', 'episodios', 'coletanea' );
	} else {//cpt Streaming não possui single! não deve aparecer nas listagens
		$cpt = array( 'selecao', 'documentario', 'programa', 'especiais', 'episodios', 'post' );
	}

	return $cpt;

}

//Posts Templates Plugin
/**
 * Hooks the WP cpt_post_types filter
 *
 * @param array $post_types An array of post type names that the templates be used by
 *
 * @return array The array of post type names that the templates be used by
 **/
function my_cpt_post_types( $post_types ) {

	$post_types[] = 'selecao';
	$post_types[] = 'documentario';
	$post_types[] = 'programa';
	$post_types[] = 'streaming';
	$post_types[] = 'especiais';
	$post_types[] = 'episodios';

	return $post_types;
}

add_filter( 'cpt_post_types', 'my_cpt_post_types' );


/* Tags for cpt */
function wpa_cpt_tags( $query ) {
	if ( $query->is_tag() && $query->is_main_query() ) {
		$query->set( 'post_type', get_custom_post_types( false ) );
	}
}

add_action( 'pre_get_posts', 'wpa_cpt_tags' );


//--Tamanho das Galerias --//
if ( ! isset( $content_width ) ) {
	$content_width = 685;
}


//-- Tamanho das Imagens --//
//add_image_size();
add_image_size( 'post_thumbnail', 660, 304, false );
add_image_size( 'home_post_thumbnail', 396, 285, true );
add_image_size( 'destaque_thumbnail', 600, 432, true );
add_image_size( 'selecao_thumbnail', 280, 201, true );
add_image_size( 'programa_thumbnail', 285, 204, true );
add_image_size( 'documentario_thumbnail', 285, 205, true );
add_image_size( 'listagem_thumbnail', 285, 205, true );
add_image_size( 'streaming_thumbnail', 440, 314, true );
add_image_size( 'selecao_parent_thumbnail', 250, 179, true );
add_image_size( 'search_thumbnail', 236, 168, true );
add_image_size( 'related_post_thumbnail', 252, 181, true );
add_image_size( 'grandes_nomes_list', 140, 140, true );

add_image_size( 'full', 600, 432 );


//-- Layout dos excerpts --//
function new_excerpt_more( $more ) {
	return ' ... ';
}

add_filter( 'excerpt_more', 'new_excerpt_more' );


/**
 * Get excerpt from string
 *
 * @param String $str String to get an excerpt from
 * @param Integer $startPos Position int string to start excerpt from
 * @param Integer $maxLength Maximum length the excerpt may be
 *
 * @return String excerpt
 */
function getExcerpt( $str, $startPos = 0, $maxLength = 503 ) {
	if ( strlen( $str ) > $maxLength ) {
		$excerpt   = substr( $str, $startPos, $maxLength - 3 );
		$lastSpace = strrpos( $excerpt, ' ' );
		$excerpt   = substr( $excerpt, 0, $lastSpace );
		$excerpt .= '...';
	} else {
		$excerpt = $str;
	}

	return $excerpt;
}

/***** WIDGETS ********/

//-- Sidebar --//
if ( function_exists( 'register_sidebar' ) ) {

	register_sidebar( array(
		'name' => 'Coletâneas',
		'id'   => 'coletaneas',

	) );
	register_sidebar( array(
		'name' => 'Especiais',
		'id'   => 'especiais',

	) );
	register_sidebar( array(
		'name' => 'Documentários',
		'id'   => 'documentario',

	) );
	register_sidebar( array(
		'name' => 'Programas',
		'id'   => 'programa',

	) );
	register_sidebar( array(
		'name' => 'Arquivo',
		'id'   => 'arquivo',

	) );
	register_sidebar( array(
		'name' => 'Seleções',
		'id'   => 'selecao',

	) );
	register_sidebar( array(
		'name' => 'Streaming',
		'id'   => 'streaming',

	) );

}
/***********************/

/* ==========================================================================
    Image responsive Picturefill
    http://www.lynda.com/articles/create-responsive-featured-images-wordpress
   ========================================================================== */

function the_post_thumbnail_responsive( $postid, $size ) {

	$attachment_id = get_post_thumbnail_id( $postid );
	$alt_text      = esc_html( get_the_title( $postid ) );

	$thumb_original = wp_get_attachment_image_src( $attachment_id, $size );
	$thumb_full     = wp_get_attachment_image_src( $attachment_id, 'full' );

	// Create array containing each image size + the alt tag
	$thumb_data = array(
		'thumb_original' => $thumb_original[0],
		'thumb_full'     => $thumb_full[0],
		'thumb_alt'      => $alt_text
	);

	// Echo out <picture> element based on code from above
	echo '<picture class="post-thumbnail">';
	echo '<!--[if IE 9]><video style="display: none;"><![endif]-->'; // Fallback for IE9
	echo '<source srcset="' . $thumb_data['thumb_full'] . '" media="(max-width: 768px)">';
	echo '<!--[if IE 9]></video><![endif]-->'; // Fallback for IE9
	echo '<img class="img-responsive" srcset="' . $thumb_data['thumb_original'] . '" alt="' . $thumb_data['thumb_alt'] . '">';
	echo '</picture>';
}


/* ==========================================================================
    Facebook Count Share
    http://designmodo.com/social-shares-wordpress/
   ========================================================================== */

function fb_share( $url ) {

	//Não funciona em localhost
	if ( strpos( $url, 'localhost' ) == false ) {

		$api = file_get_contents( 'http://graph.facebook.com/?id=' . $url );

		$count = json_decode( $api );

		return $count->shares;

	} else {
		return 0;
	}
}

;

/* Remove current item from Breadcrumb */
add_action( 'bcn_after_fill', 'bcnext_remove_current_item' );
/**
 * We're going to pop off the paged breadcrumb and add in our own thing
 *
 * @param bcn_breadcrumb_trail $trail the breadcrumb_trail object after it has been filled
 * */

function bcnext_remove_current_item( $trail ) {
	//Make sure we have a type
	//Make sure we have a type
	if ( isset( $trail->breadcrumbs[0]->type ) && is_array( $trail->breadcrumbs[0]->type ) && isset( $trail->breadcrumbs[0]->type[1] ) ) {
		//Check if we have a current item
		if ( in_array( 'current-item', $trail->breadcrumbs[0]->type ) ) {
			//Shift the current item off the front
			array_shift( $trail->breadcrumbs );
		}
	}
}


//Playlist

function playlist() {
	global $post;
	require_once( ABSPATH . "/wp-content/themes/santive/api/getID3/getid3.php" );

	$postid = get_the_ID();
	$postid = $post->ID;

	$myURL   = get_site_url() . "/wp-admin/admin-ajax.php?";
	$options = array( "action" => 'player_ajax', "post_id" => $postid );
	$myURL .= http_build_query( $options, '', '&' );
	$json        = file_get_contents( $myURL );
	$json_output = json_decode( $json, true );

	/*
	$y = count($json_output['url']);
	$x = 1;


	foreach($json_output['url'] AS $key => $valor){
		$m = str_replace("\/","/",($json_output['url'][$key]));
		$mp3 = str_replace(get_site_url(),ABSPATH,$m);
		$getID3 = new getID3;
		$OldThisFileInfo = $getID3->analyze($mp3);
		$tag = $OldThisFileInfo['tags']['id3v2'];
		?>
		{
		title:"<?php echo utf8_decode($json_output['title'][$key]); ?>",
		artist:"<?php echo $tag['artist'][0] ?>",
		genre:"<?php echo ($tag['genre'][0]); ?>",
		album:"<?php echo $tag['album'][0] ?>",
		autor:"<?php echo ($tag['year'][0]); ?>",
		mp3: "<?php echo ($json_output['url'][$key]); ?>",
		}
		<?php
		if($x < $y) echo ",";
		$x++; }
*/

}

//Widget Image

function image_widget_widgets_init() {

	register_sidebar( array(
		'name'          => 'Image Widget',
		'id'            => 'image_widget',
		'before_widget' => '<div class="banner-selecao">',
		'after_widget'  => '</div>',
	) );
}

add_action( 'widgets_init', 'image_widget_widgets_init' );

//Função Filtro para o CPT Selecao
function add_selecao_taxonomy_filters() {
	global $typenow;

	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array( 'selecao_categories', 'arquivo_categories' );

	// must set this to the post type you want the filter(s) displayed on
	if ( $typenow == 'selecao' ) {


		foreach ( $taxonomies as $tax_slug ) {
			$tax_obj = get_taxonomy( $tax_slug );

			$tax_name = $tax_obj->labels->name;

			$terms = get_terms( $tax_slug );
			if ( count( $terms ) > 0 ) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Todas as $tax_name</option>";
				foreach ( $terms as $term ) {
					echo '<option value=' . $term->slug, $_GET[ $tax_slug ] == $term->slug ? ' selected="selected"' : '', '>' . $term->name . ' (' . $term->count . ')</option>';
				}
				echo "</select>";
			}
		}
	}
}

add_action( 'restrict_manage_posts', 'add_selecao_taxonomy_filters' );

//Função Filtro para o CPT Programa
function add_programa_taxonomy_filters() {
	global $typenow;

	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array( 'programa_categories', 'arquivo_categories' );

	// must set this to the post type you want the filter(s) displayed on
	if ( $typenow == 'programa' ) {

		foreach ( $taxonomies as $tax_slug ) {
			$selected      = isset($_GET[$tax_slug]) ? $_GET[$tax_slug] : '';
			$tax_obj  = get_taxonomy( $tax_slug );
			wp_dropdown_categories(array(
				'show_option_all' => __("Show All {$tax_obj->label}"),
				'taxonomy'        => $tax_slug,
				'name'            => $tax_slug,
				'orderby'         => 'name',
				'selected'        => $selected,
				'show_count'      => true,
				'hide_empty'      => false,
				'value_field'     => 'slug',
				//'child_of'        => true,
				//'hierarchical'    => true,
				//'depth'           => 3,

			));
		}
	}
}

add_action( 'restrict_manage_posts', 'add_programa_taxonomy_filters' );

//Função Filtro para o CPT Especiais
function add_especiais_taxonomy_filters() {
	global $typenow;

	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array( 'especiais_categories', 'arquivo_categories' );

	// must set this to the post type you want the filter(s) displayed on
	if ( $typenow == 'especiais' ) {

		foreach ( $taxonomies as $tax_slug ) {
			$tax_obj  = get_taxonomy( $tax_slug );
			$tax_name = $tax_obj->labels->name;
			$terms    = get_terms( $tax_slug );
			if ( count( $terms ) > 0 ) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Todas as $tax_name</option>";
				foreach ( $terms as $term ) {
					echo '<option value=' . $term->slug, $_GET[ $tax_slug ] == $term->slug ? ' selected="selected"' : '', '>' . $term->name . ' (' . $term->count . ')</option>';
				}
				echo "</select>";
			}
		}
	}
}

add_action( 'restrict_manage_posts', 'add_especiais_taxonomy_filters' );

//Função Filtro para o CPT Streaming
function add_streaming_taxonomy_filters() {
	global $typenow;

	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array( 'streaming_categories', 'arquivo_categories' );

	// must set this to the post type you want the filter(s) displayed on
	if ( $typenow == 'streaming' ) {

		foreach ( $taxonomies as $tax_slug ) {
			$tax_obj  = get_taxonomy( $tax_slug );
			$tax_name = $tax_obj->labels->name;
			$terms    = get_terms( $tax_slug );
			if ( count( $terms ) > 0 ) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Todas as $tax_name</option>";
				foreach ( $terms as $term ) {
					echo '<option value=' . $term->slug, $_GET[ $tax_slug ] == $term->slug ? ' selected="selected"' : '', '>' . $term->name . ' (' . $term->count . ')</option>';
				}
				echo "</select>";
			}
		}
	}
}

add_action( 'restrict_manage_posts', 'add_streaming_taxonomy_filters' );

//Função Filtro para o CPT Documentarios
function add_documentario_taxonomy_filters() {
	global $typenow;

	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array( 'documentario_categories', 'arquivo_categories' );

	// must set this to the post type you want the filter(s) displayed on
	if ( $typenow == 'documentario' ) {

		foreach ( $taxonomies as $tax_slug ) {
			$tax_obj  = get_taxonomy( $tax_slug );
			$tax_name = $tax_obj->labels->name;
			$terms    = get_terms( $tax_slug );
			if ( count( $terms ) > 0 ) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Todas as $tax_name</option>";
				foreach ( $terms as $term ) {
					echo '<option value=' . $term->slug, $_GET[ $tax_slug ] == $term->slug ? ' selected="selected"' : '', '>' . $term->name . ' (' . $term->count . ')</option>';
				}
				echo "</select>";
			}
		}
	}
}

add_action( 'restrict_manage_posts', 'add_documentario_taxonomy_filters' );

add_action( 'save_post', 'mudar_titulo_home_destaques' );

function mudar_titulo_home_destaques( $post_id ) {

	$query3 = array( 'destaque_home', 'destaque' );
	$args2  = array(
		'p'         => $post_id,
		'post_type' => $query3
	);


	$query2 = new WP_Query( $args2 );


	if ( $query2->have_posts() ) {
		$post_selecionado = get_field( 'batuta_posts', $post_id );

		$post_types = array(
			'selecao',
			'documentario',
			'programa',
			'streaming',
			'especiais',
			'episodios',
			'post',
			'page'
		);
		$args       = array(
			'p'         => $post_selecionado,
			'post_type' => $post_types
		);
		$query_h    = new WP_Query( $args );

		while ( $query_h->have_posts() ) {
			$query_h->the_post();
			$valores2 = get_the_title();

		}

		wp_reset_query();
		wp_reset_postdata();

		$post_title = $valores2;

		// unhook this function so it doesn't loop infinitely
		remove_action( 'save_post', 'mudar_titulo_home_destaques' );

		// update the post, which calls save_post again
		wp_update_post( array( 'ID' => $post_id, 'post_title' => $post_title ) );

		// re-hook this function
		add_action( 'save_post', 'mudar_titulo_home_destaques' );
	}

	wp_reset_query();
	wp_reset_postdata();

}

/* Alter single main post to just get same category  */
add_action( 'pre_get_posts', 'alter_query' );

function alter_query( $query ) {
	//gets the global post var object
	global $post;

	if ( is_single() ) {


		if ( ! $query->is_main_query() ) {
			return;
		}

		$cats = wp_get_post_categories($post->ID, 'fields=ids');



		$query->set( 'category__in', $cats );

		//we remove the actions hooked on the '__after_loop' (post navigation)
		remove_all_actions( '__after_loop' );
	}
}

/* Altera a ordem dos resultaos de busca */
add_action( 'pre_get_posts', function ( $q )
{
	if (    !is_admin() // Target only front end queries
	        && $q->is_main_query() // Target the main query only
	        &&  $q->is_search()
	) {
		$q->set( 'order',    'DESC'         );
		$q->set( 'orderby',  'date'  );
	}
});

function cmp_terms($a, $b)
{
	return strcmp($a->name, $b->name);
}

function playlist_($postIDParam = null){
	global $post;
	require_once(ABSPATH."/wp-content/themes/santive/api/getID3/getid3.php");

	if(is_null($postIDParam)) {
		$postid = $post->ID;
	}else{
	    $postid = $postIDParam;
    }
	$files = rwmb_meta('batuta_file', 'type=file', $postid);
	if(empty($files)){
		$files = rwmb_meta('batuta_file_ep', 'type=file', $postid);
	}
	$playlist = array();

	foreach($files as $file){
		$m = str_replace("\/","/",($file['url']));
		$mp3 = str_replace(WWW,ABSPATH,$m);
		$getID3 = new getID3;
		$OldThisFileInfo = $getID3->analyze($mp3);
		$tag = $OldThisFileInfo['tags']['id3v2'];
		?>
		{
		title:"<?php echo html_entity_decode($file['title']); ?>",
		artist:"<?php echo $tag['artist'][0] ?>",
		genre:"<?php echo ($tag['genre'][0]); ?>",
		album:"<?php echo $tag['album'][0] ?>",
		composer: "<?php echo isset($tag['composer']) ? $tag['composer'][0] : ''; ?>",
		autor:"<?php echo ($tag['year'][0]); ?>",
		track_number: "<?php echo $OldThisFileInfo['tags']['id3v1']['track'][0] ?>",
		mp3: "<?php echo ($file['url']); ?>",
		},
		<?php
	}
}
$objPType = get_post_type_object(get_post_type(get_the_ID()));

function myfeed_request($qv) {
	if (isset($qv['feed']) && !isset($qv['post_type']))
		$qv['post_type'] = array('programa');
	return $qv;
}
add_filter('request', 'myfeed_request');

/* Função que retorna a playlist de um episodio da coletanea */
function get_selecao_coletanea_playlist($postid){
    $post_selecao = rwmb_meta('batuta_selecaoColetanea', array(), $postid);
    return $post_selecao;
}