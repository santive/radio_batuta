-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02-Jul-2015 às 23:33
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `batuta`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_commentmeta`
--

CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_comments`
--

CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Sr. WordPress', '', 'https://wordpress.org/', '', '2015-06-05 21:04:39', '2015-06-05 21:04:39', 'Olá, Isto é um comentário.\nPara excluir um comentário, faça o login e veja os comentários de posts. Lá você terá a opção de editá-los ou excluí-los.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_links`
--

CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_options`
--

CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=629 ;

--
-- Extraindo dados da tabela `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/batuta', 'yes'),
(2, 'home', 'http://localhost/batuta', 'yes'),
(3, 'blogname', 'Radio Batuta', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'eduardo@santive.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'gzipcompression', '0', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:37:"breadcrumb-navxt/breadcrumb-navxt.php";i:1;s:33:"duplicate-post/duplicate-post.php";i:2;s:59:"force-regenerate-thumbnails/force-regenerate-thumbnails.php";i:3;s:21:"meta-box/meta-box.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '2', 'yes'),
(38, 'gmt_offset', '0', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', '', 'no'),
(41, 'template', 'santive', 'yes'),
(42, 'stylesheet', 'santive', 'yes'),
(43, 'comment_whitelist', '1', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '0', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'subscriber', 'yes'),
(49, 'db_version', '31535', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '1', 'yes'),
(53, 'default_link_category', '2', 'yes'),
(54, 'show_on_front', 'posts', 'yes'),
(55, 'tag_base', '', 'yes'),
(56, 'show_avatars', '1', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '150', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '300', 'yes'),
(63, 'medium_size_h', '300', 'yes'),
(64, 'avatar_default', 'mystery', 'yes'),
(65, 'large_size_w', '1024', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', 'file', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '0', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '0', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:0:{}', 'yes'),
(79, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(82, 'uninstall_plugins', 'a:0:{}', 'no'),
(83, 'timezone_string', '', 'yes'),
(84, 'page_for_posts', '0', 'yes'),
(85, 'page_on_front', '0', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '32453', 'yes'),
(89, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:63:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:10:"copy_posts";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:35:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:10:"copy_posts";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(90, 'WPLANG', 'pt_BR', 'yes'),
(91, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:18:"orphaned_widgets_1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(98, 'cron', 'a:5:{i:1435909440;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1435914281;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1435957521;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1435957711;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(105, '_transient_random_seed', 'bf8559a4841937619557bae6f6184ace', 'yes'),
(109, '_site_transient_timeout_browser_2589a220583546006658f54ada687b45', '1434143094', 'yes'),
(110, '_site_transient_browser_2589a220583546006658f54ada687b45', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"43.0.2357.81";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(126, '_transient_timeout_plugin_slugs', '1435857505', 'no'),
(127, '_transient_plugin_slugs', 'a:6:{i:0;s:19:"akismet/akismet.php";i:1;s:37:"breadcrumb-navxt/breadcrumb-navxt.php";i:2;s:33:"duplicate-post/duplicate-post.php";i:3;s:59:"force-regenerate-thumbnails/force-regenerate-thumbnails.php";i:4;s:9:"hello.php";i:5;s:21:"meta-box/meta-box.php";}', 'no'),
(132, 'theme_mods_twentyfifteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1433538321;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(133, 'current_theme', 'Santive', 'yes'),
(134, 'theme_mods_santive', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}}', 'yes'),
(135, 'theme_switched', '', 'yes'),
(143, 'recently_activated', 'a:0:{}', 'yes'),
(144, 'db_upgraded', '', 'yes'),
(147, 'can_compress_scripts', '1', 'yes'),
(244, '_site_transient_timeout_browser_a7cef1cfa83607636f838b5bc2f2e819', '1435005662', 'yes'),
(245, '_site_transient_browser_a7cef1cfa83607636f838b5bc2f2e819', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:13:"43.0.2357.124";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(361, 'widget_calendar', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(362, 'widget_nav_menu', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(363, 'widget_tag_cloud', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(364, 'widget_pages', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(479, '_site_transient_timeout_browser_afc0dd1f4bdfa183ea07d50c68c58ce7', '1436212710', 'yes'),
(480, '_site_transient_browser_afc0dd1f4bdfa183ea07d50c68c58ce7', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:13:"43.0.2357.130";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(510, 'duplicate_post_copyexcerpt', '1', 'yes'),
(511, 'duplicate_post_copyattachments', '', 'yes'),
(512, 'duplicate_post_copychildren', '', 'yes'),
(513, 'duplicate_post_copystatus', '1', 'yes'),
(514, 'duplicate_post_taxonomies_blacklist', '', 'yes'),
(515, 'duplicate_post_show_row', '1', 'yes'),
(516, 'duplicate_post_show_adminbar', '1', 'yes'),
(517, 'duplicate_post_show_submitbox', '1', 'yes'),
(518, 'duplicate_post_version', '2.6', 'yes'),
(521, 'duplicate_post_copydate', '', 'yes'),
(522, 'duplicate_post_blacklist', '', 'yes'),
(523, 'duplicate_post_title_prefix', '', 'yes'),
(524, 'duplicate_post_title_suffix', '', 'yes'),
(525, 'duplicate_post_roles', 'a:2:{i:0;s:13:"administrator";i:1;s:6:"editor";}', 'yes'),
(565, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1435781220', 'yes'),
(566, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'a:40:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";s:4:"5223";}s:4:"post";a:3:{s:4:"name";s:4:"Post";s:4:"slug";s:4:"post";s:5:"count";s:4:"3269";}s:6:"plugin";a:3:{s:4:"name";s:6:"plugin";s:4:"slug";s:6:"plugin";s:5:"count";s:4:"3204";}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";s:4:"2734";}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";s:4:"2503";}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";s:4:"2001";}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";s:4:"1906";}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";s:4:"1836";}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";s:4:"1787";}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";s:4:"1769";}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";s:4:"1738";}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";s:4:"1728";}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";s:4:"1621";}s:8:"facebook";a:3:{s:4:"name";s:8:"Facebook";s:4:"slug";s:8:"facebook";s:5:"count";s:4:"1419";}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";s:4:"1357";}s:9:"wordpress";a:3:{s:4:"name";s:9:"wordpress";s:4:"slug";s:9:"wordpress";s:5:"count";s:4:"1299";}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";s:4:"1207";}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";s:4:"1165";}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";s:4:"1150";}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";s:4:"1021";}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";s:3:"975";}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";s:3:"942";}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";s:3:"932";}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";s:3:"896";}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";s:3:"865";}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";s:3:"853";}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";s:3:"806";}s:4:"ajax";a:3:{s:4:"name";s:4:"AJAX";s:4:"slug";s:4:"ajax";s:5:"count";s:3:"791";}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";s:3:"767";}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";s:3:"743";}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";s:3:"738";}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";s:3:"736";}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";s:3:"695";}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";s:3:"687";}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";s:3:"682";}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";s:3:"669";}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";s:3:"649";}s:8:"security";a:3:{s:4:"name";s:8:"security";s:4:"slug";s:8:"security";s:5:"count";s:3:"645";}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";s:3:"640";}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";s:3:"639";}}', 'yes'),
(570, 'bcn_options', 'a:46:{s:17:"bmainsite_display";b:0;s:18:"Hmainsite_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:28:"Hmainsite_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:13:"bhome_display";b:0;s:14:"Hhome_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:24:"Hhome_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:13:"bblog_display";b:0;s:14:"Hblog_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:24:"Hblog_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:10:"hseparator";s:6:" &gt; ";s:12:"blimit_title";b:0;s:17:"amax_title_length";i:20;s:20:"bcurrent_item_linked";b:0;s:19:"Hpost_page_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:29:"Hpost_page_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:15:"apost_page_root";s:1:"0";s:15:"Hpaged_template";s:13:"Page %htitle%";s:14:"bpaged_display";b:0;s:19:"Hpost_post_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:29:"Hpost_post_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:15:"apost_post_root";s:1:"0";s:27:"bpost_post_taxonomy_display";b:1;s:24:"Spost_post_taxonomy_type";s:8:"category";s:25:"Hpost_attachment_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:35:"Hpost_attachment_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:13:"H404_template";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:10:"S404_title";s:3:"404";s:16:"Hsearch_template";s:135:"Search results for &#039;<a title="Go to the first page of search results for %title%." href="%link%" class="%type%">%htitle%</a>&#039;";s:26:"Hsearch_template_no_anchor";s:39:"Search results for &#039;%htitle%&#039;";s:22:"Htax_post_tag_template";s:150:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to the %title% tag archives." href="%link%" class="%type%">%htitle%</a></span>";s:32:"Htax_post_tag_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:25:"Htax_post_format_template";s:150:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to the %title% tag archives." href="%link%" class="%type%">%htitle%</a></span>";s:35:"Htax_post_format_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:16:"Hauthor_template";s:107:"Articles by: <a title="Go to the first page of posts by %title%." href="%link%" class="%type%">%htitle%</a>";s:26:"Hauthor_template_no_anchor";s:21:"Articles by: %htitle%";s:12:"Sauthor_name";s:12:"display_name";s:22:"Htax_category_template";s:155:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to the %title% category archives." href="%link%" class="%type%">%htitle%</a></span>";s:32:"Htax_category_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:14:"Hdate_template";s:146:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to the %title% archives." href="%link%" class="%type%">%htitle%</a></span>";s:24:"Hdate_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:23:"Hpost_destaque_template";s:118:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%">%htitle%</a></span>";s:33:"Hpost_destaque_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:30:"bpost_destaque_archive_display";b:1;s:19:"apost_destaque_root";i:0;s:31:"bpost_destaque_taxonomy_display";b:0;s:28:"Spost_destaque_taxonomy_type";s:4:"date";}', 'yes'),
(571, 'bcn_options_bk', 'a:46:{s:17:"bmainsite_display";b:1;s:18:"Hmainsite_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:28:"Hmainsite_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:13:"bhome_display";b:1;s:14:"Hhome_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:24:"Hhome_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:13:"bblog_display";b:1;s:14:"Hblog_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:24:"Hblog_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:10:"hseparator";s:6:" &gt; ";s:12:"blimit_title";b:0;s:17:"amax_title_length";i:20;s:20:"bcurrent_item_linked";b:0;s:19:"Hpost_page_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:29:"Hpost_page_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:15:"apost_page_root";s:1:"0";s:15:"Hpaged_template";s:13:"Page %htitle%";s:14:"bpaged_display";b:0;s:19:"Hpost_post_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:29:"Hpost_post_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:15:"apost_post_root";s:1:"0";s:27:"bpost_post_taxonomy_display";b:1;s:24:"Spost_post_taxonomy_type";s:8:"category";s:25:"Hpost_attachment_template";s:133:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%" class="%type%">%htitle%</a></span>";s:35:"Hpost_attachment_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:13:"H404_template";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:10:"S404_title";s:3:"404";s:16:"Hsearch_template";s:133:"Search results for &#39;<a title="Go to the first page of search results for %title%." href="%link%" class="%type%">%htitle%</a>&#39;";s:26:"Hsearch_template_no_anchor";s:37:"Search results for &#39;%htitle%&#39;";s:22:"Htax_post_tag_template";s:150:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to the %title% tag archives." href="%link%" class="%type%">%htitle%</a></span>";s:32:"Htax_post_tag_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:25:"Htax_post_format_template";s:150:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to the %title% tag archives." href="%link%" class="%type%">%htitle%</a></span>";s:35:"Htax_post_format_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:16:"Hauthor_template";s:107:"Articles by: <a title="Go to the first page of posts by %title%." href="%link%" class="%type%">%htitle%</a>";s:26:"Hauthor_template_no_anchor";s:21:"Articles by: %htitle%";s:12:"Sauthor_name";s:12:"display_name";s:22:"Htax_category_template";s:155:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to the %title% category archives." href="%link%" class="%type%">%htitle%</a></span>";s:32:"Htax_category_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:14:"Hdate_template";s:146:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to the %title% archives." href="%link%" class="%type%">%htitle%</a></span>";s:24:"Hdate_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:23:"Hpost_destaque_template";s:118:"<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to %title%." href="%link%">%htitle%</a></span>";s:33:"Hpost_destaque_template_no_anchor";s:75:"<span typeof="v:Breadcrumb"><span property="v:title">%htitle%</span></span>";s:30:"bpost_destaque_archive_display";b:1;s:19:"apost_destaque_root";s:1:"0";s:31:"bpost_destaque_taxonomy_display";b:0;s:28:"Spost_destaque_taxonomy_type";s:4:"date";}', 'no'),
(572, 'bcn_version', '5.2.2', 'no'),
(594, '_site_transient_timeout_available_translations', '1435796518', 'yes'),
(595, '_site_transient_available_translations', 'a:56:{s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:57:37";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-01 14:30:22";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-27 06:36:25";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:22:"Продължение";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-25 18:55:51";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-20 11:58:24";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-30 08:59:10";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-03 00:26:43";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:12:"Forts&#230;t";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-19 15:30:54";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-19 21:59:02";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-25 13:39:01";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/es_PE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-29 17:53:27";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/es_MX.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-04 14:48:26";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/es_ES.zip";s:3:"iso";a:1:{i:1;s:2:"es";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:3:"4.0";s:7:"updated";s:19:"2014-09-04 19:47:01";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.0/es_CL.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-24 19:32:41";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-15 10:49:37";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-12 09:59:32";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:3:"4.0";s:7:"updated";s:19:"2014-09-05 17:37:43";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:59:"https://downloads.wordpress.org/translation/core/4.0/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 15:20:27";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.1.5/haz.zip";s:3:"iso";a:1:{i:2;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 19:32:58";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:12:"להמשיך";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-27 08:22:08";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:43:50";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:7:"Tovább";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 07:07:32";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-21 14:45:33";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-31 19:34:18";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-07 07:33:53";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:57:22";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 15:57:42";s:12:"english_name";s:17:"Myanmar (Burmese)";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.5/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ေဆာင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-23 18:44:13";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:59:29";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nn_NO";a:8:{s:8:"language";s:5:"nn_NO";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-08 07:10:14";s:12:"english_name";s:19:"Norwegian (Nynorsk)";s:11:"native_name";s:13:"Norsk nynorsk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/nn_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nn";i:2;s:3:"nno";}s:7:"strings";a:1:{s:8:"continue";s:9:"Hald fram";}}s:3:"oci";a:8:{s:8:"language";s:3:"oci";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-10 17:07:58";s:12:"english_name";s:7:"Occitan";s:11:"native_name";s:7:"Occitan";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.2.2/oci.zip";s:3:"iso";a:2:{i:1;s:2:"oc";i:2;s:3:"oci";}s:7:"strings";a:1:{s:8:"continue";s:9:"Contunhar";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-09 10:15:05";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:2:"ps";a:8:{s:8:"language";s:2:"ps";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-29 22:19:48";s:12:"english_name";s:6:"Pashto";s:11:"native_name";s:8:"پښتو";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.1.5/ps.zip";s:3:"iso";a:1:{i:1;s:2:"ps";}s:7:"strings";a:1:{s:8:"continue";s:8:"دوام";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-12 01:38:15";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-23 22:36:27";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-18 17:18:37";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-31 11:58:44";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 09:29:23";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 16:25:46";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.5/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:10:"Nadaljujte";}}s:2:"sq";a:8:{s:8:"language";s:2:"sq";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-29 08:27:12";s:12:"english_name";s:8:"Albanian";s:11:"native_name";s:5:"Shqip";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/sq.zip";s:3:"iso";a:2:{i:1;s:2:"sq";i:2;s:3:"sqi";}s:7:"strings";a:1:{s:8:"continue";s:6:"Vazhdo";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-04 20:54:02";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-26 08:48:37";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 15:16:26";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 07:01:28";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 16:45:38";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:9:"Uyƣurqə";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.5/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:2:"uk";a:8:{s:8:"language";s:2:"uk";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-28 13:43:48";s:12:"english_name";s:9:"Ukrainian";s:11:"native_name";s:20:"Українська";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/uk.zip";s:3:"iso";a:2:{i:1;s:2:"uk";i:2;s:3:"ukr";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продовжити";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-29 06:37:03";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(599, 'rewrite_rules', 'a:112:{s:11:"destaque/?$";s:28:"index.php?post_type=destaque";s:41:"destaque/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=destaque&feed=$matches[1]";s:36:"destaque/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=destaque&feed=$matches[1]";s:28:"destaque/page/([0-9]{1,})/?$";s:46:"index.php?post_type=destaque&paged=$matches[1]";s:10:"selecao/?$";s:27:"index.php?post_type=selecao";s:40:"selecao/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=selecao&feed=$matches[1]";s:35:"selecao/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=selecao&feed=$matches[1]";s:27:"selecao/page/([0-9]{1,})/?$";s:45:"index.php?post_type=selecao&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:36:"destaque/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"destaque/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"destaque/([^/]+)/trackback/?$";s:35:"index.php?destaque=$matches[1]&tb=1";s:49:"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?destaque=$matches[1]&feed=$matches[2]";s:44:"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?destaque=$matches[1]&feed=$matches[2]";s:37:"destaque/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?destaque=$matches[1]&paged=$matches[2]";s:44:"destaque/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?destaque=$matches[1]&cpage=$matches[2]";s:29:"destaque/([^/]+)(/[0-9]+)?/?$";s:47:"index.php?destaque=$matches[1]&page=$matches[2]";s:25:"destaque/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"destaque/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"selecao/[^/]+/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:51:"selecao/[^/]+/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:71:"selecao/[^/]+/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:66:"selecao/[^/]+/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:66:"selecao/[^/]+/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:36:"selecao/([^/]+)/([^/]+)/trackback/?$";s:65:"index.php?selecao_categories=$matches[1]&selecao=$matches[2]&tb=1";s:56:"selecao/([^/]+)/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:77:"index.php?selecao_categories=$matches[1]&selecao=$matches[2]&feed=$matches[3]";s:51:"selecao/([^/]+)/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:77:"index.php?selecao_categories=$matches[1]&selecao=$matches[2]&feed=$matches[3]";s:44:"selecao/([^/]+)/([^/]+)/page/?([0-9]{1,})/?$";s:78:"index.php?selecao_categories=$matches[1]&selecao=$matches[2]&paged=$matches[3]";s:51:"selecao/([^/]+)/([^/]+)/comment-page-([0-9]{1,})/?$";s:78:"index.php?selecao_categories=$matches[1]&selecao=$matches[2]&cpage=$matches[3]";s:36:"selecao/([^/]+)/([^/]+)(/[0-9]+)?/?$";s:77:"index.php?selecao_categories=$matches[1]&selecao=$matches[2]&page=$matches[3]";s:30:"selecao/[^/]+/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:40:"selecao/[^/]+/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:60:"selecao/[^/]+/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:55:"selecao/[^/]+/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:55:"selecao/[^/]+/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:48:"selecao/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:57:"index.php?selecao_categories=$matches[1]&feed=$matches[2]";s:43:"selecao/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:57:"index.php?selecao_categories=$matches[1]&feed=$matches[2]";s:36:"selecao/([^/]+)/page/?([0-9]{1,})/?$";s:58:"index.php?selecao_categories=$matches[1]&paged=$matches[2]";s:43:"selecao/([^/]+)/comment-page-([0-9]{1,})/?$";s:58:"index.php?selecao_categories=$matches[1]&cpage=$matches[2]";s:18:"selecao/([^/]+)/?$";s:40:"index.php?selecao_categories=$matches[1]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)(/[0-9]+)?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";}', 'yes'),
(601, 'category_children', 'a:0:{}', 'yes'),
(618, 'selecao_categories_children', 'a:0:{}', 'yes'),
(621, '_site_transient_timeout_theme_roots', '1435872921', 'yes'),
(622, '_site_transient_theme_roots', 'a:7:{s:7:"santive";s:7:"/themes";s:12:"twentyeleven";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:14:"twentyfourteen";s:7:"/themes";s:9:"twentyten";s:7:"/themes";s:14:"twentythirteen";s:7:"/themes";s:12:"twentytwelve";s:7:"/themes";}', 'yes'),
(624, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:65:"https://downloads.wordpress.org/release/pt_BR/wordpress-4.2.2.zip";s:6:"locale";s:5:"pt_BR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/pt_BR/wordpress-4.2.2.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.2";s:7:"version";s:5:"4.2.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1435871134;s:15:"version_checked";s:5:"4.2.2";s:12:"translations";a:1:{i:0;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-12 01:38:15";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/pt_BR.zip";s:10:"autoupdate";b:1;}}}', 'yes'),
(625, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1435871142;s:7:"checked";a:7:{s:7:"santive";s:3:"1.0";s:12:"twentyeleven";s:3:"2.1";s:13:"twentyfifteen";s:3:"1.2";s:14:"twentyfourteen";s:3:"1.4";s:9:"twentyten";s:3:"1.9";s:14:"twentythirteen";s:3:"1.5";s:12:"twentytwelve";s:3:"1.7";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'yes'),
(626, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1435871138;s:8:"response";a:1:{s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"3.1.2";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.3.1.2.zip";}}s:12:"translations";a:0:{}s:9:"no_update";a:5:{s:37:"breadcrumb-navxt/breadcrumb-navxt.php";O:8:"stdClass":6:{s:2:"id";s:4:"1283";s:4:"slug";s:16:"breadcrumb-navxt";s:6:"plugin";s:37:"breadcrumb-navxt/breadcrumb-navxt.php";s:11:"new_version";s:5:"5.2.2";s:3:"url";s:47:"https://wordpress.org/plugins/breadcrumb-navxt/";s:7:"package";s:65:"https://downloads.wordpress.org/plugin/breadcrumb-navxt.5.2.2.zip";}s:33:"duplicate-post/duplicate-post.php";O:8:"stdClass":7:{s:2:"id";s:4:"1295";s:4:"slug";s:14:"duplicate-post";s:6:"plugin";s:33:"duplicate-post/duplicate-post.php";s:11:"new_version";s:3:"2.6";s:3:"url";s:45:"https://wordpress.org/plugins/duplicate-post/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/duplicate-post.2.6.zip";s:14:"upgrade_notice";s:90:"PHP 5.4 (Strict Standards) compatible + Fixed possible XSS and SQL injections + other bugs";}s:59:"force-regenerate-thumbnails/force-regenerate-thumbnails.php";O:8:"stdClass":6:{s:2:"id";s:5:"33928";s:4:"slug";s:27:"force-regenerate-thumbnails";s:6:"plugin";s:59:"force-regenerate-thumbnails/force-regenerate-thumbnails.php";s:11:"new_version";s:5:"2.0.5";s:3:"url";s:58:"https://wordpress.org/plugins/force-regenerate-thumbnails/";s:7:"package";s:70:"https://downloads.wordpress.org/plugin/force-regenerate-thumbnails.zip";}s:9:"hello.php";O:8:"stdClass":6:{s:2:"id";s:4:"3564";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip";}s:21:"meta-box/meta-box.php";O:8:"stdClass":6:{s:2:"id";s:5:"26827";s:4:"slug";s:8:"meta-box";s:6:"plugin";s:21:"meta-box/meta-box.php";s:11:"new_version";s:5:"4.5.5";s:3:"url";s:39:"https://wordpress.org/plugins/meta-box/";s:7:"package";s:57:"https://downloads.wordpress.org/plugin/meta-box.4.5.5.zip";}}}', 'yes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_postmeta`
--

CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=106 ;

--
-- Extraindo dados da tabela `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_menu_item_type', 'post_type'),
(3, 4, '_menu_item_menu_item_parent', '0'),
(4, 4, '_menu_item_object_id', '2'),
(5, 4, '_menu_item_object', 'page'),
(6, 4, '_menu_item_target', ''),
(7, 4, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(8, 4, '_menu_item_xfn', ''),
(9, 4, '_menu_item_url', ''),
(10, 4, '_menu_item_orphaned', '1433876370'),
(11, 1, '_edit_lock', '1434559610:1'),
(12, 1, '_edit_last', '1'),
(13, 6, '_edit_last', '1'),
(14, 6, '_edit_lock', '1435695074:1'),
(17, 9, '_wp_attached_file', '2015/06/post-featured.png'),
(18, 9, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:600;s:6:"height";i:432;s:4:"file";s:25:"2015/06/post-featured.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"post-featured-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:25:"post-featured-300x216.png";s:5:"width";i:300;s:6:"height";i:216;s:9:"mime-type";s:9:"image/png";}s:14:"post_thumbnail";a:4:{s:4:"file";s:25:"post-featured-468x335.png";s:5:"width";i:468;s:6:"height";i:335;s:9:"mime-type";s:9:"image/png";}s:19:"home_post_thumbnail";a:4:{s:4:"file";s:25:"post-featured-396x285.png";s:5:"width";i:396;s:6:"height";i:285;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(19, 6, '_thumbnail_id', '9'),
(24, 10, '_edit_last', '1'),
(25, 10, '_edit_lock', '1435690254:1'),
(26, 10, '_thumbnail_id', '9'),
(27, 10, '_dp_original', '6'),
(30, 12, '_edit_last', '1'),
(31, 12, '_edit_lock', '1435690254:1'),
(32, 12, '_thumbnail_id', '9'),
(34, 12, '_dp_original', '10'),
(35, 12, '_wp_trash_meta_status', 'draft'),
(36, 12, '_wp_trash_meta_time', '1435690436'),
(41, 14, '_edit_last', '1'),
(42, 14, '_edit_lock', '1435690161:1'),
(43, 14, '_thumbnail_id', '9'),
(44, 14, '_dp_original', '6'),
(49, 16, '_edit_last', '1'),
(50, 16, '_edit_lock', '1435690161:1'),
(51, 16, '_thumbnail_id', '9'),
(52, 16, '_dp_original', '6'),
(57, 18, '_edit_last', '1'),
(58, 18, '_edit_lock', '1435690161:1'),
(59, 18, '_thumbnail_id', '9'),
(60, 18, '_dp_original', '6'),
(65, 20, '_edit_last', '1'),
(66, 20, '_edit_lock', '1435870869:1'),
(67, 20, '_thumbnail_id', '9'),
(68, 20, '_dp_original', '6'),
(71, 28, '_edit_last', '1'),
(72, 28, '_edit_lock', '1435696872:1'),
(73, 28, 'batuta_posts', '20'),
(74, 29, '_edit_last', '1'),
(75, 29, '_edit_lock', '1435696144:1'),
(76, 29, 'batuta_posts', '6'),
(77, 30, '_wp_attached_file', '2015/06/post-featured1.png'),
(78, 30, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:600;s:6:"height";i:432;s:4:"file";s:26:"2015/06/post-featured1.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"post-featured1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"post-featured1-300x216.png";s:5:"width";i:300;s:6:"height";i:216;s:9:"mime-type";s:9:"image/png";}s:14:"post_thumbnail";a:4:{s:4:"file";s:26:"post-featured1-468x335.png";s:5:"width";i:468;s:6:"height";i:335;s:9:"mime-type";s:9:"image/png";}s:19:"home_post_thumbnail";a:4:{s:4:"file";s:26:"post-featured1-396x285.png";s:5:"width";i:396;s:6:"height";i:285;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(79, 29, '_thumbnail_id', '30'),
(80, 28, '_thumbnail_id', '30'),
(81, 31, '_edit_last', '1'),
(82, 31, '_edit_lock', '1435701358:1'),
(83, 31, 'batuta_posts', '6'),
(84, 31, '_thumbnail_id', '30'),
(85, 31, '_dp_original', '29'),
(86, 36, '_edit_last', '1'),
(87, 36, '_edit_lock', '1435872240:1'),
(88, 37, '_edit_last', '1'),
(89, 37, '_edit_lock', '1435785528:1'),
(90, 37, '_wp_trash_meta_status', 'publish'),
(91, 37, '_wp_trash_meta_time', '1435785713'),
(92, 38, '_edit_last', '1'),
(93, 38, '_edit_lock', '1435871432:1'),
(94, 40, '_wp_attached_file', '2015/07/selecao.png'),
(95, 40, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:288;s:6:"height";i:202;s:4:"file";s:19:"2015/07/selecao.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"selecao-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(97, 45, '_edit_last', '1'),
(98, 45, '_edit_lock', '1435871456:1'),
(100, 46, '_wp_attached_file', '2015/07/selecao1.png'),
(101, 46, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:201;s:4:"file";s:20:"2015/07/selecao1.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"selecao1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(102, 45, '_thumbnail_id', '46'),
(104, 38, '_thumbnail_id', '46'),
(105, 36, '_thumbnail_id', '9');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_posts`
--

CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=47 ;

--
-- Extraindo dados da tabela `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2015-06-05 21:04:39', '2015-06-05 21:04:39', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a publicar!', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2015-06-15 20:41:58', '2015-06-15 20:41:58', '', 0, 'http://localhost/batuta/?p=1', 0, 'post', '', 1),
(2, 1, '2015-06-05 21:04:39', '2015-06-05 21:04:39', 'Esta é uma página de exemplo. É diferente de um post porque ela ficará em um local e será exibida na navegação do seu site (na maioria dos temas). A maioria das pessoas começa com uma página de introdução aos potenciais visitantes do site. Ela pode ser assim:\n\n<blockquote>Olá! Eu sou um bike courrier de dia, ator amador à noite e este é meu blog. Eu moro em São Paulo, tenho um cachorro chamado Tonico e eu gosto de caipirinhas. (E de ser pego pela chuva.)</blockquote>\n\nou assim:\n\n<blockquote>A XYZ foi fundada em 1971 e desde então vem proporcionando produtos de qualidade a seus clientes. Localizada em Valinhos, XYZ emprega mais de 2.000 pessoas e faz várias contribuições para a comunidade local.</blockquote>\nComo um novo usuário do WordPress, você deve ir até o <a href="http://localhost/batuta/wp-admin/">seu painel</a> para excluir essa página e criar novas páginas com seu próprio conteúdo. Divirta-se!', 'Página de Exemplo', '', 'publish', 'open', 'open', '', 'pagina-exemplo', '', '', '2015-06-05 21:04:39', '2015-06-05 21:04:39', '', 0, 'http://localhost/batuta/?page_id=2', 0, 'page', '', 0),
(4, 1, '2015-06-09 18:59:29', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'open', 'open', '', '', '', '', '2015-06-09 18:59:29', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?p=4', 1, 'nav_menu_item', '', 0),
(5, 1, '2015-06-15 20:41:58', '2015-06-15 20:41:58', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a publicar!', 'Olá, mundo!', '', 'inherit', 'open', 'open', '', '1-revision-v1', '', '', '2015-06-15 20:41:58', '2015-06-15 20:41:58', '', 1, 'http://localhost/batuta/2015/06/15/1-revision-v1/', 0, 'revision', '', 0),
(6, 1, '2015-06-29 21:11:26', '2015-06-29 21:11:26', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'publish', 'open', 'open', '', 'mais-de-149-maneiras-de-ser-carinhoso', '', '', '2015-06-30 19:43:54', '2015-06-30 19:43:54', '', 0, 'http://localhost/batuta/?p=6', 0, 'post', '', 0),
(7, 1, '2015-06-29 21:11:26', '2015-06-29 21:11:26', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'inherit', 'open', 'open', '', '6-revision-v1', '', '', '2015-06-29 21:11:26', '2015-06-29 21:11:26', '', 6, 'http://localhost/batuta/2015/06/29/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2015-06-29 21:33:53', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-29 21:33:53', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?p=8', 0, 'post', '', 0),
(9, 1, '2015-06-30 18:33:09', '2015-06-30 18:33:09', '', 'post-featured', '', 'inherit', 'open', 'open', '', 'post-featured', '', '', '2015-06-30 18:33:09', '2015-06-30 18:33:09', '', 6, 'http://localhost/batuta/wp-content/uploads/2015/06/post-featured.png', 0, 'attachment', 'image/png', 0),
(10, 1, '2015-06-30 18:53:08', '2015-06-30 18:53:08', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'publish', 'open', 'open', '', 'mais-de-149-maneiras-de-ser-carinhoso-2', '', '', '2015-06-30 18:53:08', '2015-06-30 18:53:08', '', 0, 'http://localhost/batuta/?p=10', 0, 'post', '', 0),
(11, 1, '2015-06-30 18:53:08', '2015-06-30 18:53:08', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'inherit', 'open', 'open', '', '10-revision-v1', '', '', '2015-06-30 18:53:08', '2015-06-30 18:53:08', '', 10, 'http://localhost/batuta/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2015-06-30 18:53:23', '2015-06-30 18:53:23', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'trash', 'open', 'open', '', 'mais-de-149-maneiras-de-ser-carinhoso-3', '', '', '2015-06-30 18:53:56', '2015-06-30 18:53:56', '', 0, 'http://localhost/batuta/?p=12', 0, 'post', '', 0),
(13, 1, '2015-06-30 18:53:56', '2015-06-30 18:53:56', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'inherit', 'open', 'open', '', '12-revision-v1', '', '', '2015-06-30 18:53:56', '2015-06-30 18:53:56', '', 12, 'http://localhost/batuta/12-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2015-06-30 18:54:02', '2015-06-30 18:54:02', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'publish', 'open', 'open', '', 'mais-de-149-maneiras-de-ser-carinhoso-4', '', '', '2015-06-30 18:54:02', '2015-06-30 18:54:02', '', 0, 'http://localhost/batuta/mais-de-149-maneiras-de-ser-carinhoso-4/', 0, 'post', '', 0),
(15, 1, '2015-06-30 18:54:02', '2015-06-30 18:54:02', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'inherit', 'open', 'open', '', '14-revision-v1', '', '', '2015-06-30 18:54:02', '2015-06-30 18:54:02', '', 14, 'http://localhost/batuta/14-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2015-06-30 18:54:07', '2015-06-30 18:54:07', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'publish', 'open', 'open', '', 'mais-de-149-maneiras-de-ser-carinhoso-5', '', '', '2015-06-30 18:54:07', '2015-06-30 18:54:07', '', 0, 'http://localhost/batuta/mais-de-149-maneiras-de-ser-carinhoso-5/', 0, 'post', '', 0),
(17, 1, '2015-06-30 18:54:07', '2015-06-30 18:54:07', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'inherit', 'open', 'open', '', '16-revision-v1', '', '', '2015-06-30 18:54:07', '2015-06-30 18:54:07', '', 16, 'http://localhost/batuta/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2015-06-30 18:54:13', '2015-06-30 18:54:13', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'publish', 'open', 'open', '', 'mais-de-149-maneiras-de-ser-carinhoso-6', '', '', '2015-06-30 18:54:13', '2015-06-30 18:54:13', '', 0, 'http://localhost/batuta/mais-de-149-maneiras-de-ser-carinhoso-6/', 0, 'post', '', 0),
(19, 1, '2015-06-30 18:54:13', '2015-06-30 18:54:13', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'inherit', 'open', 'open', '', '18-revision-v1', '', '', '2015-06-30 18:54:13', '2015-06-30 18:54:13', '', 18, 'http://localhost/batuta/18-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2015-06-30 18:54:18', '2015-06-30 18:54:18', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'publish', 'open', 'open', '', 'mais-de-149-maneiras-de-ser-carinhoso-7', '', '', '2015-06-30 18:54:19', '2015-06-30 18:54:19', '', 0, 'http://localhost/batuta/mais-de-149-maneiras-de-ser-carinhoso-7/', 0, 'post', '', 0),
(21, 1, '2015-06-30 18:54:19', '2015-06-30 18:54:19', 'A equipe da Reserva de Música do IMS, coordenada por Bia Paes Leme, reuniu 153 versões de "Carinhoso", a obra-prima de Pixinguinha e João de Barro, o Braguinha. A lista começa na versão original, de 1928, nove anos antes de a composição ganhar letra (e ser lançada por Orlando Silva), até uma de 2009. Veja ao lado os anos das gravações e os nomes dos intérpretes. É só clicar para ouvir.\r\n\r\nApresentação: <strong>Reinaldo Figueiredo </strong>\r\n\r\nEdição e sonorização: <strong>Filipe Di Castro</strong>\r\n\r\nFoto: <strong>Acervo</strong> <strong>IMS</strong>', 'Mais de 149 maneiras de ser ''Carinhoso''', '', 'inherit', 'open', 'open', '', '20-revision-v1', '', '', '2015-06-30 18:54:19', '2015-06-30 18:54:19', '', 20, 'http://localhost/batuta/20-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2015-06-30 20:13:42', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-30 20:13:42', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=destaque&p=22', 0, 'destaque', '', 0),
(23, 1, '2015-06-30 20:16:11', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-30 20:16:11', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=destaque&p=23', 0, 'destaque', '', 0),
(24, 1, '2015-06-30 20:18:15', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-30 20:18:15', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=destaque&p=24', 0, 'destaque', '', 0),
(25, 1, '2015-06-30 20:18:49', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-30 20:18:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=destaque&p=25', 0, 'destaque', '', 0),
(26, 1, '2015-06-30 20:18:55', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-30 20:18:55', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=destaque&p=26', 0, 'destaque', '', 0),
(27, 1, '2015-06-30 20:19:22', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-30 20:19:22', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=destaque&p=27', 0, 'destaque', '', 0),
(28, 1, '2015-06-30 20:19:59', '2015-06-30 20:19:59', '', 'Destaque 1', '', 'publish', 'closed', 'closed', '', 'destaque-1', '', '', '2015-06-30 20:32:24', '2015-06-30 20:32:24', '', 0, 'http://localhost/batuta/?post_type=destaque&#038;p=28', 0, 'destaque', '', 0),
(29, 1, '2015-06-30 20:20:14', '2015-06-30 20:20:14', '', 'Destaque 2', '', 'publish', 'closed', 'closed', '', 'destaque-2', '', '', '2015-06-30 20:31:25', '2015-06-30 20:31:25', '', 0, 'http://localhost/batuta/?post_type=destaque&#038;p=29', 0, 'destaque', '', 0),
(30, 1, '2015-06-30 20:31:19', '2015-06-30 20:31:19', '', 'post-featured', '', 'inherit', 'open', 'open', '', 'post-featured-2', '', '', '2015-06-30 20:31:19', '2015-06-30 20:31:19', '', 29, 'http://localhost/batuta/wp-content/uploads/2015/06/post-featured1.png', 0, 'attachment', 'image/png', 0),
(31, 1, '2015-06-30 21:29:48', '2015-06-30 21:29:48', '', 'Destaque 3', '', 'publish', 'closed', 'closed', '', 'destaque-2-2', '', '', '2015-06-30 21:29:55', '2015-06-30 21:29:55', '', 0, 'http://localhost/batuta/destaque/destaque-2-2/', 0, 'destaque', '', 0),
(32, 1, '2015-07-01 16:27:54', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-07-01 16:27:54', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=destaque&p=32', 0, 'destaque', '', 0),
(33, 1, '2015-07-01 18:48:45', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-07-01 18:48:45', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=destaque&p=33', 0, 'destaque', '', 0),
(34, 1, '2015-07-01 19:04:50', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-07-01 19:04:50', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=destaque&p=34', 0, 'destaque', '', 0),
(35, 1, '2015-07-01 20:20:25', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-07-01 20:20:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=destaque&p=35', 0, 'destaque', '', 0),
(36, 1, '2015-07-01 20:21:02', '2015-07-01 20:21:02', 'Eduardo Gonçalves de Andrade, o Tostão, não conseguiu restringir a cinco suas escolhas de músicas preferidas de Chico Buarque. Citou oito e completou com "dezenas de outras". Craque no campo, campeão do mundo em 1970, e craque na imprensa, colunista de futebol da Folha de S. Paulo, Tostão elegeu alguns grandes sucessos de Chico, como Quem te viu, quem te vê, Olhos nos olhos e Gota d''água, mas também incluiu a pouco lembrada Valsinha, parceria do compositor com Vinicius de Moraes.\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\nEduardo Gonçalves de Andrade, o Tostão, não conseguiu restringir a cinco suas escolhas de músicas preferidas de Chico Buarque. Citou oito e completou com "dezenas de outras". Craque no campo, campeão do mundo em 1970, e craque na imprensa, colunista de futebol da Folha de S. Paulo, Tostão elegeu alguns grandes sucessos de Chico, como Quem te viu, quem te vê, Olhos nos olhos e Gota d''água, mas também incluiu a pouco lembrada Valsinha, parceria do compositor com Vinicius de Moraes.', 'Chico 70 anos - por Tostão', '', 'publish', 'closed', 'closed', '', 'teste', '', '', '2015-07-02 21:23:59', '2015-07-02 21:23:59', '', 0, 'http://localhost/batuta/?post_type=selecao&#038;p=36', 0, 'selecao', '', 0),
(37, 1, '2015-07-01 21:18:21', '2015-07-01 21:18:21', '', 'teste 2', '', 'trash', 'closed', 'closed', '', 'teste-2', '', '', '2015-07-01 21:21:53', '2015-07-01 21:21:53', '', 0, 'http://localhost/batuta/?post_type=selecao&#038;p=37', 0, 'selecao', '', 0),
(38, 1, '2015-07-01 21:22:18', '2015-07-01 21:22:18', 'Pedimos aos dez convidados da série Chico 70 anos, criada em homenagem à idade que Chico Buarque completa em 19 de junho, em torno de cinco músicas preferidas dentre as do compositor. Fernanda Torres mandou seis. Duas delas vieram com títulos inexatos, mas não houve dúvidas de quais se tratavam: "Os escafandristas" é Futuros amantes, de 1993. E "Estreitos nós" é Tira as mãos de mim, da trilha da peça Calabar (1972/73) e uma dessas letras femininas em que.\r\n\r\n&nbsp;\r\n\r\nPedimos aos dez convidados da série Chico 70 anos, criada em homenagem à idade que Chico Buarque completa em 19 de junho, em torno de cinco músicas preferidas dentre as do compositor. Fernanda Torres mandou seis. Duas delas vieram com títulos inexatos, mas não houve dúvidas de quais se tratavam: "Os escafandristas" é Futuros amantes, de 1993. E "Estreitos nós" é Tira as mãos de mim, da trilha da peça Calabar (1972/73) e uma dessas letras femininas em que..', 'Chico 70 anos - Por Fernanda Torres', '', 'publish', 'closed', 'closed', '', 'teste2', '', '', '2015-07-02 21:12:53', '2015-07-02 21:12:53', '', 0, 'http://localhost/batuta/?post_type=selecao&#038;p=38', 0, 'selecao', '', 0),
(39, 1, '2015-07-01 21:26:12', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-07-01 21:26:12', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?p=39', 0, 'post', '', 0),
(40, 1, '2015-07-02 20:00:00', '2015-07-02 20:00:00', '', 'selecao', '', 'inherit', 'open', 'open', '', 'selecao', '', '', '2015-07-02 20:00:00', '2015-07-02 20:00:00', '', 38, 'http://localhost/batuta/wp-content/uploads/2015/07/selecao.png', 0, 'attachment', 'image/png', 0),
(41, 1, '2015-07-02 20:59:41', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-07-02 20:59:41', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=selecao&p=41', 0, 'selecao', '', 0),
(42, 1, '2015-07-02 20:59:59', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-07-02 20:59:59', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=selecao&p=42', 0, 'selecao', '', 0),
(43, 1, '2015-07-02 21:00:18', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-07-02 21:00:18', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=selecao&p=43', 0, 'selecao', '', 0),
(44, 1, '2015-07-02 21:01:23', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-07-02 21:01:23', '0000-00-00 00:00:00', '', 0, 'http://localhost/batuta/?post_type=selecao&p=44', 0, 'selecao', '', 0),
(45, 1, '2015-07-02 21:02:13', '2015-07-02 21:02:13', 'Convidada a escolher cinco músicas favoritas de Chico Buarque para a série da Batuta em homenagem aos 70 anos do compositor, Adriana Calcanhotto não achou a missão difícil. "É totalmente impossível", afirmou. Fez um esforço sobre-humano e enviou seis. Dá para afirmar que a maior surpresa é A foto da capa, última faixa do disco Paratodos (1993), na qual Chico recorda sua detenção na adolescência por tentar furtar um carro. Futuros amantes é do mesmo\r\n\r\n&nbsp;\r\n\r\nConvidada a escolher cinco músicas favoritas de Chico Buarque para a série da Batuta em homenagem aos 70 anos do compositor, Adriana Calcanhotto não achou a missão difícil. "É totalmente impossível", afirmou. Fez um esforço sobre-humano e enviou seis. Dá para afirmar que a maior surpresa é A foto da capa, última faixa do disco Paratodos (1993), na qual Chico recorda sua detenção na adolescência por tentar furtar um carro. Futuros amantes é do mesmo...', 'Chico 70 anos - por Adriana Calcanhotto', '', 'publish', 'closed', 'closed', '', 'teste3', '', '', '2015-07-02 21:13:17', '2015-07-02 21:13:17', '', 0, 'http://localhost/batuta/?post_type=selecao&#038;p=45', 0, 'selecao', '', 0),
(46, 1, '2015-07-02 21:10:07', '2015-07-02 21:10:07', '', 'selecao', '', 'inherit', 'open', 'open', '', 'selecao-2', '', '', '2015-07-02 21:10:07', '2015-07-02 21:10:07', '', 45, 'http://localhost/batuta/wp-content/uploads/2015/07/selecao1.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_terms`
--

CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'teste', 'teste', 0),
(3, 'teste', 'teste', 0),
(4, 'Programas', 'programas', 0),
(5, 'Streaming', 'streaming', 0),
(6, 'Seleções', 'selecoes', 0),
(7, 'Pixinguinha', 'pixinguinha', 0),
(8, 'Velha Guarda', 'velha-guarda', 0),
(9, 'MPB', 'mpb', 0),
(11, 'Chico 70 anos', 'chico70anos', 0),
(12, 'Chico 60 anos', 'chico60anos', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_term_relationships`
--

CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(1, 3, 0),
(6, 3, 0),
(6, 6, 0),
(6, 7, 0),
(6, 8, 0),
(6, 9, 0),
(10, 6, 0),
(10, 7, 0),
(10, 8, 0),
(10, 9, 0),
(12, 6, 0),
(12, 7, 0),
(12, 8, 0),
(12, 9, 0),
(14, 6, 0),
(14, 7, 0),
(14, 8, 0),
(14, 9, 0),
(16, 6, 0),
(16, 7, 0),
(16, 8, 0),
(16, 9, 0),
(18, 6, 0),
(18, 7, 0),
(18, 8, 0),
(18, 9, 0),
(20, 6, 0),
(20, 7, 0),
(20, 8, 0),
(20, 9, 0),
(36, 11, 0),
(38, 11, 0),
(45, 11, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 0),
(3, 3, 'category', '', 0, 2),
(4, 4, 'category', '', 0, 0),
(5, 5, 'category', '', 0, 0),
(6, 6, 'category', '', 0, 6),
(7, 7, 'post_tag', '', 0, 6),
(8, 8, 'post_tag', '', 0, 6),
(9, 9, 'post_tag', '', 0, 6),
(11, 11, 'selecao_categories', '', 0, 3),
(12, 12, 'selecao_categories', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_usermeta`
--

CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=31 ;

--
-- Extraindo dados da tabela `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'santive'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wp_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets,wp410_dfw'),
(13, 1, 'show_welcome_panel', '0'),
(14, 1, 'session_tokens', 'a:4:{s:64:"2ad096689b1fd49903aa64cf1ebdf7da6bdc33414c915ebd618c54929c4c7020";a:4:{s:10:"expiration";i:1435855843;s:2:"ip";s:3:"::1";s:2:"ua";s:109:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36";s:5:"login";i:1435683043;}s:64:"d3ed37c5a12e35094b484abd9799cbf3a89979ecc0f6db335a62b34a9ecec4ed";a:4:{s:10:"expiration";i:1435858630;s:2:"ip";s:3:"::1";s:2:"ua";s:109:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36";s:5:"login";i:1435685830;}s:64:"318e3560885235aea54b53a43bd8ff5baa584729f6cc74df69ed377333937c7c";a:4:{s:10:"expiration";i:1435939992;s:2:"ip";s:3:"::1";s:2:"ua";s:109:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36";s:5:"login";i:1435767192;}s:64:"654147100bde3e7851e2e667a79ba088faa5ca990e6c8f3d215e9556c958c577";a:4:{s:10:"expiration";i:1436028600;s:2:"ip";s:3:"::1";s:2:"ua";s:109:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36";s:5:"login";i:1435855800;}}'),
(15, 1, 'wp_dashboard_quick_press_last_post_id', '8'),
(16, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(17, 1, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:"dashboard_right_now";i:1;s:18:"dashboard_activity";i:2;s:21:"dashboard_quick_press";i:3;s:17:"dashboard_primary";}'),
(18, 1, 'managenav-menuscolumnshidden', 'a:1:{i:0;s:0:"";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:0:{}'),
(20, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(21, 1, 'wp_user-settings', 'libraryContent=browse'),
(22, 1, 'wp_user-settings-time', '1435689207'),
(23, 1, 'closedpostboxes_destaque', 'a:0:{}'),
(24, 1, 'metaboxhidden_destaque', 'a:1:{i:0;s:7:"slugdiv";}'),
(25, 1, 'closedpostboxes_post', 'a:0:{}'),
(26, 1, 'metaboxhidden_post', 'a:6:{i:0;s:13:"trackbacksdiv";i:1;s:10:"postcustom";i:2;s:16:"commentstatusdiv";i:3;s:11:"commentsdiv";i:4;s:7:"slugdiv";i:5;s:9:"authordiv";}'),
(27, 1, 'closedpostboxes_selecao', 'a:1:{i:0;s:10:"postcustom";}'),
(28, 1, 'metaboxhidden_selecao', 'a:1:{i:0;s:7:"slugdiv";}'),
(29, 1, 'meta-box-order_selecao', 'a:3:{s:4:"side";s:44:"submitdiv,selecao_categoriesdiv,postimagediv";s:6:"normal";s:40:"postexcerpt,postcustom,slugdiv,authordiv";s:8:"advanced";s:0:"";}'),
(30, 1, 'screen_layout_selecao', '2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_users`
--

CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'santive', '$P$BurbKyuzkoWskjA.A3AEIm.DHckr1H.', 'santive', 'eduardo@santive.com', '', '2015-06-05 21:04:39', '', 0, 'santive');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
